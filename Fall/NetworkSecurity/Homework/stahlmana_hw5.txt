Andrew Stahlman
aps0015@auburn.edu
9/6/2012
userid: tronbabylove
No collaborators

1. This is an executable file compiled from C.
2. The program displays 'enter serial number to continue:' to the user and waits for user input.
If the user enters 'zoidberg' the program outputs 'need a serial? why not zoidberg?'
If the user enters anything else, the program outputs 'your serial is bad, and you should feel bad.'
3. zoidberg
4. The program calls strcmp using the user's input and the char array pointed to by s2, which is visible in the data segment as 'zoidberg'
5. 48571
6. This program is vulnerable to a buffer overflow attack. The gets call is reading a char[] into stack memory, but no check is being performed on the bounds of the input. 
7. An attacker could exploit this by entering a string that overflows the buffer and overwrites the return address at the bottom the stack, causing the function to pass execution to an arbitrary function on return. We would most likely try to execute code that would spawn a shell. To do this, we would write a simple program that launches a shell and then disassemble it, obtaining the instructions in raw hex form. We would then store these instructions but add a "padding" of NOP instructions at the beginning of the buffer. The need for the NOP's arises from the fact that we don't know the address at which our buffer will be stored. We need to overwrite the return address that is on the stack with a value that points back into our overflowed buffer. The difficulty is predicting what the address of our buffer will be. By using the NOP sled, we increase our target area. If the return address that we overwrite points anywhere into the string of NOP's, they will be executed (with no effect) until execution reaches our shellcode instructions.
8. One way of protecting against this attack would be to use a "canary" variable. If we initialize a variable on the stack whose value we are certain should never change during a function's execution, then before we return from a function we can check the value of that variable. If the value has changed, then we know that a buffer was overflowed, and we should terminate execution of the program immediately. 

Source: Smashing the Stack for Fun and Profit: http://insecure.org/stf/smashstack.html 
