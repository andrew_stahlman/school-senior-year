/*
 * Author: Andrew Stahlman - aps0015@auburn.edu
 * userid: tronbabylove
 * No collaborators
 * October 4, 2012
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LOWER_RANGE_MIN 97
#define LOWER_RANGE_MAX 122
#define UPPER_RANGE_MIN 65
#define UPPER_RANGE_MAX 90
#define ALPHA_LEN 26

float freq_table_ref[ALPHA_LEN] = {
	8.2, 1.5, 2.8, 4.3, 12.7, 2.2, 2.0, 6.1, 7.0, 0.2, 0.8, 4.0, 2.4,
	6.7, 7.5, 1.9, 0.1, 6.0, 6.3, 9.1, 2.8, 1.0, 2.4, 0.2, 2.0, 0.1
};

int count_table[ALPHA_LEN];
float freq_table[ALPHA_LEN];
int total = 0;

void add_char(char c);
void print_table();
void get_freq();

int main() {
	FILE *fp = fopen("hw1_concat.txt", "r");
	if (fp == NULL) {
		perror("Error opening file.");
		return -1;
	}

	// init table
	int i;
	for (i = 0; i < ALPHA_LEN; i++) {
		count_table[i] = 0;
		freq_table[i] = 0;
	}
	
	char c;
	while ((c = fgetc(fp)) != EOF) {
		//printf("got char %c\n", c);
		add_char(c);
	}
	get_freq();
	print_table();
	fclose(fp);
	return 0;
}

void add_char(char c) {
	if (c >= UPPER_RANGE_MIN && c <= UPPER_RANGE_MAX) {
		//printf("adjusting upper case: %c\n", c);
		c += (LOWER_RANGE_MIN - UPPER_RANGE_MIN);
		//printf("adjusted to: %c\n", c);
	}
	if (c >= LOWER_RANGE_MIN && c <= LOWER_RANGE_MAX) {
		count_table[c - LOWER_RANGE_MIN] += 1;
		total++;
	} else {
		//printf("character is not in the alphabet: %c\n", c);
	}
}

void get_freq() {
	int i;
	for (i = 0; i < ALPHA_LEN; i++) {
		freq_table[i] = ((float)count_table[i]) / total;
		freq_table[i] *= 100;
	}
}

void print_table() {
	FILE *fp = fopen("hw4_output.txt", "w");

	puts("RESULTS");
	puts("-------------");
	int i = 0;
	float ftotal = 0, deviation = 0;
	for (i = 0; i < ALPHA_LEN; i++) {
		ftotal += freq_table[i];
		deviation = freq_table[i] - freq_table_ref[i];
		printf("%c: Frequency: %.3f; Deviation: %.3f\n", i + LOWER_RANGE_MIN, freq_table[i], deviation);
		fprintf(fp, "%c: Frequency: %.3f; Deviation: %.3f\n", i + LOWER_RANGE_MIN, freq_table[i], deviation);
	}
	puts("-------------");

	fclose(fp);
}
