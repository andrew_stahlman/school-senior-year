#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "md5/md5.h"

#define STRING_SIZE 10
#define FLAG_MARKER "POLY_FLAG:"
#define MIN_CHAR '!'
#define MAX_CHAR '~'

const char* DISPLAY_MSG = "The %s hash value is %s - Andrew Stahlman\n";

void print_hash(unsigned char* input, unsigned int len, char* identifier_string);
void mutate_string(char* new, char* original);
int find_flag(unsigned char* file, int len);

int main(int argc, char* argv[]) {
	srand(time(NULL));
	printf("File: %s\n", argv[0]);
	char* flag = "POLY_FLAG:0123456789";
	FILE* fp = fopen(argv[0], "r+");
	//FILE* fp = fopen(argv[0], "r+b");

	long size = fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	rewind(fp);

	unsigned char* buffer = (unsigned char*)malloc(size);
	size_t result = fread(buffer, 1, size, fp);
	if (result != size) {
		puts("Error reading file. Exiting...");
		exit(1);
	}


	print_hash(buffer, (unsigned int)result, "original");

	int offset = find_flag(buffer, result) + strlen(FLAG_MARKER);
	fseek(fp, offset, SEEK_SET);
	char* original_flag= (char*)malloc(STRING_SIZE + 1);
	fread(original_flag, 1, STRING_SIZE, fp);
	//printf("original_flag: %s\n", original_flag);

	fseek(fp, STRING_SIZE * -1, SEEK_CUR);
	char* new_flag = (char*)malloc(STRING_SIZE + 1);
	mutate_string(new_flag, original_flag);
	//printf("new_flag: %s\n", new_flag);
	fwrite(new_flag, 1, STRING_SIZE, fp);
	fflush(fp);

	rewind(fp);
	size_t old_size = result;
	result = fread(buffer, 1, size, fp); //size is old size
	if (result != old_size) {
		puts("File size changed. Exiting...\n");
		exit(1);
	}
	print_hash(buffer, (unsigned int)result, "modified");
	return 0;
}

/*
 * md5 hash by L. Peter Deutsch. http://sourceforge.net/projects/libmd5-rfc/files/
 */
void print_hash(unsigned char* input, unsigned int len, char* identifier_string) {

	md5_state_t state;
	md5_byte_t digest[16];
	char hex_output[16*2 + 1];
	int di;

	md5_init(&state);
	md5_append(&state, (const md5_byte_t *)input, len);
	md5_finish(&state, digest);
	for (di = 0; di < 16; ++di) {
	    sprintf(hex_output + di * 2, "%02x", digest[di]);
	}

	printf(DISPLAY_MSG, identifier_string, hex_output);
}

void mutate_string(char* new, char* original) {
	short i;
	for (i = 0; i < STRING_SIZE; i++) {
		new[i] = original[i] + 1;
		if (new[i] > MAX_CHAR) {
			new[i] = MIN_CHAR;
		}
	}
}

int find_flag(unsigned char* file, int len) {
	int i, m = 0;
	while (i < len) {
		i++;
		while ((char)file[i + m] == FLAG_MARKER[m]) {
			//printf("MATCH: m = %d, %c\n", m, file[i + m]);
			m++;
			if (m == STRING_SIZE) {
				return i;
			}
		}
		m = 0;
	}
	return 0;
}
