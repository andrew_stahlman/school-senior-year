#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "md5-c/global.h"
#include "md5-c/md5.h"

void do_hash(unsigned char* input, unsigned int len);

int main(int argc, char* argv[]) {
	printf("File: %s\n", argv[0]);
	char* flag = "POLY_FLAG:0123456789";
	FILE* fp = fopen(argv[0], "rb");

	long size = fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	rewind(fp);

	unsigned char* buffer = (unsigned char*)malloc(size);
	size_t result = fread(buffer, 1, size, fp);
	if (result != size) {
		puts("Error reading file. Exiting...");
		exit(1);
	}
	
	do_hash(buffer, (unsigned int)result);
	return 0;
}

void do_hash(unsigned char* input, unsigned int len) {
	MD5_CTX context;
	unsigned char digest[16];

	MD5Init (&context);
	MD5Update (&context, input, len);
	MD5Final (digest, &context);

	printf ("MD5 (\"%s\") = ", input);
	unsigned int i;
	for (i = 0; i < 16; i++) {
		printf ("%02x", digest[i]);
	}
	printf ("\n");
}
