#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hash.h"

#define BUFF_SIZE 10 << 20 // 10 megabytes

int main(int argc, char* argv[]) {
	byte buff[BUFF_SIZE];
	size_t sz;
	char hex_result[128];

	if (argc > 1) {
		FILE* fp;
		fp = fopen(argv[1], "rb");
		if (fp) {
			sz = fread(buff, 1, BUFF_SIZE, fp);
		}
		else {
			puts("Error opening file. Exiting...");
			exit(0);
		}
	} else {
		char stdin_buff[BUFF_SIZE];
		fgets(stdin_buff, BUFF_SIZE, stdin);
		sz = strlen(stdin_buff);
		memcpy(buff, stdin_buff, sz);
	}
	hash(buff, sz - 1, hex_result);
	printf("%s\n", hex_result);
}
