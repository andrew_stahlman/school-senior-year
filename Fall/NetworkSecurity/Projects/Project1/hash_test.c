#include "hash.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#define NUM_TESTS 25
#define BUFF_SIZE 250

int load_test_cases(byte inputs[][BUFF_SIZE], char outputs[][BUFF_SIZE], int in_lens[]);

int main() {
	byte inputs[NUM_TESTS][BUFF_SIZE];
	char expected[NUM_TESTS][BUFF_SIZE];
	int in_lens[NUM_TESTS];
	int num_tests = load_test_cases(inputs, expected, in_lens);

	short i = 0;
	for (i = 0; i < num_tests; i++) {
		char res[BUFF_SIZE];
		hash(inputs[i], in_lens[i], res);
		if (strcmp(res, expected[i]) != 0) {
			printf("Test %d failed.\nExp.: %s\nAct.: %s\n", i, expected[i], res);
		} else {
			printf("Test %d passed!\nExp.: %s\nAct.: %s\n", i, expected[i], res);
		}
	}
	return 0;
}

int load_test_cases(byte inputs[][BUFF_SIZE], char outputs[][BUFF_SIZE], int in_lens[]){
	FILE *fp;
	unsigned char buff[BUFF_SIZE];
	size_t sz;

	fp = fopen("test_inputs.txt", "rb");
	if (fp)
	{
		sz = fread(buff, 1, BUFF_SIZE, fp);
	}
	else
	{
		puts("Error opening file. Exiting...");
		exit(0);
	}
	
	printf("sz=%d\n", sz);
	
	int start, end;
	int in_i, out_i;
	while (end < sz) {
		if (buff[end] == '\n') {
			puts("Found newline\n");
			if (in_i > out_i) {
				memcpy(&outputs[out_i], &buff[start], end - start);
				out_i++;
			} else {
				memcpy(&inputs[in_i], &buff[start], end - start);
				in_lens[in_i] = end - start;
				in_i++;
			}
			start = end + 1;
		}
		end++;
	}

	puts("Printing inputs");
	int j;
	char in_hex[BUFF_SIZE >> 1];
	for (j = 0; j < in_i; j++) {
		sprintf_hex(in_hex, inputs[j], in_lens[j]);
		printf("Input %d: %s\n", j, in_hex);
	}

	puts("Printing expected outputs");
	for (j = 0; j < out_i; j++) {
		printf("Output %d: %s\n", j, outputs[j]);
	}

	return out_i;
}
