#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hash.h"

#define HASH_LEN 32

const char KEY[] = "Hamilton's awesome hash function";

void hash(byte input[], int in_len, char res[]) {
	if (in_len == 0 || in_len % HASH_LEN) {
		int len = HASH_LEN - (in_len % HASH_LEN);
		memcpy(&input[in_len], KEY, len);
		in_len += len;
	}

	byte hash[HASH_LEN];
	memset(hash, 0, HASH_LEN);
   	memcpy(hash, KEY, HASH_LEN);
	byte block_32[HASH_LEN];
	int num_blocks = in_len / HASH_LEN;
	int b, h, i;
	for (b = 0; b < num_blocks; b++) {
		memcpy(block_32, &input[HASH_LEN * b], HASH_LEN);
		for (h = 0; h < HASH_LEN; h++) {
			hash[h] ^= block_32[h];
		}
		if (hash[0] % 2) {
			for (i = 0; i < HASH_LEN; i++) {
				hash[h] = (hash[h] + 42) % 256;
			}
		}
		if (hash[1] % 3) {
			byte rem[HASH_LEN - 10];;
			memcpy(rem, &hash[10], HASH_LEN - 10);
			memcpy(&hash[HASH_LEN - 10], hash, 10);
			memcpy(hash, rem, HASH_LEN - 10);
		}
	}
	sprintf_hex(res, hash, HASH_LEN);
}

void sprintf_hex(char res[], byte b[], int len) {
	int i = 0;
	while (i < len) {
		sprintf(&res[i * 2], "%02x", b[i]);
		i++;
	}
}
