#include <stdio.h>
#include "encrypt.h"

int main() {
	unsigned long *blocks;
	load_blocks("plaintext.txt", &blocks);
	encrypt(blocks);
	return 0;
}

int load_blocks(char* filepath, unsigned long **blocks) {
	FILE *fp = fopen(filepath, "rb");
	if (fp == NULL) {
		puts("Error opening file. Exiting...");
		exit(0);
	}

	fseek(fp, 0, SEEK_END);
	long fileSize = ftell(fp);
	rewind(fp);

	int numBlocks = fileSize / (sizeof(unsigned long) + 1;
	*blocks = (unsigned long*) malloc(fileSize / numBlocks);

	fread(*blocks, sizeof(unsigned long), numBlocks, fp);
	fclose(fp);
}
