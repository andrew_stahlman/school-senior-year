Homework:
		CPU1	CPU2
APP1	100		200
APP2	500		50

Approach 1: Get overall performance, then speedup
Approach 2: Get speedup from app1 perspective, then speedup from app2 perspective. Then apply weight for overall. *(Use this and justify w/ one sentence).

Project:
Use C, no objects, only functions.
Don't forget make file.
Use singly linked lists for practice instead of arrays (but not for memory).
Use malloc and a lot of pointers.

Segmenting approach: 
	1 array, split into 4 parts. 
	4 separate arrays

The quadratic_eval.s is not compatible with our accum and stack-based machines. We need to translate it manually.
Read some spim source code.
General Tip: Only read the header files, data structures, function prototypes.
TODO: Read some Linux kernel source.
Use hashing if possible. Look it up. (for address translation)

