#include <stdint.h>
#define TRUE 1
#define FALSE 0

#define TEXT_MIN 0x400000
#define DATA_MIN 0x10000000
#define STACK_MAX 0x7fffffff

typedef uint32_t mem_addr, uword;
typedef uint8_t byte;
typedef int32_t word;
typedef char bool;

void init_addr_space();
byte* addr_access(mem_addr loc);
bool read_word(mem_addr loc, word* w);
bool write_word(mem_addr loc, word w);

bool stack_push(mem_addr m);
bool stack_pop(mem_addr m);
bool stack_add();
bool stack_mult();

void accum_load(mem_addr m);
void accum_store(mem_addr m);
void accum_add(mem_addr m);
void accum_mult(mem_addr m);
