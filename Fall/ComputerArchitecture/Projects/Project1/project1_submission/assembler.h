#include "mem.h"
#include <stdio.h>

#define STACK_MODE 1
#define ACCUM_MODE 2

#define SYM_BUCKETS 10
#define MAX_REFERENCES 50
#define MAX_VAL_LEN 256

#define INVALID_OP -1
#define PUSH_OP 0
#define POP_OP 1
#define PUSHIMM_OP 2

#define LOAD_OP 0
#define STO_OP 1
#define LOADIMM_OP 2

#define ADD_OP 3
#define MULT_OP 4
#define END_OP 5


struct instr {
	char* op_code;
	uint8_t bin_op_code;
	mem_addr operand;
};

typedef struct instr instr;


struct sym_table_ent {
	char* name;
	mem_addr addr;
	struct sym_table_ent *next;
	instr* ref_instr[MAX_REFERENCES];
	short num_ref_instr;
};

typedef struct sym_table_ent sym_table_ent;

typedef short status_code;

status_code load_program(FILE* file, mem_addr *data_ctr, mem_addr *text_ctr, short mode);
