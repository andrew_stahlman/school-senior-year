#include "mem.h"

#define TEXT_MIN 0x400000
#define DATA_MIN 0x10000000
#define STACK_MAX 0x7fffffff

#define SYM_BUCKETS 10

struct sym_table_ent {
	char* name;
	mem_addr addr;
	sym_table_ent *next;
};

typedef struct sym_table_ent sym_table_ent;

struct instr {
	char* op_code;
	sym_table_ent *operand;
};

typedef struct instr instr;

// djb2 string hashing via: http://www.cse.yorku.ca/~oz/hash.html
unsigned long hash(char *s); 
void add_sym(sym_table_ent table[], char *name);
sym_table_ent* lookup_sym(sym_table_ent *table[], char *name);
mem_range* load_program(char *stream);
