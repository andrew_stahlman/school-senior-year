#include "assembler.h"
#include "logger.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	mem_addr text_top = TEXT_MIN;
	mem_addr data_top = DATA_MIN;
	//TODO: cmd line params
	FILE *fp = fopen("accumCode", "r");
	load_program(fp, &data_top, &text_top);
	mem_addr pc = TEXT_MIN;
	instr *next_instr = (instr*)addr_access(pc);
	bool user_mode = TRUE;
	while (user_mode && pc < text_top) {
		LOG_PRINT("Next instruction: %s", next_instr->op_code);
		if (strcmp(next_instr->op_code, "load") == 0) {
			LOG_PRINT("Got load command\n");
			accum_load(next_instr->operand);
		} else if (strcmp(next_instr->op_code, "sto") == 0) {
			LOG_PRINT("Got sto command\n");
			accum_store(next_instr->operand);
		} else if (strcmp(next_instr->op_code, "add") == 0) {
			LOG_PRINT("Got add command\n");
			accum_add(next_instr->operand);
		} else if (strcmp(next_instr->op_code, "mult") == 0) {
			LOG_PRINT("Got mult command\n");
			accum_mult(next_instr->operand);
		} else if (strcmp(next_instr->op_code, "end") == 0) {
			LOG_PRINT("Got end command\n");
			user_mode == FALSE;
		}
		pc += sizeof(instr);
		next_instr = (instr*)addr_access(pc);
	}
	word result;
	read_word(DATA_MIN, &result);
	printf("Finished run. The result is: %d\n", result);
}

