#include "mem.h"
#include <stdio.h>
#define SYM_BUCKETS 10
#define MAX_REFERENCES 50
#define MAX_VAL_LEN 256

struct instr {
	char* op_code;
	mem_addr operand;
};

typedef struct instr instr;


struct sym_table_ent {
	char* name;
	mem_addr addr;
	struct sym_table_ent *next;
	instr* ref_instr[MAX_REFERENCES];
	short num_ref_instr;
};

typedef struct sym_table_ent sym_table_ent;

typedef short status_code;

status_code load_program(FILE* file, mem_addr *data_ctr, mem_addr *text_ctr);
