#include "mem.h"
#include <stdlib.h>
#include <stdio.h>

void do_assert(word expected, word actual, char* msg) {
	if (expected != actual) {
		printf("Error: %s", msg);
		exit(1);
	}
	puts("Assertion ok.");
}

int main() {
	init_addr_space();
	word s_word = -56;
	word u_word = DATA_MIN;
	word val;
	mem_addr loc = DATA_MIN;
	
	write_word(loc, s_word);
	read_word(loc, &val);
	do_assert(s_word, val, "Test 1 Failed.");
	loc += sizeof(word);

	write_word(loc, u_word);
	read_word(loc, &val);
	do_assert(u_word, val, "Test 2 Failed.");

	// val is a pointer to DATA_MIN
	read_word((mem_addr)val, &val);
	do_assert(s_word, val, "Pointer test failed.");
	return 0;
}
