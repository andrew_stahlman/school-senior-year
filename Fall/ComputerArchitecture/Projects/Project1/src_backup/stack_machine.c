#include "assembler.h"
#include "logger.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	if (argc != 2) {
		puts("Usage: filename");
		return -1;
	}

	FILE *fp = fopen(argv[1], "r");
	if (fp == 0) {
		printf("Could not read from source file %s. Exiting.\n", argv[1]);
		return -1;
	}

	mem_addr text_top = TEXT_MIN;
	mem_addr data_top = DATA_MIN;
	// text_top is set to the address of the end of the last loaded instruction
	// data_top is set to the address of the end of the last word we init 
	if (load_program(fp, &data_top, &text_top) != 0) {
		printf("Error loading program %s. Exiting...\n", argv[1]);
		return -1;
	}
	LOG_PRINT("Finished loading program.\n");

	mem_addr pc = TEXT_MIN;
	instr *next_instr = (instr*)addr_access(pc);
	bool user_mode = TRUE;
	while (user_mode && pc < text_top) {
		LOG_PRINT("Next instruction: %s", next_instr->op_code);
		if (strcmp(next_instr->op_code, "push") == 0) {
			LOG_PRINT("Got push command\n");
			stack_push(next_instr->operand);
		} else if (strcmp(next_instr->op_code, "pop") == 0) {
			LOG_PRINT("Got pop command\n");
			stack_pop(next_instr->operand);
		} else if (strcmp(next_instr->op_code, "add") == 0) {
			LOG_PRINT("Got add command\n");
			stack_add();
		} else if (strcmp(next_instr->op_code, "mult") == 0) {
			LOG_PRINT("Got mult command\n");
			stack_mult();
		} else if (strcmp(next_instr->op_code, "end") == 0) {
			LOG_PRINT("Got end command\n");
			user_mode == FALSE;
		}
		pc += sizeof(instr);
		next_instr = (instr*)addr_access(pc);
	}

	word result;
	read_word(DATA_MIN, &result);
	printf("Finished execution. The value stored in RESULT is: %d\n", result);

}

