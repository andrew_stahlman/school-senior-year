#include <stdio.h>
#include "mem_mgr.h"
#include "mem.h"
#include "string.h"

#define BITS 32
#define FAIL 0

typedef short status_code;

mem_addr data_ctr = DATA_MIN;
mem_addr text_ctr = TEXT_MIN;
mem_addr stack_ctr = STACK_MAX;

// NOTE: When loading symbols, each symbol should represent one memory address, so one byte?
// OR: Should we use words? 1 word = 4 bytes (Definitely use this approach to store instructions.)
status_code load_program(FILE* file, mem_range **addr_space) {
	// initialize the address space
	mem_range *mem = (mem_range*) malloc(sizeof(mem_range));
	init_addr_space(mem, BITS);
	*addr_space = mem;
	
	// init symbol table
	sym_table_entry* sym_table[SYM_BUCKETS];

	char* line;
	char* word1, word2;
	int scan_count;
	const short data_mode = 1;
	const short text_mode = 2;
	short cur_mode = 0;
	while (fgetline(*file, line)) {
		scan_count = sscanf(line, "%s %s", word1, word2);
		if (scan_count == 1) {
			if (strcmp(word1, ".data") == 0) {
				cur_mode = data_mode;
			} else if (strcmp(word1, ".text") == 0) {
				cur_mode = text_mode;
			} else {
				return FAIL;
			}
		} else if (scan_count == 2) {
			if (cur_mode == text_mode) {
				instr* i = (instr*)malloc(sizeof(instr));
				strcpy(i->op_code, word1);
				// TODO: check op_code
				i->operand = lookup_sym(&sym_table, word2);
				if (i->operand == 0) {
					return FAIL;
				}
				// add the instruction to the text segment
				// TODO: need an instruction to write bit-by-bit
				// TODO: or use a fixed-size data type instead of byte
				// text_ctr += sizeof(instr);
			} else if (cur_mode == data_mode) {
				// TODO: pick width
				//short width = sizeof(*type*);
				// convert word2 to suitable data type
				// *type* stored_val = (*type*)word2;
				// for i in width...
				addr_write(mem, stored_val, data_ctr);
				add_sym(&table, word1, width, data_ctr);
				//data_ctr += width;
			}
			else {
				return FAIL;
			}
		}
	}
	// open the file for reading
	// parse code
	// 	add symbols to table
	// 	add instr. to text segment

}

unsigned long hash(char* name) {
        unsigned long hash = 5381;
        int c;
        while (c = *name++) {
            hash = ((hash << 5) + hash) + c; // hash * 33 + c
		}
        return hash;
}

void add_sym(sym_table_ent *table[], char *name, short width, mem_addr addr) {
	short bucket = (short) hash(name) % SYM_BUCKETS;
	sym_table_ent *ent = (sym_table_ent*)malloc(sizeof(sym_table_ent));
	ent->name = name;
	ent->width = width;
	ent->mem_addr = addr;
	sym_table_ent **n = &table[bucket];
	while (*n != 0) {
		n = &((*n)->next);
	}
	*n = ent;
}

sym_table_ent* lookup_sym(sym_table_ent *table[], char *name) {
	short bucket = (short) hash(name) % SYM_BUCKETS;
	sym_table_ent *n = table[bucket];
	while (n->next != 0) {
		if (strcmp(name, n->name) == 0) {
			return n;
		}
	}
	return 0;
}

