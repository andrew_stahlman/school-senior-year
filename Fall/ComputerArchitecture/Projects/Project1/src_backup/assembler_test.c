#include "assembler.h"
#include "logger.h"
#include <stdio.h>
#include <stdlib.h>

int main() {
	mem_addr text_ctr = TEXT_MIN;
	mem_addr data_ctr = DATA_MIN;
	FILE* fp = fopen("test_source.s", "r");
	load_program(fp, &data_ctr, &text_ctr);
	LOG_PRINT("Done loading program\n");
	mem_addr pc = TEXT_MIN;
	instr *i = (instr*)addr_access(pc);
	while (pc < text_ctr) {
		word operand_val;
	   	read_word(i->operand, &operand_val);
		LOG_PRINT("Read operand_val.\n");
		LOG_PRINT("instruction: %s %d(=%d)\n", i->op_code, i->operand, operand_val);
		pc += sizeof(instr);
		i = (instr*)addr_access(pc);
	}
	return 0;
}
