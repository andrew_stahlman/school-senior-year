#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "logger.h"
#include "assembler.h"

#define FAIL -1
#define OK 0
#define WORD_LEN 64
#define REMAINDER_LEN 256

unsigned long hash(char* name);
sym_table_ent* add_sym(sym_table_ent *table[], char *name);
void lookup_sym(sym_table_ent *table[], char *name, sym_table_ent** e);

static mem_addr text_ctr = TEXT_MIN;

status_code load_program(FILE* file, mem_addr *data_ctr, mem_addr *text_ctr) {

	// initialize the address space
	init_addr_space();

	// init symbol table
	sym_table_ent* sym_table[SYM_BUCKETS];
	memset(&sym_table[0], 0, sizeof(sym_table_ent*) * SYM_BUCKETS);

	// Pass 1: Fill out the symbol table,
	// create an array of instructions and data declarations
	char* line = (char*) malloc(MAX_VAL_LEN);
	char word1[WORD_LEN], word2[WORD_LEN], remainder[REMAINDER_LEN];
	int scan_count;
	const short data_mode = 1;
	const short text_mode = 2;
	short cur_mode = 0;
	size_t len;
	while (getline(&line, &len, file) != -1) {
		scan_count = sscanf(line, "%s %s %1023[^\n]", word1, word2, remainder);
		LOG_PRINT("Scanned this line: 1: %s 2: %s 3: %s\n", word1, word2, remainder);
		if (scan_count < 1) {
			continue;
		}

		if (strcmp(word1, ".data") == 0) {
			cur_mode = data_mode;
			LOG_PRINT("Switched to data mode\n");
		} else if (strcmp(word1, ".text") == 0) {
			cur_mode = text_mode;
			LOG_PRINT("Switched to text mode\n");
		} else if (cur_mode == text_mode) {
			instr* next_instr = (instr*)addr_access(*text_ctr);
			LOG_PRINT("Next instr at %d\n", next_instr);
			*text_ctr += sizeof(instr);
			next_instr->op_code = (char*)malloc(WORD_LEN);
			strcpy(next_instr->op_code, word1);
			LOG_PRINT("next_instr->op_code = %s\n", next_instr->op_code);
			// TODO: check op_code

			if (scan_count > 1) {
				// we have a mem_addr as an operand
				sym_table_ent* entry;
				lookup_sym(&sym_table[0], word2, &entry);
				if (entry == 0) {
					entry = add_sym(&sym_table[0], word2);
				}
				entry->ref_instr[entry->num_ref_instr] = next_instr;
				entry->num_ref_instr++;
			} else {
				next_instr->operand = 0;
			}

		} else if (cur_mode == data_mode) {
			sym_table_ent* entry;
			lookup_sym(&sym_table[0], word1, &entry);
			if (entry == 0) {
				entry = add_sym(&sym_table[0], word1);
			}
			entry->addr = *data_ctr;
			if (strcmp(word2, ".word") == 0) {
				int v = atoi(remainder); // TODO: check input
				write_word(*data_ctr, (word)v);
				LOG_PRINT("type=WORD, val=%d\n", v);
			} else if (strcmp(word2, ".ascii") == 0) {
				// store a pointer to the string
				char *c = (char*)malloc(strlen(remainder));
				strcpy(c, remainder);
				write_word(*data_ctr, (mem_addr)c);
				LOG_PRINT("type=ASCII, val=%d\n", c);
			} else {
				return FAIL;
			}
			*data_ctr += sizeof(word);
		}
		memset(word1, 0, WORD_LEN);
		memset(word2, 0, WORD_LEN);
		memset(remainder, 0, REMAINDER_LEN);
	}

	// for each entry in the symbol table, assign a mem_addr
	// and substitute the mem_addr in each instruction and 
	// data declaration that references the symbol
	short b = 0, r = 0;
	sym_table_ent* ent;
	for (b = 0; b < SYM_BUCKETS; b++) {
		ent = sym_table[b];
		while (ent != 0) {
			for (r = 0; r < ent->num_ref_instr; r++) {
				instr* i = ent->ref_instr[r];
				i->operand = ent->addr;
			}
			ent = ent->next;
		}
	}
	return OK;
}

// djb2 string hashing via: http://www.cse.yorku.ca/~oz/hash.html
unsigned long hash(char* name) {
        unsigned long hash = 5381;
        int c;
        while (c = *name++) {
            hash = ((hash << 5) + hash) + c; // hash * 33 + c
		}
        return hash;
}

sym_table_ent* add_sym(sym_table_ent *table[], char *name) {
	unsigned short bucket = (unsigned short) hash(name) % SYM_BUCKETS;
	sym_table_ent *ent = (sym_table_ent*)malloc(sizeof(sym_table_ent));
	ent->name = name;
	sym_table_ent **n = &table[bucket];
	while (*n != 0) {
		n = &((*n)->next);
	}
	*n = ent;
	return ent;
}

void lookup_sym(sym_table_ent *table[], char *name, sym_table_ent** e) {
	unsigned short bucket = (unsigned short) hash(name) % SYM_BUCKETS;
	*e = table[bucket];
	while (*e != 0 && (*e)->next != 0) {
		if (strcmp(name, (*e)->name) == 0) {
			return;
		} else {
			*e = (*e)->next;
		}
	}
}

