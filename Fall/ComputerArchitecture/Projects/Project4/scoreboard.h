#ifndef SCOREBOARD_H 
#define SCOREBOARD_H
// we need the bool type from mem.h
#include "mem.h"
#include "instr.h"

#define UNDEFINED -1

#define NUM_STAGES 4
#define ISSUE 0
#define READ_OPS 1
#define EXEC_COMPL 2
#define WB 3

#define NUM_FU 4
#define FU_INT 0
#define FU_FLOAT1 1
#define FU_FLOAT2 2
#define FU_MEM 3

#define NUM_REGS 48 // 16 floating point + 32 integer
#define NUM_INT_REGS 32
#define NUM_FLOAT_REGS 16

struct instr_status_t { 
	uint8_t bin_op_code;
	short id; // TODO: We should probably just store the instr. pointer
	uint8_t i; // dest reg
	uint8_t j; // rs
	uint8_t k; // rt
	short completion_times[NUM_STAGES];
	struct instr_status_t* next;
};
typedef struct instr_status_t instr_status_t;

struct fu_status_t {
	short time_left;
	bool is_busy;
	uint8_t bin_op_code;
	short Fi;
	short Fj;
	short Fk;
	short Qj;
	short Qk;
	bool Rj;
	bool Rk;
};
typedef struct fu_status_t fu_status_t;


struct scoreboard_t {
	short num_issued;
	//instr_status_t instr_status[INSTR_STATUS_BUFF_SIZE]; // TODO:linked list 
	instr_status_t* instr_status;
	fu_status_t fu_status[NUM_FU];
	short reg_result_status[NUM_REGS];
};
typedef struct scoreboard_t scoreboard_t;

struct pre_ex_latch_t {
	word op_A;
	word op_B;
	double f_op_A;
	double f_op_B;
	instr* instruction;
	short syscall_op;
	short syscall_print_int_val;
	//mem_addr syscall_str_loc;
	//short syscall_str_len;
};

typedef struct pre_ex_latch_t pre_ex_latch_t;

struct post_ex_latch_t {
	bool is_ready;
	instr* instruction;
	uint8_t rd;
	word result;
	double f_result;
};

typedef struct post_ex_latch_t post_ex_latch_t;

struct issued_buffer_node {
	instr* i;
	struct issued_buffer_node* next;
};
typedef struct issued_buffer_node issued_buffer_node;

struct issued_buffer {
	issued_buffer_node* head;
	issued_buffer_node* tail;
	short count;
};
typedef struct issued_buffer issued_buffer;

void print_scoreboard(scoreboard_t score);

bool instr_issue(mem_addr text_top, scoreboard_t old_score, scoreboard_t* new_score, issued_buffer* issued);
void read_operands(scoreboard_t old_score, scoreboard_t* new_score, issued_buffer old_issued, issued_buffer* new_issued, pre_ex_latch_t latch_out[]);
void fu_int(scoreboard_t old_score, scoreboard_t* new_score, pre_ex_latch_t latch_in, post_ex_latch_t* latch_out, bool* user_mode);
void fu_float_add(scoreboard_t old_score, scoreboard_t* new_score, pre_ex_latch_t latch_in, post_ex_latch_t* latch_out);
void fu_float_mul(scoreboard_t old_score, scoreboard_t* new_score, pre_ex_latch_t latch_in, post_ex_latch_t* latch_out);
void fu_mem(scoreboard_t old_score, scoreboard_t* new_score, pre_ex_latch_t latch_in, post_ex_latch_t* latch_out);
short write_back(scoreboard_t old_score, scoreboard_t* new_score, post_ex_latch_t latch_in[]);

bool remove_instr(issued_buffer* buffer, instr* i);
void add_instr(issued_buffer* buffer, instr* i);

void execute(mem_addr text_top);
void init_fu_status(fu_status_t* fu_status);
void init_scoreboard(scoreboard_t* score);
#endif
