#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include <stdio.h>
#include "instr.h"

#define STACK_MODE 1
#define ACCUM_MODE 2

#define SYM_BUCKETS 10
#define MAX_REFERENCES 50
#define MAX_VAL_LEN 256

struct sym_table_ent {
	char* name;
	mem_addr addr;
	struct sym_table_ent *next;
	instr* ref_instr[MAX_REFERENCES];
	short num_ref_instr;
};

typedef struct sym_table_ent sym_table_ent;

typedef short status_code;

status_code load_program(FILE* file, mem_addr *data_ctr, mem_addr *text_ctr, short mode);
void test_assembler();
#endif
