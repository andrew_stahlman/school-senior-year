.data
KX: .dword 1.65
X2: .dword -.57
X1: .dword 0
KY: .dword .8
Y2: .dword -.92
Y1: .dword 0
X_RES: .dword 0
Y_RES: .dword 0

.text

# kx = 0
# ky = 1
# X1 = 2
# X2 = 3
# Y1 = 4
# Y2 = 5

main:

# initialize float regs
la $31, KX
ld $f0, 0($31)
la $31, KY 
ld $f1, 0($31)
la $31, X1
ld $f2, 0($31)
la $31, X2
ld $f3, 0($31)
la $31, Y1
ld $f4, 0($31)
la $31, Y2
ld $f5, 0($31)

# set loop counter
li $1, 99
li $0, 9

#la $f13, X_RES # this should be here, but it causes a crash
#la $f14, Y_RES # this should be here, but it causes a crash

# f10 = X0
# f11 = Y0
loop:
fsub $f12, $f2, $f3 #X1 - X2
fmul $f10, $f0, $f12 # kx * X1 - X2
subi $1, $1, 1
fsub $f12, $f4, $f5 #Y1 - Y2
fmul $f11, $f1, $f12 # ky * Y1 - Y2
addi $f3, $f2, 0 #X2 = X1
addi $f2, $f10, 0 #X1 = X0
addi $f5, $f4, 0 #Y2 = Y1
addi $f4, $f11, 0 #Y1 = Y0
#sd $f13, 0($f10) # this should be here, but it causes a crash
#sd $f14, 0($f11) # this should be here, but it causes a crash
subi $1, $1, 1
bge $1, $0, loop
nop
nop
