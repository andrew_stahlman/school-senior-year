#include "mem.h"

#define INVALID_OP -1

// stack ops
#define PUSH_OP 0
#define POP_OP 1
#define PUSHIMM_OP 2

// accum ops
#define LOAD_OP 3
#define STO_OP 4
#define LOADIMM_OP 5

// stack and accum ops
#define ADD_OP 6
#define MULT_OP 7
#define END_OP 8

// reg ops
#define ADDI_OP 9
#define B_OP 10
#define BEQZ_OP 11
#define BGE_OP 12
#define BNE_OP 13
#define LA_OP 14
#define LB_OP 15
#define LI_OP 16
#define SUBI_OP 17
#define SYSCALL_OP 18

#define NUM_INSTR_T 19
#define MAX_OPERANDS 3

// operands
#define UNUSED -1
#define RS 0
#define RT 1
#define RD 2
#define IMM 3
#define LABEL 4
#define REG_OFF 5
#define MEM_ADDR 6

struct instr {
	char* op_code;
	uint8_t bin_op_code;

	char* label;
	mem_addr operand;

	uint8_t rd;
	uint8_t rt;
	uint8_t rs;

	int32_t imm;
};

typedef struct instr instr;

void test_instr();
void parse_instr(char* text, instr **i);
