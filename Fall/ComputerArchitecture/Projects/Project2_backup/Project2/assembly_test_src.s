.data
ONE:	.word 	1
ZERO:	.word 	0
NEG_ONE: 	.word 	-1
STRING1:	.ascii	"This is string 1\n"
STRING2:	.ascii	"This is string 2\n"
FAIL_STRING:	.ascii	"Failure\n"
SPACE:	.space	1024

.text
REG:
	li $1, 0
	li $2, 4
	addi, $1, $2, 1 # test $1 == 5
	li $3, 5
	bne $3, $5, 9	#9 instructions until FAIL
	la $2, NEG_ONE
	subi $1, $1, 1 # test $1 == -2
	la $3, -2
	bne $1, $3, 5	# 5 instructions until FAIL
BRANCH_I:
	b P_STRING1 # test string1 printed and string2 is not
P_STRING2:
	la $0, STRING2
P_STRING1:
	la $0, STRING1
DO_PRINT:
	syscall
	EXIT
FAIL:
	la $0, FAIL_STRING
	syscall
EXIT:
	exit

#test SPACE[0] - SPACE[1023] == 0
#test ONE, ZERO, NEG_ONE == 1, 0, -1
