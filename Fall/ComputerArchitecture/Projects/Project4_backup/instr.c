#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "logger.h"
#include "instr.h"

#define BUFF 128
#define OP_COUNT MAX_OPERANDS

#define TRUE 1
#define FALSE 0

static uint8_t instr_format[NUM_INSTR_T][MAX_OPERANDS + 1];
static char* instr_names[NUM_INSTR_T];
static short needs_init = TRUE;

static void init_instr_format() {

	// add $rd, $rs, $rt
	instr_format[ADD_OP][0] = RD;
	instr_format[ADD_OP][1] = RS;
	instr_format[ADD_OP][2] = RT;
	instr_format[ADD_OP][OP_COUNT] = 3;

	// addi $rd, $rs, C
	instr_format[ADDI_OP][0] = RD;
	instr_format[ADDI_OP][1] = RS;
	instr_format[ADDI_OP][2] = IMM;
	instr_format[ADDI_OP][OP_COUNT] = 3;
	
	// b label
	instr_format[B_OP][0] = LABEL;
	instr_format[B_OP][1] = UNUSED;
	instr_format[B_OP][2] = UNUSED;
	instr_format[B_OP][OP_COUNT] = 1;

	// beqz $rs, label
	instr_format[BEQZ_OP][0] = RS;
	instr_format[BEQZ_OP][1] = LABEL;
	instr_format[BEQZ_OP][2] = UNUSED;
	instr_format[BEQZ_OP][OP_COUNT] = 2;
	
	// bge $rs, $rt, label
	instr_format[BGE_OP][0] = RS;
	instr_format[BGE_OP][1] = RT;
	instr_format[BGE_OP][2] = LABEL;
	instr_format[BGE_OP][OP_COUNT] = 3;

	// bne $rs, $rt, label 
	instr_format[BNE_OP][0] = RS;
	instr_format[BNE_OP][1] = RT;
	instr_format[BNE_OP][2] = LABEL;
	instr_format[BNE_OP][OP_COUNT] = 3;

	// la $rd, label
	instr_format[LA_OP][0] = RD;
	instr_format[LA_OP][1] = LABEL;
	instr_format[LA_OP][2] = UNUSED;
	instr_format[LA_OP][OP_COUNT] = 2;

	// lb $rd, Imm($rs)
	instr_format[LB_OP][0] = RD;
	instr_format[LB_OP][1] = REG_OFF;
	instr_format[LB_OP][2] = UNUSED;
	instr_format[LB_OP][OP_COUNT] = 2;

	// li $rd, Imm
	instr_format[LI_OP][0] = RD;
	instr_format[LI_OP][1] = IMM;
	instr_format[LI_OP][2] = UNUSED;
	instr_format[LI_OP][OP_COUNT] = 2;

	// subi $rd, $rs, Imm
	instr_format[SUBI_OP][0] = RD;
	instr_format[SUBI_OP][1] = RS;
	instr_format[SUBI_OP][2] = IMM;
	instr_format[SUBI_OP][OP_COUNT] = 3;

	// syscall
	instr_format[SYSCALL_OP][0] = UNUSED;
	instr_format[SYSCALL_OP][1] = UNUSED;
	instr_format[SYSCALL_OP][2] = UNUSED;
	instr_format[SYSCALL_OP][OP_COUNT] = 0;
	
	// nop
	instr_format[NOP_OP][0] = UNUSED;
	instr_format[NOP_OP][1] = UNUSED;
	instr_format[NOP_OP][2] = UNUSED;
	instr_format[NOP_OP][OP_COUNT] = 0;

	// fadd $rd, $rs, $rt 
	instr_format[FADD_OP][0] = RD;
	instr_format[FADD_OP][1] = RS;
	instr_format[FADD_OP][2] = RT;
	instr_format[FADD_OP][OP_COUNT] = 3;

	// fmul $rd, $rs, $rt 
	instr_format[FMUL_OP][0] = RD;
	instr_format[FMUL_OP][1] = RS;
	instr_format[FMUL_OP][2] = RT;
	instr_format[FMUL_OP][OP_COUNT] = 3;

	// fsub $rd, $rs, $rt 
	instr_format[FSUB_OP][0] = RD;
	instr_format[FSUB_OP][1] = RS;
	instr_format[FSUB_OP][2] = RT;
	instr_format[FSUB_OP][OP_COUNT] = 3;
	
	// ld $rd, Imm($rs)
	instr_format[LD_OP][0] = RD;
	instr_format[LD_OP][1] = REG_OFF;
	instr_format[LD_OP][2] = UNUSED;
	instr_format[LD_OP][OP_COUNT] = 2;

	// sd $rd, Imm($rs)
	instr_format[SD_OP][0] = RD;
	instr_format[SD_OP][1] = REG_OFF;
	instr_format[SD_OP][2] = UNUSED;
	instr_format[SD_OP][OP_COUNT] = 2;

}

static void init_instr_names() {
	instr_names[END_OP] = "end";
	instr_names[ADD_OP] = "add";
	instr_names[ADDI_OP] = "addi";
	instr_names[B_OP] = "b";
	instr_names[BEQZ_OP] = "beqz";
	instr_names[BGE_OP] = "bge";
	instr_names[BNE_OP] = "bne";
	instr_names[LA_OP] = "la";
	instr_names[LB_OP] = "lb";
	instr_names[LI_OP] = "li";
	instr_names[SUBI_OP] = "subi";
	instr_names[SYSCALL_OP] = "syscall";
	instr_names[NOP_OP] = "nop";
	instr_names[FADD_OP] = "fadd";
	instr_names[FMUL_OP] = "fmul";
	instr_names[FSUB_OP] = "fsub";
	instr_names[LD_OP] = "ld";
	instr_names[SD_OP] = "sd";
}

uint8_t lookup_instr(char* name) {
	uint8_t i;
	for (i = 0; i < NUM_INSTR_T; i++) {
		if (strcasecmp(instr_names[i], name) == 0) {
			return i;
		}
	}
	printf("Assembly failed on op: %s\n", name);
	return INVALID_OP;
}


uint8_t parse_reg_name(char* text) {
	short len = strlen(text);
	if (text[0] == 'f') {
		text++; // skip past the 'f'
		return atoi(text) + 32; // adjust for float regs
	} else {
		return atoi(text);
	}
}

void parse_instr(char* text, instr **i) {
	if (needs_init == TRUE) {
		init_instr_format();
		init_instr_names();
		needs_init = FALSE;
	}
	char op_name[BUFF], op1[BUFF], op2[BUFF], op3[BUFF];
	short scan_count;
	LOG_PRINT("instr: scanning this line: %s\n", text);
	scan_count = sscanf(text, "%s %s %s %s", op_name, op1, op2, op3);
	LOG_PRINT("Got this operation: %s\n", op_name);
	uint8_t op_index = lookup_instr(op_name);
	char* operands[MAX_OPERANDS] = { &op1[0], &op2[0], &op3[0] };
	// strip $ and ,
	int j;
	for (j = 0; j < instr_format[op_index][OP_COUNT]; j++) {
		if (operands[j][0] == '$') {
			operands[j]++;
		}
		int len = strlen(operands[j]);
		if (operands[j][len - 1] == ',') {
			operands[j][len - 1] = 0;
		}
		LOG_PRINT("New operand: %s\n", operands[j]);
	}
	
	if (instr_format[op_index][OP_COUNT] != scan_count - 1) {
		LOG_PRINT("Wrong number of arguments for op: %d. Expected: %d, Actual %d\n", op_index, instr_format[op_index][OP_COUNT], scan_count - 1);
		exit(0);
	}
	(*i)->bin_op_code = op_index;
	(*i)->op_code = malloc(strlen(op_name) + 1);
	strcpy((*i)->op_code, op_name);
	(*i)->rs = INVALID_REG;
	(*i)->rd = INVALID_REG;
	(*i)->rt = INVALID_REG;
	for (j = 0; j < instr_format[op_index][OP_COUNT]; j++) {
		switch (instr_format[op_index][j]) {
			case RT:
				//(*i)->rt = (uint8_t)atoi(operands[j]);
				(*i)->rt = parse_reg_name(operands[j]);
				LOG_PRINT("rt = %d\n", (*i)->rt);
				break;
			case RS:
				//(*i)->rs = (uint8_t)atoi(operands[j]);
				(*i)->rs = parse_reg_name(operands[j]);
				LOG_PRINT("rs = %d\n", (*i)->rs);
				break;
			case RD:
				//(*i)->rd = (uint8_t)atoi(operands[j]);
				(*i)->rd = parse_reg_name(operands[j]);
				LOG_PRINT("rd = %d\n", (*i)->rd);
				break;
			case IMM:
				(*i)->imm = (int32_t)atoi(operands[j]);
				LOG_PRINT("imm = %d\n", (*i)->imm);
				break;
			case REG_OFF:
			{
				int reg;
				sscanf(operands[1], "%d($%u)", &((*i)->imm), &reg);
				(*i)->rs = (uint8_t)reg;
				LOG_PRINT("rs = %d\n", (*i)->rs);
				LOG_PRINT("imm = %d\n", (*i)->imm);
				break;
			}
			case LABEL:
				(*i)->label = malloc(strlen(operands[j]) + 1);
				strcpy((*i)->label, operands[j]);
				break;
		}
	}
}

void test_instr() {
	init_instr_names();
	init_instr_format();
	char* success = "Encoded successfully.\n";
	char* failure = "Encoding failed.\n";

	char* test = "addi $0, $1, 45";
	instr *i = malloc(sizeof(instr));
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 0 && i->rs == 1 && i->imm == 45) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "beqz $31, MYLABEL";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rs == 31) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "bge $17, $15, MYLABEL";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rs == 17 && i->rt == 15) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "bne $17, $15, LABEL";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rs == 17 && i->rt == 15 && strcmp(i->label, "LABEL") == 0) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "la $15, MYLABEL";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 15) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}
	
	test = "lb $7, 1600($13)";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 7 && i->imm == 1600 && i->rs == 13) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "li $8, 78";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 8 && i->imm == 78) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "subi $5, $4, -9";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 5 && i->rs == 4 && i->imm == -9) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "fadd $3, $1, $2";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 3 && i->rs == 1 && i->rt == 2 && i->bin_op_code == FADD_OP) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "fmul $3, $1, $2";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 3 && i->rs == 1 && i->rt == 2 && i->bin_op_code == FMUL_OP) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "fsub $3, $1, $2";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 3 && i->rs == 1 && i->rt == 2 && i->bin_op_code == FSUB_OP) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "sd $3, 100($1)";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 3 && i->rs == 1 && i->imm == 100 && i->bin_op_code == SD_OP) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}

	test = "ld $3, 100($1)";
	parse_instr(test, &i);
	puts("Finished encoding.");
	if (i->rd == 3 && i->rs == 1 && i->imm == 100 && i->bin_op_code == LD_OP) {
		LOG_PRINT(success);
		puts(success);
	} else {
		LOG_PRINT(failure);
		puts(failure);
	}
}

void print_instr(instr i) {
	printf("Instr %d:\n", i.id);
	printf("\top = %s (%d)\n", i.op_code, i.bin_op_code);
	printf("\trd = %d\n", i.rd);
	printf("\trs = %d\n", i.rs);
	printf("\trt = %d\n", i.rt);
	printf("\timm = %d\n", i.imm);
	if (i.label != NULL) {
		printf("\tlabel= %s\n", i.label);
	}
}
