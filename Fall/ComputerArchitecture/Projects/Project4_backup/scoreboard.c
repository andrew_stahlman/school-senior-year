#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "scoreboard.h"

// SYSCALL OPS
#define PRINT_INT 1
#define PRINT 4
#define READ 8
#define EXIT 10

#define STR_REG 4
#define STR_LEN_REG 5
#define SYSCALL_REG 2

#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)

const char* fu_names[] = { "Integer", "Float1", "Float2", "Mem" };
const uint8_t fu_times[] = { 2, 2, 6, 2 };

word int_regs[NUM_INT_REGS];
double float_regs[NUM_FLOAT_REGS];
short cycle = 0;
mem_addr pc = TEXT_MIN;

void add_instr_status(scoreboard_t* score, instr_status_t* i_stat) {
	if (score->instr_status == NULL) {
		score->instr_status = i_stat;
	} else {
		instr_status_t* temp = score->instr_status;
		score->instr_status = i_stat;
		score->instr_status->next = temp;
	}
}

void get_instr_status(scoreboard_t* score, uint16_t id, instr_status_t** i_stat) {
	*i_stat = score->instr_status;
	while ((*i_stat)->id != id) {
		*i_stat = (*i_stat)->next;
	}
}

void print_scoreboard(scoreboard_t score) {
	puts("INSTRUCTION STATUS\n");
	printf("num_issued = %d\n", score.num_issued);
	puts("Instr.\ti\tj\tk\t\tIssue\tRead\tExec.\tWB\n");

	instr_status_t* i_stat = score.instr_status;
	// print only the 10 most recent
	short limit = 0;
	while (i_stat != NULL && limit < 10) {
		printf("%d\t%d\t%d\t%d\t", i_stat->bin_op_code, i_stat->i, i_stat->j, i_stat->k);
		short stage;
		for (stage = 0; stage < NUM_STAGES; stage++) {
			printf("\t%d", i_stat->completion_times[stage]);
		}
		puts("");
		i_stat = i_stat->next;
		limit++;
	}
	
	puts("\nFU STATUS\n");
	puts("FU\t\tTime\tBusy\tOp\tFi\tFj\tFk\tQj\tQk\tRj\tRk\n");
	fu_status_t fu;
	int i;
	for (i = 0; i < NUM_FU; i++) {
		fu = score.fu_status[i];
		printf("%s\t\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", fu_names[i], fu.time_left, fu.is_busy, fu.bin_op_code, fu.Fi, fu.Fj, fu.Fk, fu.Qj, fu.Qk, fu.Rj, fu.Rk);
	}
	puts("\nREGISTER RESULT STATUS\n");
	
	for (i = 0; i < NUM_REGS; i++) {
		if (score.reg_result_status[i] != UNDEFINED) {
			printf("Reg %d: %s\n", i, fu_names[score.reg_result_status[i]]);
		}
	}
}

uint8_t select_fu(uint8_t op) {
	if (op > 0 && op < FADD_OP) {
		return FU_INT;
	} else if (op >= FADD_OP && op < FMUL_OP) {
		return FU_FLOAT1;
	} else if (op == FMUL_OP) {
		return FU_FLOAT2;
	} else if (op >= LD_OP && op < INVALID_OP) {
		return FU_MEM;
	} else {
		printf("Can't select functional unit for invalid op: %d\n", op);
		exit(1);
	}
}

bool instr_issue(mem_addr text_top, scoreboard_t old_score, scoreboard_t* new_score, issued_buffer* issued) {
	instr* i;
	if (pc < text_top) {
		i = (instr*)addr_access(pc);
	} else {
		//printf("Not issuing, no more instructions...\n");
		return FALSE;
	}

	// early exit if the functional unit is busy
	uint8_t fu = select_fu(i->bin_op_code);
	if (old_score.fu_status[fu].is_busy == TRUE) {
		//printf("Not issuing, %s is busy.\n", fu_names[fu]);
		return FALSE;
	}

	// early exit on WAW hazard
	if (i->rd != INVALID_REG && old_score.reg_result_status[i->rd] != UNDEFINED) {
		//printf("reg_result_status[%d] = %d\n", i->rd, old_score.reg_result_status[i->rd]);
		//printf("Not issuing, WAW for reg %d and FU %s.\n", i->rd, fu_names[old_score.reg_result_status[i->rd]]);
		return FALSE;
	}

	//puts("Issuing this instruction:");
	print_instr(*i);
	i->id = cycle;

	instr_status_t* i_stat = malloc(sizeof(instr_status_t));
	new_score->fu_status[fu].is_busy = TRUE;
	add_instr_status(new_score, i_stat);
	new_score->fu_status[fu].bin_op_code = i->bin_op_code;
	new_score->fu_status[fu].Fi = i->rd;
	new_score->fu_status[fu].Fj = i->rs;
	new_score->fu_status[fu].Fk = i->rt;
	i_stat->bin_op_code = i->bin_op_code;
	i_stat->i = i->rd;
	i_stat->j = i->rs;
	i_stat->k = i->rt;
	i_stat->id = cycle;
	i_stat->completion_times[ISSUE] = cycle;
	int stage;
	for (stage = READ_OPS; stage < NUM_STAGES; stage++) {
		i_stat->completion_times[stage] = UNDEFINED;
	}
	

	if (i->rs != INVALID_REG) {
		new_score->fu_status[fu].Qj = old_score.reg_result_status[i->rs];
		new_score->fu_status[fu].Rj = (old_score.reg_result_status[i->rs] == UNDEFINED) ? TRUE : FALSE;
		//new_score->reg_result_status[i->rs] = fu; this can't be right...
	} else {
		new_score->fu_status[fu].Qj = UNDEFINED;
		new_score->fu_status[fu].Rj = UNDEFINED;
	}
	if (i->rt != INVALID_REG) {
		new_score->fu_status[fu].Qk = old_score.reg_result_status[i->rt];
		new_score->fu_status[fu].Rk = (old_score.reg_result_status[i->rt] == UNDEFINED) ? TRUE : FALSE;
		//new_score->reg_result_status[i->rt] = fu; this can't be right...
	} else {
		new_score->fu_status[fu].Qk = UNDEFINED;
		new_score->fu_status[fu].Rk = UNDEFINED;
	}

	if (i->rd != INVALID_REG) {
		new_score->reg_result_status[i->rd] = fu;
	}

	pc += sizeof(instr);
	new_score->num_issued = old_score.num_issued + 1;
	add_instr(issued, i);
	return TRUE;
}


void read_operands(scoreboard_t old_score, scoreboard_t* new_score, issued_buffer old_issued, issued_buffer* new_issued, pre_ex_latch_t latch_out[]) {
	memset(latch_out, 0, sizeof(pre_ex_latch_t) * NUM_FU);
	issued_buffer_node* n = old_issued.head;
	instr* i;
	while (n != NULL) {
		i = n->i;
		uint8_t fu = select_fu(i->bin_op_code);
		if (old_score.fu_status[fu].Rj != FALSE && old_score.fu_status[fu].Rk != FALSE) {
			//puts("READ_OPS: reading for this instr:");
			print_instr(*i);
			if (fu == FU_FLOAT1 || fu == FU_FLOAT2) {
				latch_out[fu].f_op_A = float_regs[i->rs - 32];
				latch_out[fu].f_op_B = float_regs[i->rt - 32];
			} else {
				latch_out[fu].op_A = int_regs[i->rs];
				latch_out[fu].op_B = int_regs[i->rt];
			}
			latch_out[fu].instruction = i;

			if (i->bin_op_code == SYSCALL_OP) {
				latch_out[FU_INT].syscall_print_int_val = int_regs[STR_REG];
				latch_out[FU_INT].syscall_op = int_regs[SYSCALL_REG];
			}

			instr_status_t* i_stat;
			get_instr_status(new_score, i->id, &i_stat);
			i_stat->completion_times[READ_OPS] = cycle;
			new_score->fu_status[fu].Rj = FALSE;
			new_score->fu_status[fu].Rk = FALSE;

			// remove from the buffer
			assert(TRUE == remove_instr(new_issued, i));
		} else {
			//printf("Could not read for instruction %d in %s!\n", i->id, fu_names[fu]);
		}
		n = n->next;
	}

}

void notify_completion(scoreboard_t old_score, scoreboard_t* new_score, uint8_t rd) {
	//printf("Notifying for %d\n", rd);
	short fu;
	for (fu = 0; fu < NUM_FU; fu++) {
		if (old_score.fu_status[fu].Fj == rd) {
			new_score->fu_status[fu].Rj = TRUE;
			new_score->fu_status[fu].Qj = UNDEFINED;
		}
		if (old_score.fu_status[fu].Fk == rd) {
			new_score->fu_status[fu].Rk = TRUE;
			new_score->fu_status[fu].Qk = UNDEFINED;
		}
	}
}

void fu_mem(scoreboard_t old_score, scoreboard_t* new_score, pre_ex_latch_t latch_in, post_ex_latch_t* latch_out) {
	latch_out->is_ready = FALSE;
	if (old_score.fu_status[FU_MEM].time_left == UNDEFINED) {
		if (latch_in.instruction == NULL) {
			//puts("FU_MEM: Nothing to do.");
		} else {
			instr* i = latch_in.instruction;
			//puts("FU_MEM: Received this instruction");
			print_instr(*i);
			new_score->fu_status[FU_MEM].time_left = fu_times[FU_MEM] - 1;
			latch_out->instruction = i;
			latch_out->rd = latch_in.instruction->rd;
			mem_addr addr;
			byte *b;
			switch (i->bin_op_code) {
				case LA_OP:
					// Are we supposed to store the address or the contents of the address?
					// For now, the address itself
					//assert(read_word(i->operand, &(latch_out->result)) == TRUE);
					latch_out->result = i->operand;
					latch_out->result = i->operand;
					break;
				case LB_OP:
					addr = latch_in.op_A + i->imm;
					b = addr_access(addr);
					latch_out->result = *b;
					latch_out->f_result = *b;
					break;
				case LI_OP:
					latch_out->result = i->imm;
					latch_out->f_result = i->imm;
					break;
				case LD_OP:
					addr = latch_in.op_A + i->imm;
					assert(read_double(addr, &(latch_out->f_result)) == TRUE);
					break;
				case SD_OP:
					addr = latch_in.op_A + i->imm;
					assert(write_double(addr, latch_out->f_result) == TRUE);
					break;
				default:
					puts("ERROR: FU_MEM received an invalid operand!");
					assert(1 != 1);
			}
		}
	} else if (old_score.fu_status[FU_MEM].time_left > 1) {
		//puts("FU_MEM: Working...");
		new_score->fu_status[FU_MEM].time_left = old_score.fu_status[FU_MEM].time_left - 1;
	} else if (old_score.fu_status[FU_MEM].time_left == 1) {
		//puts("FU_MEM: Finishing...");
		instr* i = latch_out->instruction;
		instr_status_t* i_stat;
		latch_out->is_ready = TRUE;
		get_instr_status(new_score, i->id, &i_stat);
		i_stat->completion_times[EXEC_COMPL] = cycle;
		new_score->reg_result_status[i->rd] = UNDEFINED; // forwarding
		//notify_completion(new_score, i->rd);
		init_fu_status(&(new_score->fu_status[FU_MEM]));
	}
}

void fu_float_add(scoreboard_t old_score, scoreboard_t* new_score, pre_ex_latch_t latch_in, post_ex_latch_t* latch_out) {
	latch_out->is_ready = FALSE;
	if (old_score.fu_status[FU_FLOAT1].time_left == UNDEFINED) {
		if (latch_in.instruction == NULL) {
			//puts("FU_FLOAT1: Nothing to do.");
		} else {
			instr* i = latch_in.instruction;
			//puts("FU_FLOAT1: Received this instruction");
			//printf("f_op_A = %f\n", latch_in.f_op_A);
			//printf("f_op_B = %f\n", latch_in.f_op_B);
			print_instr(*i);
			new_score->fu_status[FU_FLOAT1].time_left = fu_times[FU_FLOAT1] - 1;
			latch_out->instruction = i;
			latch_out->rd = latch_in.instruction->rd;
			switch (i->bin_op_code) {
				case FADD_OP:
					latch_out->f_result = latch_in.f_op_A + latch_in.f_op_B;
					//printf("FADD: %f + %f = %f\n", latch_in.f_op_A, latch_in.f_op_B, latch_out->f_result);
					break;
				case FSUB_OP:
					latch_out->f_result = latch_in.f_op_A - latch_in.f_op_B;
					//printf("FSUB: %f - %f = %f\n", latch_in.f_op_A, latch_in.f_op_B, latch_out->f_result);
					break;
				default:
					puts("ERROR: FU_FLOAT1 received an invalid operand!");
					assert(1 != 1);
			}
		}
	} else if (old_score.fu_status[FU_FLOAT1].time_left > 1) {
		//puts("FU_FLOAT1: Working...");
		new_score->fu_status[FU_FLOAT1].time_left = old_score.fu_status[FU_FLOAT1].time_left - 1;
	} else if (old_score.fu_status[FU_FLOAT1].time_left == 1) {
		//puts("FU_FLOAT1: Finishing...");
		instr* i = latch_out->instruction;
		instr_status_t* i_stat;
		latch_out->is_ready = TRUE;
		get_instr_status(new_score, i->id, &i_stat);
		i_stat->completion_times[EXEC_COMPL] = cycle;
		new_score->reg_result_status[i->rd] = UNDEFINED; // forwarding
		//notify_completion(new_score, i->rd);
		init_fu_status(&(new_score->fu_status[FU_FLOAT1]));
	}
}

void fu_float_mul(scoreboard_t old_score, scoreboard_t* new_score, pre_ex_latch_t latch_in, post_ex_latch_t* latch_out) {
	latch_out->is_ready = FALSE;
	if (old_score.fu_status[FU_FLOAT2].time_left == UNDEFINED) {
		if (latch_in.instruction == NULL) {
			//puts("FU_FLOAT2: Nothing to do.");
		} else {
			instr* i = latch_in.instruction;
			//puts("FU_FLOAT2: Received this instruction");
			print_instr(*i);
			new_score->fu_status[FU_FLOAT2].time_left = fu_times[FU_FLOAT2] - 1;
			latch_out->instruction = i;
			latch_out->rd = latch_in.instruction->rd;
			switch (i->bin_op_code) {
				case FMUL_OP:
					latch_out->f_result = latch_in.f_op_A * latch_in.f_op_B;
					//printf("FMUL: %f * %f = %f\n", latch_in.f_op_A, latch_in.f_op_B, latch_out->f_result);
					break;
				default:
					puts("ERROR: FU_FLOAT2 received an invalid operand!");
					assert(1 != 1);
			}
		}
	} else if (old_score.fu_status[FU_FLOAT2].time_left > 1) {
		//puts("FU_FLOAT2: Working...");
		new_score->fu_status[FU_FLOAT2].time_left = old_score.fu_status[FU_FLOAT2].time_left - 1;
	} else if (old_score.fu_status[FU_FLOAT2].time_left == 1) {
		//puts("FU_FLOAT2: Finishing...");
		instr* i = latch_out->instruction;
		instr_status_t* i_stat;
		latch_out->is_ready = TRUE;
		get_instr_status(new_score, i->id, &i_stat);
		i_stat->completion_times[EXEC_COMPL] = cycle;
		new_score->reg_result_status[i->rd] = UNDEFINED; // forwarding
		//notify_completion(new_score, i->rd);
		init_fu_status(&(new_score->fu_status[FU_FLOAT2]));
	}
}

void fu_int(scoreboard_t old_score, scoreboard_t* new_score, pre_ex_latch_t latch_in, post_ex_latch_t* latch_out, bool* user_mode) {
	latch_out->is_ready = FALSE;
	if (old_score.fu_status[FU_INT].time_left == UNDEFINED) {
		if (latch_in.instruction == NULL) {
			//puts("FU_INT: Nothing to do.");
		} else {
			instr* i = latch_in.instruction;
			//puts("FU_INT: Received this instruction");
			print_instr(*i);
			new_score->fu_status[FU_INT].time_left = fu_times[FU_INT] - 1;
			latch_out->is_ready = FALSE;
			latch_out->instruction = i;
			latch_out->rd = latch_in.instruction->rd;
			switch (i->bin_op_code) {
				case B_OP:
					pc = i->operand;
					break;
				case BEQZ_OP:
					if (latch_in.op_A == 0) {
						//puts("BEQZ took branch!");
						pc = i->operand;
					}
					break;
				case BGE_OP:
					//printf("bge: %d >= %d?\n", latch_in.op_A, latch_in.op_B);
					if (latch_in.op_A >= latch_in.op_B) {
						//printf("BGE took branch to %s: %d!\n", i->label, i->operand);
						pc = i->operand;
					}
					break;
				case BNE_OP:
					if (latch_in.op_A != latch_in.op_B) {
						//puts("BNE took branch!");
						pc = i->operand;
					}
					break;
				case ADDI_OP:
					latch_out->result = latch_in.op_A + i->imm;
					break;
				case ADD_OP:
					latch_out->result = latch_in.op_A + latch_in.op_B;
					break;
				case SUBI_OP:
					//printf("SUBI: %d - %d\n", latch_in.op_A, i->imm);
					latch_out->result = latch_in.op_A - i->imm;
					break;
				case SYSCALL_OP:
					// TODO: read syscall regs into pre_ex_latch
					//puts("Got syscall instruction, but not handling it.");
					switch (latch_in.syscall_op) {
						case PRINT_INT:
							printf("%d\n", latch_in.syscall_print_int_val);
							break;
						case EXIT:
							*user_mode = FALSE;
							break;
						default:
							puts("EXE received syscall with unknown syscall_op");
					}
					break;
				case END_OP:
					//puts("Got END. Exiting...");
					exit(0);
					break;
				case NOP_OP:
					//puts("Executing NOP.");
					break;
			}
		}
	} else if (old_score.fu_status[FU_INT].time_left > 1) {
		//puts("FU_INT: Working...");
		new_score->fu_status[FU_INT].time_left = old_score.fu_status[FU_INT].time_left - 1;
	} else if (old_score.fu_status[FU_INT].time_left == 1) {
		//puts("FU_INT: Finishing...");
		instr* i = latch_out->instruction;
		instr_status_t* i_stat;
		latch_out->is_ready = TRUE;
		get_instr_status(new_score, i->id, &i_stat);
		i_stat->completion_times[EXEC_COMPL] = cycle;
		new_score->reg_result_status[i->rd] = UNDEFINED; // forwarding
		//notify_completion(new_score, i->rd);
		init_fu_status(&(new_score->fu_status[FU_INT]));
	}
}



bool detect_WAR(scoreboard_t old_score, uint8_t rd) {
	short fu;
	for (fu = 0; fu < NUM_FU; fu++) {
		if (old_score.fu_status[fu].Fj == rd &&
			old_score.fu_status[fu].Rj == TRUE) 
		{
			return TRUE;
		} else if (old_score.fu_status[fu].Fk == rd &&
			old_score.fu_status[fu].Rk == TRUE) 
		{
			return TRUE;
		}
	}
	return FALSE;
}

// return TRUE if write occurs
short write_back(scoreboard_t old_score, scoreboard_t* new_score, post_ex_latch_t latch_in[]) {

	short numWrites = 0;
	uint8_t i;
	for (i = 0; i < NUM_FU; i++) {

		// early exit
		if (latch_in[i].is_ready != TRUE) {
			continue;
		}
		//printf("%s ready to write!\n", fu_names[i]);

		instr_status_t* i_stat;
		get_instr_status(new_score, latch_in[i].instruction->id, &i_stat);
		if (latch_in[i].rd == INVALID_REG) {
			//puts("WB: No write needed for INVALID_REG");
			i_stat->completion_times[WB] = cycle;
			memset(&(latch_in[i]), 0, sizeof(post_ex_latch_t));
			numWrites++;
		} else { 
			bool hasWAR = detect_WAR(old_score, latch_in[i].rd);
			if (hasWAR == FALSE) {
				//printf("Performing write for %s\n", fu_names[i]);
				i_stat->completion_times[WB] = cycle;
				if (latch_in[i].rd >= NUM_INT_REGS) {
					float_regs[latch_in[i].rd - 32] = latch_in[i].f_result; 
					//printf("WB: Wrote float %f to %d\n", float_regs[latch_in[i].rd - 32], latch_in[i].rd);
				} else {
					int_regs[latch_in[i].rd] = latch_in[i].result;
					//printf("WB: Wrote %d to %d\n", int_regs[latch_in[i].rd], latch_in[i].rd);
				}
				
				notify_completion(old_score, new_score, latch_in[i].rd);
				memset(&(latch_in[i]), 0, sizeof(post_ex_latch_t));
				numWrites++;
			} else {
				//puts("Can't write! WAR");
			}
		}
	}

	return numWrites;
}

bool remove_instr(issued_buffer* buffer, instr* i) {
	issued_buffer_node* next = buffer->head;
	issued_buffer_node* cur;

	// is head?
	if (next->i == i) {
		buffer->head = next->next;
		buffer->count -= 1;
		// is tail, too?
		if (buffer->count == 0) {
			buffer->tail = NULL;
		}
		//printf("Removed head. Count = %d. Head = %p, tail = %p\n", buffer->count, buffer->head, buffer->tail);
		return TRUE;
	}
	while (next != NULL) {
		cur = next;
		next = cur->next;
		if (next->i == i) {
			cur->next = next->next;
			buffer->count -= 1;
			// removed tail?
			if (cur->next == NULL) {
				buffer->tail = cur;
			}
			//printf("Removed node. Count = %d. Head = %p, tail = %p\n", buffer->count, buffer->head, buffer->tail);
			return TRUE;
		}
	}

	printf("ERROR: Tried to remove a nonexistent element!");
	exit(1);
	return FALSE;
}

void add_instr(issued_buffer* buffer, instr* i) {
	issued_buffer_node* new_node = malloc(sizeof(issued_buffer_node));
	new_node->i = i;
	buffer->count += 1;
	if (buffer->tail == NULL) {
		//puts("Added first node.");
		buffer->head = new_node;
		buffer->tail = new_node;
	} else {
		//puts("Added node at end.");
		buffer->tail->next = new_node;
		buffer->tail = new_node;
	}
}


void init_fu_status(fu_status_t* fu_status) {
	//puts("Resetting fu_status");
	memset(fu_status, UNDEFINED, sizeof(fu_status_t));
	fu_status->is_busy = FALSE;
}

void init_scoreboard(scoreboard_t* score) {
	memset(score, UNDEFINED, sizeof(scoreboard_t));
	score->num_issued = 0;
	score->instr_status = NULL;
	short fu;
	for (fu = 0; fu < NUM_FU; fu++) {
		init_fu_status(&(score->fu_status[fu]));
	}
}

void execute(mem_addr text_top) {
	//puts("In execute");
	scoreboard_t old_score;
	scoreboard_t new_score;
	issued_buffer old_issued;
	issued_buffer new_issued;
	pre_ex_latch_t old_pre_latches[NUM_FU];
	pre_ex_latch_t new_pre_latches[NUM_FU];
	post_ex_latch_t old_post_latches[NUM_FU];
	post_ex_latch_t new_post_latches[NUM_FU];

	memset(&new_issued, 0, sizeof(issued_buffer));
	memset(&old_issued, 0, sizeof(issued_buffer));
	memset(old_pre_latches, 0, sizeof(pre_ex_latch_t) * NUM_FU);
	memset(old_post_latches, 0, sizeof(post_ex_latch_t) * NUM_FU);
	memset(new_pre_latches, 0, sizeof(pre_ex_latch_t) * NUM_FU);
	memset(new_post_latches, 0, sizeof(post_ex_latch_t) * NUM_FU);
	init_scoreboard(&new_score);
	init_scoreboard(&old_score);

	bool user_mode = TRUE;
	short outstandingInstr = 0;

	while (user_mode == TRUE) {

		// backup scoreboard
		memcpy(&old_score, &new_score, sizeof(scoreboard_t));

		// backup latches
		memcpy(old_pre_latches, new_pre_latches, sizeof(pre_ex_latch_t) * NUM_FU);
		memcpy(old_post_latches, new_post_latches, sizeof(post_ex_latch_t) * NUM_FU);
		// backup issued_buffer
		memcpy(&old_issued, &new_issued, sizeof(issued_buffer));


		printf("\n\nStarting Cycle %d - %d outstanding instr. Scoreboard:\n", cycle, outstandingInstr);
		print_scoreboard(old_score);

		outstandingInstr -= write_back(old_score, &new_score, old_post_latches);
		fu_int(old_score, &new_score, old_pre_latches[FU_INT], &(new_post_latches[FU_INT]), &user_mode); 
		fu_float_add(old_score, &new_score, old_pre_latches[FU_FLOAT1], &(new_post_latches[FU_FLOAT1])); 
		fu_float_mul(old_score, &new_score, old_pre_latches[FU_FLOAT2], &(new_post_latches[FU_FLOAT2])); 
		fu_mem(old_score, &new_score, old_pre_latches[FU_MEM], &(new_post_latches[FU_MEM])); 

		read_operands(old_score, &new_score, old_issued, &new_issued, new_pre_latches);
		bool didIssue = instr_issue(text_top, old_score, &new_score, &new_issued);
		if (didIssue == TRUE) {
			outstandingInstr++;
		}

		user_mode = (user_mode == TRUE && outstandingInstr > 0) ? TRUE : FALSE;
		cycle++;
	}


	/*
	puts("Execution finished. Here are the int regs:");
	short r;
	for (r = 0; r < NUM_INT_REGS; r++) {
		printf("$%d = %d\n", r, int_regs[r]);
	}
	puts("Here are the float regs:");
	for (r = 0; r < NUM_FLOAT_REGS; r++) {
		printf("$%d = %f\n", r, float_regs[r]);
	}
	*/
}
