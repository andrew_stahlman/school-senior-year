#include "assembler.h"
#include "logger.h"
#include "pipeline.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_REGS 32

// our 32 registers
word regs[NUM_REGS];

int main(int argc, char* argv[]) {
	if (argc != 2) {
		puts("Usage: filename");
		return -1;
	}

	FILE *fp = fopen(argv[1], "r");
	if (fp == 0) {
		printf("Could not read from source file %s. Exiting.\n", argv[1]);
		return -1;
	}

	short r;
	for (r = 0; r < NUM_REGS; r++) {
		regs[r] = 0;
	}

	mem_addr text_top = TEXT_MIN;
	mem_addr data_top = DATA_MIN;
	// text_top is set to the address of the end of the last loaded instruction
	// data_top is set to the address of the end of the last word we init 
	if (load_program(fp, &data_top, &text_top, ACCUM_MODE) != 0) {
		printf("Error loading program %s. Exiting...\n", argv[1]);
		return -1;
	}
	
	execute(text_top, regs);
}
