#include <stdlib.h>
#include <stdio.h>
#include "pipeline.h"

// SYSCALL OPS
#define PRINT_INT 1
#define PRINT 4
#define READ 8
#define EXIT 10

#define STR_REG 4
#define STR_LEN_REG 5
#define SYSCALL_REG 2

void IF(if_id_latch* if_id, mem_addr *pc, mem_addr text_top);
void ID(if_id_latch* if_id, id_exe_latch* id_exe, exe_mem_latch* ex_mem, mem_wb_latch* mem_wb, word regs[]);
void EXE(id_exe_latch *id_exe, exe_mem_latch *exe_mem, mem_addr *pc, bool *user_mode);
void MEM(exe_mem_latch *exe_mem, mem_wb_latch *mem_wb);
bool WB(mem_wb_latch *mem_wb, word regs[]);

void reset_if_id(if_id_latch* l) {
	l->ir = NULL;
}

void reset_id_exe(id_exe_latch* l) {
	l->ir = NULL;
	l->op_A = 0;
	l->op_A = 0;
	l->branch_target = 0;
	l->syscall_op = INVALID_OP;
	l->syscall_str_loc = 0;
	l->syscall_str_len = 0;
	l->syscall_print_int_val = 0;
}

void reset_exe_mem(exe_mem_latch* l) {
	l->bin_op_code = INVALID_OP;
	l->ALU_out = 0;
	l->rd = INVALID_REG;
	l->syscall_op = INVALID_OP;
	l->load_src = 0;
	l->syscall_str_loc = 0;
	l->syscall_str_len = 0;
}

void reset_mem_wb(mem_wb_latch* l) {
	l->bin_op_code = INVALID_OP;
	l->ALU_out = 0;
	l->rd = INVALID_REG;
}

void execute(mem_addr text_top, word regs[]) {
	if_id_latch if_id;
	id_exe_latch id_exe;
	exe_mem_latch exe_mem;
	mem_wb_latch mem_wb;
	
	reset_mem_wb(&mem_wb);
	reset_exe_mem(&exe_mem);
	reset_id_exe(&id_exe);
	reset_if_id(&if_id);

	mem_addr pc = TEXT_MIN;
	bool user_mode = TRUE;
	unsigned int cycles = 0, IC = 0, nop_count = 0;

	while (user_mode) {
		//printf("------STARTING CYCLE %d------\n", cycles);
		user_mode = WB(&mem_wb, regs);
		reset_mem_wb(&mem_wb);
		MEM(&exe_mem, &mem_wb);
		reset_exe_mem(&exe_mem);
		if (id_exe.ir != NULL) { 
			IC++;
			if ((id_exe.ir)->bin_op_code == NOP_OP) {
				nop_count++;
			}
		}
		EXE(&id_exe, &exe_mem, &pc, &user_mode);
		reset_id_exe(&id_exe);
		ID(&if_id, &id_exe, &exe_mem, &mem_wb, regs);
		reset_if_id(&if_id);
		IF(&if_id, &pc, text_top);

		//printf("------END OF CYCLE %d------\n\n\n", cycles);

		cycles++;
		if (cycles < 5) {
			user_mode = TRUE;
		}
	}

	printf("Cycles: %d\n", cycles);
	printf("Instructions: %d\n", IC);
	printf("NOPs: %d\n", nop_count);

}

int is_op_branch(uint8_t op) {
	short branch_ops[] = { B_OP, BEQZ_OP, BGE_OP, BNE_OP };
	short n = 4;
	short i;
	for (i = 0; i < n; i++) {
		if (branch_ops[i] == op) {
			return TRUE;
		}
	}
	return FALSE;
}

void print_instr(instr* i) {
	printf("op_code = %s\n", i->op_code);
	if (i->rd != INVALID_REG) {
		printf("rd = %d\n", i->rd);
	}
	if (i->rs != INVALID_REG) {
		printf("rs = %d\n", i->rs);
	}
	if (i->rt != INVALID_REG) {
		printf("rt = %d\n\n", i->rt);
	}
	if (i->operand != 0) {
		printf("operand = %d\n", i->operand);
	}
}

void IF(if_id_latch* if_id, mem_addr *pc, mem_addr text_top) {
	if (*pc < text_top) {
		if_id->ir = (instr*)addr_access(*pc);
		//printf("IF: got this instruction:\n");
		//print_instr(if_id->ir);
		*pc += sizeof(instr);
	} else {
		if_id->ir = NULL;
	}
}

void ID(if_id_latch* if_id, id_exe_latch* id_exe, exe_mem_latch* ex_mem, mem_wb_latch* mem_wb, word regs[]) {

	instr* cur_instr = if_id->ir;
	if (cur_instr == NULL) {
		//puts("ID: current instruction is NULL");
		id_exe->ir = NULL;
		return;
	} else {
		//printf("ID: got this instruction:\n");
		//print_instr(cur_instr);
	}

	id_exe->ir = cur_instr;
	id_exe->op_A = regs[cur_instr->rs];
	id_exe->op_B = regs[cur_instr->rt];
	if (is_op_branch(cur_instr->bin_op_code)) {
		id_exe->branch_target = cur_instr->operand;
	} else {
		id_exe->branch_target = 0;
		id_exe->syscall_op = regs[SYSCALL_REG];
		id_exe->syscall_str_loc = regs[STR_REG];
		id_exe->syscall_str_len = regs[STR_LEN_REG];
		id_exe->syscall_print_int_val = regs[STR_REG]; // they both seem to be register 4...
	}

	// FORWARDING
	if (cur_instr->rs == mem_wb->rd && cur_instr->rs != INVALID_REG) {
		//printf("Forwarding mem_wb->ALU_out to op_A: %d => %d\n", id_exe->op_A, mem_wb->ALU_out);
		id_exe->op_A = mem_wb->ALU_out;
	} else if (cur_instr->rt == mem_wb->rd && cur_instr->rt != INVALID_REG) {
		//printf("Forwarding mem_wb->ALU_out to op_B: %d => %d\n", id_exe->op_B, mem_wb->ALU_out);
		id_exe->op_B = mem_wb->ALU_out;
	}

	if (cur_instr->rs == ex_mem->rd && cur_instr->rs != INVALID_REG) {
		//printf("Forwarding ex_mem->ALU_out to op_A: %d => %d\n", id_exe->op_A, ex_mem->ALU_out);
		id_exe->op_A = ex_mem->ALU_out;
	} else if (cur_instr->rt == ex_mem->rd && cur_instr->rt != INVALID_REG) {
		//printf("Forwarding ex_mem->ALU_out to op_B: %d => %d\n", id_exe->op_B, ex_mem->ALU_out);
		id_exe->op_B = ex_mem->ALU_out;
	}
}

void EXE(id_exe_latch *id_exe, exe_mem_latch *exe_mem, mem_addr *pc, bool *user_mode) {
	instr* cur_instr = id_exe->ir;
	if (cur_instr == NULL) {
		//puts("EXE: current instruction is NULL");
		exe_mem->bin_op_code = INVALID_OP;
		return;
	} else {
		//printf("EXE: got this instruction:\n");
		//print_instr(id_exe->ir);
	}

	if (id_exe->branch_target) {
		switch (id_exe->ir->bin_op_code) {
			case B_OP:
				*pc = id_exe->branch_target;
				break;
			case BEQZ_OP:
				if (id_exe->op_A == 0) {
					//puts("BEQZ took branch!");
					*pc = id_exe->branch_target;
				}
				break;
			case BGE_OP:
				if (id_exe->op_A >= id_exe->op_B) {
					//puts("BGE took branch!");
					*pc = id_exe->branch_target;
				}
				break;
			case BNE_OP:
				if (id_exe->op_A != id_exe->op_B) {
					//puts("BNE took branch!");
					*pc = id_exe->branch_target;
				}
				break;
			default:
				puts("ERROR: Exe stage received a branch_target in latch but op_code is not a branch.");
		}
	} else {
		switch (cur_instr->bin_op_code) {
			// Switching it up, ALU_out contains value to be written, op_B contains addr
			case ADDI_OP:
				exe_mem->ALU_out = id_exe->op_A + cur_instr->imm;
				break;
			case ADD_OP:
				exe_mem->ALU_out = id_exe->op_A + id_exe->op_B;
				break;
			case LA_OP:
				exe_mem->ALU_out = (word)cur_instr->operand;
				//printf("LA: cur_instr->operand = %d\n", cur_instr->operand);
				break;
			case LB_OP:
			{
				exe_mem->load_src = cur_instr->imm + id_exe->op_A;
				break;
			}
			case LI_OP:
				exe_mem->ALU_out = cur_instr->imm;
				break;
			case SUBI_OP:
				exe_mem->ALU_out = id_exe->op_A - cur_instr->imm;
				break;
			case SYSCALL_OP:
				//puts("Got syscall instruction.");
				switch (id_exe->syscall_op) {
					case PRINT_INT:
						printf("%d\n", id_exe->syscall_print_int_val);
						break;
					case PRINT:
						//puts("Got print instruction.");
						//printf("%s", (char*)addr_access(regs[STR_REG]));
						exe_mem->syscall_str_loc = id_exe->syscall_str_loc;
						exe_mem->syscall_op = PRINT;
						break;
					case READ:
					{
						exe_mem->syscall_str_loc = id_exe->syscall_str_loc;
						exe_mem->syscall_str_len = id_exe->syscall_str_len;
						exe_mem->syscall_op = READ;
						break;
					}
					case EXIT:
						*user_mode = FALSE;
						//puts("EXIT");
						break;
					default:
						puts("EXE received syscall with unknown syscall_op");
						//printf("syscall op = %d\n", id_exe->syscall_op);
				}
				break;
		}
	}

	exe_mem->bin_op_code = cur_instr->bin_op_code;
	exe_mem->rd = cur_instr->rd;
	exe_mem->syscall_op = id_exe->syscall_op;
	exe_mem->syscall_str_loc = id_exe->syscall_str_loc;
	exe_mem->syscall_str_len = id_exe->syscall_str_len;
	//printf("exe_mem->ALU_out = %d\n", exe_mem->ALU_out);
}

void MEM(exe_mem_latch *exe_mem, mem_wb_latch *mem_wb) {

	if (exe_mem->bin_op_code == INVALID_OP) {
		mem_wb->bin_op_code == INVALID_OP;
		//puts("MEM: nothing to do...");
		return;
	}
	
	mem_wb->bin_op_code = exe_mem->bin_op_code;
	mem_wb->ALU_out = exe_mem->ALU_out;
	mem_wb->rd = exe_mem->rd;

	byte *b;
	switch(exe_mem->bin_op_code) {
		case LB_OP:
			b = addr_access(exe_mem->load_src);
			mem_wb->ALU_out = *b;
			break;
		case SYSCALL_OP:
			switch(exe_mem->syscall_op) {
				case PRINT:
					printf("PRINT: %s\n", (char*)addr_access(exe_mem->syscall_str_loc));
					break;
				case READ:
				{
					//printf("READ: read string_loc = %d\n", exe_mem->syscall_str_loc);
					char *target = (char*)addr_access(exe_mem->syscall_str_loc);
					fgets(target, exe_mem->syscall_str_len, stdin);
					//puts("READ: read from stdin");
					//printf("READ: string at target: %s\n", target);
					break;
				}
				case PRINT_INT:
				case EXIT:
					break;
				default:
					puts("Error: MEM stage received a SYSCALL other than PRINT or READ");
			}
			break;
	}

}

bool WB(mem_wb_latch *mem_wb, word regs[]) {
	bool keep_executing = TRUE;
	switch (mem_wb->bin_op_code) {
		case ADD_OP:
		case ADDI_OP:
		case SUBI_OP:
			//printf("WB: R-type, storing %d at $%d\n", mem_wb->ALU_out, mem_wb->rd);
			regs[mem_wb->rd] = mem_wb->ALU_out;
			break;
		case LB_OP:
			//printf("WB: LA or LB stored %d at $%d\n", mem_wb->ALU_out, mem_wb->rd);
			regs[mem_wb->rd] = mem_wb->ALU_out;
			break;
		case LA_OP:
		case LI_OP:
			//printf("WB: LI stored %d at $%d\n", mem_wb->ALU_out, mem_wb->rd);
			regs[mem_wb->rd] = mem_wb->ALU_out;
			break;
		case INVALID_OP:
			keep_executing = FALSE;
			break;
	}

	return keep_executing;
}

