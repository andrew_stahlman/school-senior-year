#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "logger.h"
#include "assembler.h"

#define FAIL -1
#define OK 0
#define WORD_LEN 64
#define REMAINDER_LEN 256

const char* BINARY_FILE = "bin_output.txt";

unsigned long hash(char* name);
void get_sym(sym_table_ent *table[], char *name, sym_table_ent** e);
sym_table_ent* add_sym(sym_table_ent *table[], char *name);
void lookup_sym(sym_table_ent *table[], char *name, sym_table_ent** e);
void create_binaries(mem_addr data_max, mem_addr text_max, const char*);

bool is_one_word_instr(char* line) {
	char* one_words[] = { "syscall", "nop" };
	short i;
	for (i = 0; i < sizeof(one_words) / sizeof(one_words[0]); i++) {
		if (strcmp(one_words[i], line) == 0) {
			return TRUE;
		}
	}
	return FALSE;
}

static mem_addr text_ctr = TEXT_MIN;

status_code load_program(FILE* file, mem_addr *data_ctr, mem_addr *text_ctr, short mode) {

	// initialize the address space
	init_addr_space();

	// init symbol table
	sym_table_ent* sym_table[SYM_BUCKETS];
	memset(&sym_table[0], 0, sizeof(sym_table_ent*) * SYM_BUCKETS);

	// Pass 1: Fill out the symbol table,
	// create an array of instructions and data declarations
	char* line = (char*) malloc(MAX_VAL_LEN);
	char word1[WORD_LEN], word2[WORD_LEN], remainder[REMAINDER_LEN];
	int scan_count;
	const short data_mode = 1;
	const short text_mode = 2;
	short cur_mode = 0;
	size_t len;
	int instruction_count = 0;
	while (getline(&line, &len, file) != -1) {
		instruction_count++;
		//printf("instruction_count = %d\n", instruction_count);
		// handle the case where there is only one word
		// must be a directive or a text label
		scan_count = sscanf(line, "%s %s %1023[^\n]", word1, word2, remainder);
		LOG_PRINT("assembler: scanned this line: %s. scan_count = %d\n", line, scan_count);
		if (scan_count <= 0) {
			LOG_PRINT("assembler: skipping blank line.\n");
			continue;
		} else if (scan_count == 1 && !is_one_word_instr(word1)) {
			if (strcmp(word1, ".data") == 0) {
				cur_mode = data_mode;
				LOG_PRINT("Switched to data mode\n");
				continue;
			} else if (strcmp(word1, ".text") == 0) {
				cur_mode = text_mode;
				LOG_PRINT("Switched to text mode\n");
				continue;
			}
			else if (cur_mode == text_mode) { // must be a text label
				sym_table_ent* entry;
				if (word1[strlen(word1) - 1] == ':') {
					word1[strlen(word1) - 1] = 0;
				}
				get_sym(&sym_table[0], word1, &entry);
				LOG_PRINT("entry->name = %s; word1 = %s\n", entry->name, word1);
				entry->addr = *text_ctr;
				LOG_PRINT("assigning %s address %d\n", entry->name, entry->addr);
			}
		} else if (cur_mode == text_mode) { // not a directive or a label
			instr *next_instr = (instr*)addr_access(*text_ctr);
			parse_instr(line, &next_instr);
			LOG_PRINT("Parsed instr at %u\n", next_instr);
			LOG_PRINT("parsed op_code = %s\n", next_instr->op_code);
			LOG_PRINT("parsed bin_op_code = %d\n", next_instr->bin_op_code);
			if (next_instr->label != 0) {
				LOG_PRINT("label: %s\n", next_instr->label);
				// we have a mem_addr as an operand
				sym_table_ent* entry;
				get_sym(&sym_table[0], next_instr->label, &entry);
				entry->ref_instr[entry->num_ref_instr] = next_instr;
				entry->num_ref_instr++;
			}
			*text_ctr += sizeof(instr);
		} else if (cur_mode == data_mode) {
			sym_table_ent* entry;
			if (word1[strlen(word1) - 1] == ':') {
				word1[strlen(word1) - 1] = 0;
			}
			get_sym(&sym_table[0], word1, &entry);
			entry->addr = *data_ctr;
			LOG_PRINT("about to write %s at %d\n", remainder, *data_ctr);
			if (strcmp(word2, ".word") == 0) {
				int v = atoi(remainder); // TODO: check input
				write_word(*data_ctr, (word)v);
				LOG_PRINT("type=WORD, val=%d\n", v);
			} else if (strcmp(word2, ".asciiz") == 0) {
				char *result = malloc(strlen(remainder));
				strcpy(result, remainder);
				if (*result == '"') {
					result++;
					char *tail = result;
					while (tail) {
						if (*tail == '\\' && *(tail+1) == 'n') {
							*tail = '\n';
							*(tail+1) = 0;
						}
						if (*tail == '"') {
							*tail = 0;
							break;
						}
						tail++;
					}
				}
				char *c = (char*)addr_access(*data_ctr);
				strcpy(c, result); // TODO: bounds checking
				*data_ctr += strlen(result) + 1;
				LOG_PRINT("type=ASCII, val=%d\n", c);
				LOG_PRINT("c[0] = %c, c[1]  = %c\n", c[0], c[1]);
			} else if (strcmp(word2, ".space") == 0) {
				int size = atoi(remainder);
				*data_ctr += size;
				byte *b = addr_access(*data_ctr);
				memset(b, 0, size);
				LOG_PRINT("Wrote %d bytes at %d\n", size, *data_ctr - size);
			} else {
				LOG_PRINT("Here is the failure: %s\n", word2);
				return FAIL;
			}
			*data_ctr += sizeof(word);
		} else {
			LOG_PRINT("Assembly failed on line: %s", line);
			printf("Assembly failed on line: %s\n", line);
		}
		memset(word1, 0, WORD_LEN);
		memset(word2, 0, WORD_LEN);
		memset(remainder, 0, REMAINDER_LEN);
	}
	// for each entry in the symbol table, assign a mem_addr
	// and substitute the mem_addr in each instruction and 
	// data declaration that references the symbol
	short b = 0, r = 0;
	sym_table_ent* ent;
	for (b = 0; b < SYM_BUCKETS; b++) {
		LOG_PRINT("b = %d\n", b);
		ent = sym_table[b];
		while (ent != 0) {
			LOG_PRINT("entry name: %s; addr: %d; num_instr: %d\n", ent->name, ent->addr, ent->num_ref_instr);
			for (r = 0; r < ent->num_ref_instr; r++) {
				instr* i = ent->ref_instr[r];
				i->operand = ent->addr;
			}
			ent = ent->next;
		}
	}
	create_binaries(*data_ctr, *text_ctr, BINARY_FILE);
	return OK;
}

// djb2 string hashing via: http://www.cse.yorku.ca/~oz/hash.html
unsigned long hash(char* name) {
	unsigned long hash = 5381;
	int c;
	while (c = *name++) {
		hash = ((hash << 5) + hash) + c; // hash * 33 + c
	}
	return hash;
}

void get_sym(sym_table_ent *table[], char *name, sym_table_ent** e) {
	lookup_sym(table, name, e);
	if (*e == 0) {
		*e = add_sym(&table[0], name);
	}
	LOG_PRINT("got symbol, name: %s\n", (*e)->name);
}

sym_table_ent* add_sym(sym_table_ent *table[], char *name) {
	unsigned short bucket = (unsigned short) hash(name) % SYM_BUCKETS;
	sym_table_ent *ent = (sym_table_ent*)malloc(sizeof(sym_table_ent));
	ent->name = malloc(strlen(name) + 1);
	strcpy(ent->name, name);
	ent->next = 0;
	sym_table_ent **n = &table[bucket];
	while (*n != 0) {
		n = &((*n)->next);
	}
	*n = ent;
	return ent;
}

void lookup_sym(sym_table_ent *table[], char *name, sym_table_ent** e) {
	unsigned short bucket = (unsigned short) hash(name) % SYM_BUCKETS;
	*e = table[bucket];
	//while (*e != 0 && (*e)->next != 0) {
	while (*e != 0) {
		if (strcmp(name, (*e)->name) == 0) {
			return;
		} else {
			*e = (*e)->next;
		}
	}
	*e = 0;
}

void create_binaries(mem_addr data_max, mem_addr text_max, const char* bin_file) {
	const short INSTR_SIZE = 40;
	FILE *fp = fopen(bin_file, "w");
	bool file_ok = TRUE;
	if (fp == 0) {
		puts("Error: could not create assembled binary file.");
		file_ok = FALSE;
	} else {
		// Custom format - let first word be text size in bits
		// second word is size of data in bits
		unsigned int num_instr = (text_max - TEXT_MIN) / sizeof(instr);
		fprintf(fp, "%0#10X\n", num_instr * INSTR_SIZE);
		fprintf(fp, "%0#10X\n", data_max - DATA_MIN);
	}
	mem_addr dc = DATA_MIN;
	mem_addr pc = TEXT_MIN;
	instr *next_instr = (instr*)addr_access(pc);
	short count = 0;
	while (pc < text_max) {
		count++;
		byte bin_instr[INSTR_SIZE];
		memset(&bin_instr[0], next_instr->bin_op_code, sizeof(byte));
		memset(&bin_instr[8], next_instr->operand, sizeof(mem_addr));
		LOG_PRINT("Instruction %d: %0#4X | %0#10X\n", count, (uint8_t)bin_instr[0], (mem_addr)bin_instr[8]);
		if (file_ok) {
			fprintf(fp, "%0#4X%010X\n", (uint8_t)bin_instr[0], (mem_addr)bin_instr[8]);
		}
		pc += sizeof(instr);
		next_instr = (instr*)addr_access(pc);
	}
	int text_size = count * INSTR_SIZE;
	LOG_PRINT("Total text size: %d bits\n\n", text_size);

	word w;
	read_word(dc, &w);
	count = 0;
	while (dc < data_max) {
		count++;
		LOG_PRINT("Data %d: %0#10X\n", count, w);
		if (file_ok) {
			fprintf(fp, "%010X\n", w);
		}
		read_word(dc, &w);
		dc += sizeof(word);
	}
	int data_size = count * sizeof(word) * 8;
	LOG_PRINT("Total data size: %d bits\n\n", data_size);
	LOG_PRINT("Total program code size (text + data): %d bits\n\n", text_size + data_size);

}


static int test_num = 1;

static void fail_test() {
	char *f = "Failed test %d\n";
	printf(f, test_num);
	LOG_PRINT(f, test_num); 
}

void test_assembler() {
	FILE *fp = fopen("test_source.s", "r");
	mem_addr text_top = TEXT_MIN;
	mem_addr data_top = DATA_MIN;
	if (load_program(fp, &data_top, &text_top, ACCUM_MODE) != 0) {
		LOG_PRINT("load_program returned error");
	}

	mem_addr pc = TEXT_MIN;
	instr *next_instr = (instr*)addr_access(pc);
	if (next_instr->bin_op_code != LA_OP) {
		fail_test();
	}
	test_num++;
	if (next_instr->rd != 10 || strcmp(next_instr->label, "string_space") != 0) {
		fail_test();
	}
	test_num++;
	if (next_instr->operand != (DATA_MIN + (2 * sizeof(word)))) {
		printf("next_instr->operand = %u\n", next_instr->operand);
		printf("expected = %lu\n", (DATA_MIN + (2 * sizeof(word))));
		fail_test();
	}
	test_num++;

	pc += sizeof(instr);
	next_instr = (instr*)addr_access(pc);
	if (next_instr->bin_op_code != LI_OP) {
		fail_test();
	}
	test_num++;
	if (next_instr->rd != 11 && next_instr->imm != 1024) {
		fail_test();
	}
	test_num++;

	pc += sizeof(instr) * 2;
	next_instr = (instr*)addr_access(pc);
	if (next_instr->bin_op_code != SYSCALL_OP) {
		printf("next_instr->bin_op_code = %d\n", next_instr->bin_op_code);
		fail_test();
	}
	test_num++;
}
