#ifndef MEM_H
#include "mem.h"
#endif

#ifndef INSTR_H
#include "instr.h"
#endif


void execute(mem_addr text_top, word regs[]);

struct if_id_latch {
	instr* ir;
};

typedef struct if_id_latch if_id_latch;

struct id_exe_latch {
	instr* ir;
	word op_A; // registers[rs]
	word op_B; // registers[rt]
	mem_addr branch_target;

	// decoded syscall data
	uint8_t syscall_op;
	mem_addr syscall_str_loc;
	mem_addr syscall_str_len;
	word syscall_print_int_val;
};

typedef struct id_exe_latch id_exe_latch;

struct exe_mem_latch {
	uint8_t bin_op_code;
	word ALU_out; 
	//word op_B; // value for writing to memory in MEM stage, or an IMM for writing to $rd in case of LI
	uint8_t rd;
	uint8_t syscall_op;
	mem_addr load_src;
	mem_addr syscall_str_loc;
	mem_addr syscall_str_len;
};

typedef struct exe_mem_latch exe_mem_latch;

struct mem_wb_latch {
	uint8_t bin_op_code;
	//word MDR; // = memory[exe_mem.ALU_out], stores the value loaded from memory
	//word op_B;
	word ALU_out;
	uint8_t rd;
};

typedef struct mem_wb_latch mem_wb_latch;
