#ifndef INSTR_H
#define INSTR_H
#endif

#ifndef MEM_H
#include "mem.h"
#endif

// reg ops
#define END_OP 0
#define ADD_OP 1
#define ADDI_OP 2
#define B_OP 3
#define BEQZ_OP 4
#define BGE_OP 5
#define BNE_OP 6
#define LA_OP 7
#define LB_OP 8
#define LI_OP 9
#define SUBI_OP 10
#define SYSCALL_OP 11
#define NOP_OP 12
#define INVALID_OP 13

#define NUM_INSTR_T 13 
#define MAX_OPERANDS 3

// operands
#define UNUSED -1
#define RS 0
#define RT 1
#define RD 2
#define IMM 3
#define LABEL 4
#define REG_OFF 5
#define MEM_ADDR 6

#define INVALID_REG 33

struct instr {
	char* op_code;
	uint8_t bin_op_code;

	char* label;
	mem_addr operand;

	uint8_t rd;
	uint8_t rt;
	uint8_t rs;

	int32_t imm;
};

typedef struct instr instr;

void test_instr();
void parse_instr(char* text, instr **i);
