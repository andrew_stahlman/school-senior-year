#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "mem.h"
#include "logger.h"

#define SEG_SIZE 2048

static byte* data_seg;
static byte* text_seg;
static byte* stack_seg;
static mem_addr tos = STACK_MAX;
static word accum = 0;

void init_addr_space() {
	data_seg = (byte*) malloc(SEG_SIZE);
	text_seg = (byte*) malloc(SEG_SIZE);
	stack_seg = (byte*) malloc(SEG_SIZE);
}

byte* addr_access(mem_addr loc) {
	int offset;
	if (loc >= TEXT_MIN && loc < DATA_MIN) {
		offset = loc - TEXT_MIN;
		LOG_PRINT("Text offset: %d", offset);
		if (offset >= SEG_SIZE) {
			return 0;
		}
		return &data_seg[offset];
	} else if (loc >= DATA_MIN && loc < tos) {
		offset = loc - DATA_MIN;
		LOG_PRINT("Data offset: %d", offset);
		if (offset >= SEG_SIZE) {
			return 0;
		}
		return &text_seg[offset];
	} else if (loc > DATA_MIN && loc <= STACK_MAX) {
		offset = STACK_MAX - loc;
		LOG_PRINT("Stack offset: %d", offset);
		if (offset >= SEG_SIZE) {
			return 0;
		}
		return &stack_seg[offset];
	}
	LOG_PRINT("Invalid mem access!!!!\n");
}

bool read_word(mem_addr loc, word *w) {
	byte* l = addr_access(loc);
	if (l == 0) {
		return FALSE;
	}
	word *ptr = (word*)l;
	*w = *ptr; 
	return TRUE;
}

bool write_word(mem_addr loc, word w) {
	byte* l = addr_access(loc);
	if (l == 0) {
		return FALSE;
	}
	word* w_ptr = (word*)l;
	*w_ptr = w;
	return TRUE;
}

bool stack_push(mem_addr m) {
	word w;
	word w_test;
	LOG_PRINT("Push the value at this mem_address: %d\n", m);
	bool result = read_word(m, &w);
	LOG_PRINT("The value at %d is %d\n", m, w);
	LOG_PRINT("Writing %d to the top of the stack at %d\n", w, tos);
	result &= write_word(tos, w);
	read_word(tos, &w_test);
	LOG_PRINT("Wrote at %d: %d, Actual: %d\n", tos, w, w_test);
	tos -= sizeof(word);
	LOG_PRINT("New TOS: %d\n", tos);
	return result;
}

bool stack_pop(mem_addr m) {
	tos += sizeof(word);
	word w;
	bool result = read_word(tos, &w);
	result &= write_word(m, w);
	return result;
}

bool stack_add() {
	LOG_PRINT("In stack_add()\n");
	word w1, w2;
	tos += sizeof(word);
	bool result = read_word(tos, &w1);
	LOG_PRINT("Got word1=%d from %d\n", w1, tos);
	tos += sizeof(word);
	result &= read_word(tos, &w2);
	LOG_PRINT("Got word2=%d from %d\n", w2, tos);
	if (result) {
		word w3 = w1 + w2;
		LOG_PRINT("Result of add=%d\n", w3);
		result &= write_word(tos, w3);
		tos -= sizeof(word);
	}
	return result;
}

bool stack_mult() {
	word w1, w2;
	tos += sizeof(word);
	bool result = read_word(tos, &w1);
	tos += sizeof(word);
	result &= read_word(tos, &w2);
	if (result) {
		word w3 = w1 * w2;
		result &= write_word(tos, w3);
		tos -= sizeof(word);
	}
	return result;
}

void accum_load(mem_addr m) {
	read_word(m, &accum);
}

void accum_store(mem_addr m) {
	write_word(m, accum);
}

void accum_add(mem_addr m) {
	word w;
	read_word(m, &w);
	accum += w;
}

void accum_mult(mem_addr m) {
	word w;
	read_word(m, &w);
	accum *= w;
}
