#include "assembler.h"
#include "logger.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_REGS 32
#define STR_REG 10
#define STR_LEN_REG 11
#define SYSCALL_REG 20
#define STAGES 5

// SYSCALL OPS
#define PRINT 4
#define READ 8
#define EXIT 10

// our 32 registers
word regs[NUM_REGS];

// instruction count and cycles
unsigned int IC = 0;
unsigned int C = 0;


static byte get_cycle_count(uint8_t op_code);
void print_diagnostics();

int main(int argc, char* argv[]) {
	if (argc != 2) {
		puts("Usage: filename");
		return -1;
	}

	FILE *fp = fopen(argv[1], "r");
	if (fp == 0) {
		printf("Could not read from source file %s. Exiting.\n", argv[1]);
		return -1;
	}

	short r;
	for (r = 0; r < NUM_REGS; r++) {
		regs[r] = 0;
	}

	mem_addr text_top = TEXT_MIN;
	mem_addr data_top = DATA_MIN;
	// text_top is set to the address of the end of the last loaded instruction
	// data_top is set to the address of the end of the last word we init 
	if (load_program(fp, &data_top, &text_top, ACCUM_MODE) != 0) {
		printf("Error loading program %s. Exiting...\n", argv[1]);
		return -1;
	}

	mem_addr pc = TEXT_MIN;
	instr *next_instr = (instr*)addr_access(pc);
	LOG_PRINT("First instruction: %s", next_instr->op_code);
	bool user_mode = TRUE;
	while (user_mode && pc < text_top) {
		LOG_PRINT("Next instruction: %s", next_instr->op_code);
		switch (next_instr->bin_op_code) {
			case ADDI_OP:
				regs[next_instr->rt] = regs[next_instr->rs] + next_instr->imm;
				break;
			case B_OP:
				pc = next_instr->operand - sizeof(instr); // move to one instruction behind target
				break;
			case BEQZ_OP:
				if (regs[next_instr->rs] == 0) {
					pc = next_instr->operand - sizeof(instr); // move to one instruction behind target
				}
				break;
			case BGE_OP:
				if (regs[next_instr->rs] >= regs[next_instr->rt]) {
					pc = next_instr->operand - sizeof(instr); // move to one instruction behind target
				}
				break;
			case BNE_OP:
				//printf("rs = %d, rt = %d\n", next_instr->rs, next_instr->rt);
				//printf("values: rs = %d, rt = %d\n", regs[next_instr->rs], regs[next_instr->rt]);
				if (regs[next_instr->rs] != regs[next_instr->rt]) {
					pc = next_instr->operand - sizeof(instr); // move to one instruction behind target
				}
				break;
			case LA_OP:
				//printf("la from addr %lu\n", next_instr->operand);
				regs[next_instr->rd] = next_instr->operand;
				break;
			case LB_OP:
			{
				//printf("lb, imm = %d, rs = %lu", next_instr->imm, regs[next_instr->rs]);
				mem_addr target = next_instr->imm + regs[next_instr->rs];
				//printf("lb from addr %lu\n", target);
				//printf("reading into register: %d", next_instr->rt);
				byte *b = addr_access(target);
				regs[next_instr->rt] = *b;
				//printf("new register value: %d", regs[next_instr->rt]);
				break;
			}
			case LI_OP:
				//printf("imm = %d\n", next_instr->imm);
				regs[next_instr->rd] = next_instr->imm;
				break;
			case SUBI_OP:
				regs[next_instr->rt] = regs[next_instr->rs] - next_instr->imm;
				break;
			case SYSCALL_OP:
				//puts("Got syscall instruction.");
				switch (regs[SYSCALL_REG]) {
					case PRINT:
						//puts("Got print instruction.");
						printf("%s", (char*)addr_access(regs[STR_REG]));
						break;
					case READ:
					{
						char *target = (char*)addr_access(regs[STR_REG]);
						fgets(target, regs[STR_LEN_REG], stdin);
						//puts("read from stdin");
						//printf("string at target: %s\n", target);
						break;
					}
					case EXIT:
						user_mode = FALSE;
						break;
				}
				break;
		}

		IC++;
		C += get_cycle_count(next_instr->bin_op_code);

		//print_diagnostics(pc);
		pc += sizeof(instr);
		next_instr = (instr*)addr_access(pc);
	}

	float speedup = (8.0 * IC) / C; 
	printf("IC = %d\nC = %d\nSpeedup %f\n", IC, C, speedup);
}

void print_diagnostics(mem_addr pc) {
	printf("instruction #: %lu\n", (pc - TEXT_MIN) / sizeof(instr));
	printf("pc: %d\n", pc);
	short r;
	for (r = 0; r < NUM_REGS; r++) {
		if (regs[r] != 0) {
			printf("reg %d = %d\n", r, regs[r]);
		}
	}
}

static byte cycle_table[NUM_INSTR_T][STAGES];
static byte cycle_table_ready = FALSE;

static const byte ADDI_CYCLES[STAGES] = 	{ 2, 1, 2, 0, 1 };
static const byte B_CYCLES[STAGES] =		{ 2, 0, 2, 0, 0 };
static const byte BEQZ_CYCLES[STAGES] =	{ 2, 1, 2, 0, 0 };
static const byte BGE_CYCLES[STAGES] =		{ 2, 1, 2, 0, 0 };
static const byte BNE_CYCLES[STAGES] =		{ 2, 1, 2, 0, 0 };
static const byte LA_CYCLES[STAGES] =		{ 2, 0, 2, 0, 1 };
static const byte LB_CYCLES[STAGES] =		{ 2, 1, 2, 0, 1 };
static const byte LI_CYCLES[STAGES] =		{ 2, 0, 0, 0, 1 };
static const byte SUBI_CYCLES[STAGES] =	{ 2, 1, 2, 0, 1 };
static const byte SYSCALL_CYCLES[STAGES] =	{ 2, 1, 2, 2, 1 };

static init_cycle_table() {
	memcpy(cycle_table[ADDI_OP], ADDI_CYCLES, STAGES);
	memcpy(cycle_table[B_OP], B_CYCLES, STAGES);
	memcpy(cycle_table[BEQZ_OP], BEQZ_CYCLES, STAGES);
	memcpy(cycle_table[BGE_OP], BGE_CYCLES, STAGES);
	memcpy(cycle_table[BNE_OP], BNE_CYCLES, STAGES);
	memcpy(cycle_table[LA_OP], LA_CYCLES, STAGES);
	memcpy(cycle_table[LB_OP], LB_CYCLES, STAGES);
	memcpy(cycle_table[LI_OP], LI_CYCLES, STAGES);
	memcpy(cycle_table[SUBI_OP], SUBI_CYCLES, STAGES);
	memcpy(cycle_table[SYSCALL_OP], SYSCALL_CYCLES, STAGES);
	cycle_table_ready = TRUE;
}

static byte get_cycle_count(uint8_t op_code) {
	if (cycle_table_ready == FALSE) {
		init_cycle_table();
	}
	byte total = 0;
	short i;
	for (i = 0; i < STAGES; i++) {
		total += cycle_table[op_code][i]; 
	}
	return total;
}

