'''
Created on Oct 8, 2012

@author: astahlman
'''
from CA03.test.apstestcase import unittest, APSTestCase
from CA03.prod.utilities import Utilities
from CA03.prod.world import World

class WorldTest(APSTestCase):
        
    def test100_900_NonInteger(self):
        self.assertRaisesWithMessage(ValueError, Utilities.constructorErr, World, 5.1, 5)

    def test100_910_NegativeNumber(self):
        self.assertRaisesWithMessage(ValueError, Utilities.constructorErr, World, -4, 5)
    
    def test100_920_InvalidZero(self):
        self.assertRaisesWithMessage(ValueError, Utilities.constructorErr, World, 5, 0)
    
    def test100_930_InvalidString(self):
        self.assertRaisesWithMessage(ValueError, Utilities.constructorErr, World, "", 3)
    
    def test100_100_ValidInit(self):
        w = World(5, 5)
        self.assertEqual(w.getAge(), 0)
        self.assertTrue(w.getColumnCount() == w.getRowCount() == 5);
    
    def test200_900_LocationOccupied(self):
        w = World(5, 5)
        numPlants = w.addSeedAt(0, 0)
        self.assertEqual(numPlants, 1)
        self.assertRaisesWithMessage(ValueError, Utilities.addSeedAtAlreadyOccupiedErr, w.addSeedAt, 0, 0)
        
    def test200_910_InvalidLocation(self):
        w = World(5, 5)
        self.assertRaisesWithMessage(ValueError, Utilities.addSeedAtInvalidLocationErr, w.addSeedAt, 5, 5)
    
    def test300_100_IntensityCalculation(self):
        expected = [0, .707, 1, .707, 0, 0, 0, 0]
        w = World(1,1)
        for t in range(16):
            self.assertAlmostEqual(w.getSunIntensity(), expected[t % 8], 3)
            w.tick()
            
    def test400_100_IsolatedPlantDies(self):
        w = World(5, 5)
        self.assertEqual(w.getNumPlants(), 0)
        self.assertIsNone(w.getPlantAt(0,0))
        self.assertEqual(w.addSeedAt(0, 0), 1)
        self.assertEqual(w.getPlantAt(0,0), 0.0)
        w.tick()
        self.assertEqual(w.getNumPlants(), 0)
        self.assertIsNone(w.getPlantAt(0,0))
    
    def test400_110_NormalGrowth(self):
        w = World(5, 6)
        w.addSeedAt(1, 2)
        w.addSeedAt(2, 2)
        w.addSeedAt(2, 3)
        w.addSeedAt(3, 3)
        
        self.assertEqual(w.getPlantAt(1, 2), 0)
        self.assertEqual(w.getPlantAt(2, 2), 0)
        self.assertEqual(w.getPlantAt(2, 3), 0)
        self.assertEqual(w.getPlantAt(3, 3), 0)
        
        self.assertEqual(w.tick(), 1)
        
        self.assertEqual(w.getPlantAt(1, 2), 0)
        self.assertEqual(w.getPlantAt(1, 3), 0)
        self.assertEqual(w.getPlantAt(2, 2), 0)
        self.assertEqual(w.getPlantAt(2, 3), 0)
        self.assertEqual(w.getPlantAt(3, 2), 0)
        self.assertEqual(w.getPlantAt(3, 3), 0)
                
        self.assertEqual(w.tick(), 2)
        
        self.assertEqual(w.getPlantAt(1, 2), 0)
        self.assertAlmostEqual(w.getPlantAt(1, 3), .707, 3)
        self.assertEqual(w.getPlantAt(2, 1), 0)
        self.assertIsNone(w.getPlantAt(2, 2))
        self.assertIsNone(w.getPlantAt(2, 3))
        self.assertEqual(w.getPlantAt(2, 4), 0)
        self.assertAlmostEqual(w.getPlantAt(3, 2), .707, 3)
        self.assertAlmostEqual(w.getPlantAt(3, 3), .707, 3)
        
        self.assertEqual(w.tick(), 3)
        
        self.assertEqual(w.getPlantAt(1, 2), 0)
        self.assertAlmostEqual(w.getPlantAt(1, 3), .707, 3)
        self.assertEqual(w.getPlantAt(2, 1), 1)
        self.assertIsNone(w.getPlantAt(2, 2))
        self.assertIsNone(w.getPlantAt(2, 3))
        self.assertEqual(w.getPlantAt(2, 4), 1)
        self.assertAlmostEqual(w.getPlantAt(3, 2), 1.707, 3)
        self.assertAlmostEqual(w.getPlantAt(3, 3), 1.707, 3)
        
        self.assertEqual(w.tick(), 4)
        self.assertEqual(w.getPlantAt(1, 2), 0)
        self.assertAlmostEqual(w.getPlantAt(1, 3), .707 * 2, 3)
        self.assertAlmostEqual(w.getPlantAt(2, 1), 1.707, 3)
        self.assertIsNone(w.getPlantAt(2, 2))
        self.assertIsNone(w.getPlantAt(2, 3))
        self.assertEqual(w.getPlantAt(2, 4), 1)
        self.assertAlmostEqual(w.getPlantAt(3, 2), 1.707 + .707, 3)
        self.assertAlmostEqual(w.getPlantAt(3, 3), 1.707 + .707, 3)
        
        self.assertEqual(w.tick(), 5) #NO CHANGE
        self.assertEqual(w.getPlantAt(1, 2), 0)
        self.assertAlmostEqual(w.getPlantAt(1, 3), .707 * 2, 3)
        self.assertAlmostEqual(w.getPlantAt(2, 1), 1.707, 3)
        self.assertIsNone(w.getPlantAt(2, 2))
        self.assertIsNone(w.getPlantAt(2, 3))
        self.assertEqual(w.getPlantAt(2, 4), 1)
        self.assertAlmostEqual(w.getPlantAt(3, 2), 1.707 + .707, 3)
        self.assertAlmostEqual(w.getPlantAt(3, 3), 1.707 + .707, 3)
        
        self.assertEqual(w.tick(), 6) # (1,2) dies from overshading
        self.assertIsNone(w.getPlantAt(1, 2))
        self.assertAlmostEqual(w.getPlantAt(1, 3), .707 * 2, 3)
        self.assertAlmostEqual(w.getPlantAt(2, 1), 1.707, 3)
        self.assertIsNone(w.getPlantAt(2, 2))
        self.assertIsNone(w.getPlantAt(2, 3))
        self.assertEqual(w.getPlantAt(2, 4), 1)
        self.assertAlmostEqual(w.getPlantAt(3, 2), 1.707 + .707, 3)
        self.assertAlmostEqual(w.getPlantAt(3, 3), 1.707 + .707, 3)

    def test500_100_Iterator(self):
        w = World(3,3)
        w.addSeedAt(1,0)
        for height in w:
            print str(height)
            
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()