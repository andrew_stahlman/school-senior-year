'''
Created on Oct 11, 2012

@author: astahlman
'''
import unittest
from CA03.prod.grid import Grid

class GridTest(unittest.TestCase):


    def test100_900_NegativeRowCount(self):
        self.assertRaises(ValueError, Grid, -1, 5)
        
    def test100_900_FloatRowCount(self):
        self.assertRaises(ValueError, Grid, 1.3, 5)
        
    def test100_100_ValidSize(self):
        g = Grid(5, 5)
        self.assertEqual(g.getRowCount(), 5)
        self.assertEqual(g.getColumnCount(), 5)
        
    def test200_100_TestIterator(self):
        g = Grid(2, 3)
        expected = [(0,0), (1,0), (0,1), (1,1), (0, 2), (1,2)]
        i = 0
        for gp in g:
            actual = (gp.getRow(), gp.getColumn())
            self.assertEqual(actual, expected[i])
            i += 1
    
    def test300_100_GetNeighbors_TopLeftCorner(self):
        g = Grid(3, 3)
        expected = [(1,0), (0,1), (1,1)]
        neighbors = g.getNeighbors(0, 0)
        self.assertEqual(len(neighbors), 3)
        i = 0
        for gp in neighbors:
            self.assertEqual(gp.getRow(), expected[i][0])
            self.assertEqual(gp.getColumn(), expected[i][1])
            i += 1
        
    def test300_110_GetNeighbors_BottomRightCorner(self):
        g = Grid(3, 3)
        expected = [(1,1), (2,1), (1,2)]
        neighbors = g.getNeighbors(2, 2)
        self.assertEqual(len(neighbors), 3)
        i = 0
        for gp in neighbors:
            self.assertEqual(gp.getRow(), expected[i][0])
            self.assertEqual(gp.getColumn(), expected[i][1])
            i += 1
    
    def test300_120_GetNeighbors_MiddleOfGrid(self):
        g = Grid(3, 3)
        expected = [(0,0), (1,0), (2,0), (0,1), (2,1), (0,2), (1,2), (2,2)]
        neighbors = g.getNeighbors(1, 1)
        self.assertEqual(len(neighbors), 8)
        i = 0
        for gp in neighbors:
            self.assertEqual(gp.getRow(), expected[i][0])
            self.assertEqual(gp.getColumn(), expected[i][1])
            i += 1
            
    def test400_100_GetDownVector(self):
        g = Grid(3, 3)
        positions = g.getGridPositionsOnVector(0, 2, 1, 0)
        expected = [(1,2), (2,2)]
        self.assertEquals(len(positions), 2)
        i = 0
        for gp in positions:
            self.assertEquals(gp.getRow(), expected[i][0])
            self.assertEquals(gp.getColumn(), expected[i][1])
            i += 1
    
    def test400_110_GetRightVector(self):
        g = Grid(3, 3)
        positions = g.getGridPositionsOnVector(2, 0, 0, 1)
        self.assertEqual(len(positions), 2)
        expected = [(2,1), (2,2)]
        i = 0
        for gp in positions:
            self.assertEquals(gp.getRow(), expected[i][0])
            self.assertEquals(gp.getColumn(), expected[i][1])
            i += 1

    def test400_110_GetUpLeftVector(self):
        g = Grid(3, 3)
        positions = g.getGridPositionsOnVector(1, 1, -1, -1)
        self.assertEqual(len(positions), 1)
        self.assertEqual(positions[0].getRow(), 0)
        self.assertEqual(positions[0].getColumn(), 0)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()