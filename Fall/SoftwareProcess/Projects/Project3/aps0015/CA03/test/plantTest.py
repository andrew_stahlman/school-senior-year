'''
Created on Oct 11, 2012

@author: astahlman
'''
import unittest
from CA03.prod.plant import Plant
from CA03.prod.gridposition import GridPosition

class PlantTest(unittest.TestCase):

    g = GridPosition(1, 1)
    
    def test100_900_InvalidHeight(self):
        self.assertRaises(ValueError, Plant, PlantTest.g, -0.1)
    
    def test100_100_ValidPlant(self):
        p = Plant(PlantTest.g, 1)
        self.assertEqual(p.getHeight(), 1.0)
        self.assertEqual(p.getRow(), 1)
        self.assertEqual(p.getColumn(), 1)
        
    def test100_110_ValidPlantDefaultHeight(self):
        p = Plant(PlantTest.g)
        self.assertEqual(p.getHeight(), 0.0)
        self.assertEqual(p.getRow(), 1)
        self.assertEqual(p.getColumn(), 1)
    
    def test200_100_PlantGrow(self):
        p = Plant(PlantTest.g)
        self.assertEqual(p.getHeight(), 0)
        self.assertEqual(p.getShadedStreak(), 0)
        
        p.grow(0)
        self.assertEqual(p.getHeight(), 0)
        self.assertEqual(p.getShadedStreak(), 1)
        
        p.grow(.1)
        self.assertEqual(p.getHeight(), .1)
        self.assertEqual(p.getShadedStreak(), 0)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()