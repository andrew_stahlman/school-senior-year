'''
Created on Oct 11, 2012

@author: astahlman
'''
import unittest
from CA03.prod.gridposition import GridPosition

class GridPositionTest(unittest.TestCase):

    def test100_900_NonIntegerRowCol(self):
        self.assertRaises(ValueError, GridPosition, 1.2, 2.4)
        
    def test100_910_NonIntegerRowCol(self):
        self.assertRaises(ValueError, GridPosition, -1, 2)
        
    def test100_100_NewPlant(self):
        gp = GridPosition(1, 1)
        self.assertEqual(gp.getRow(), 1)
        self.assertEqual(gp.getColumn(), 1)
        p = gp.addPlant()
        self.assertEqual(p, gp.getPlant())
        self.assertEqual(p.getHeight(), 0.0)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()