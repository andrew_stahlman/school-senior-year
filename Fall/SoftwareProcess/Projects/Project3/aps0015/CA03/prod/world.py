'''
Created on Oct 8, 2012

@author: astahlman
'''
import math
from CA03.prod.grid import Grid
from CA03.prod.utilities import Utilities
        
class World(object):
    '''
    classdocs
    '''
    SHADED_INTENSITY = 0.0
    
    def __iter__(self):
        return self
    
    def next(self):
        gp = self.grid.next()
        return gp.getPlantHeight()

    def __init__(self, rowCount, columnCount):
        '''
        Constructor
        '''
        if not Utilities.isInt(rowCount) or not Utilities.isInt(columnCount):
            raise ValueError(Utilities.constructorErr)
        elif rowCount <= 0 or columnCount <= 0:
            raise ValueError(Utilities.constructorErr)
        else:
            self.rowCount = rowCount
            self.columnCount = columnCount
            self.age = 0
            self.numPlants = 0
            self.grid = Grid(self.getRowCount(), self.getColumnCount())

    def addSeedAt(self, row, column):
        if not self.grid.isLocationOnGrid(row, column):
            raise ValueError(Utilities.addSeedAtInvalidLocationErr)
        if self.grid.getPlantHeightAt(row, column) is None:
            newPlantHeight = self.grid.plantSeedAt(row, column)
            assert(newPlantHeight == 0.0)
            self.numPlants += 1
            return self.getNumPlants()
        else:
            raise ValueError(Utilities.addSeedAtAlreadyOccupiedErr)
    
    def getPlantAt(self, row, column):
        if not self.grid.isLocationOnGrid(row, column):
            raise ValueError(Utilities.getPlantAtErr)
        else:
            return self.grid.getPlantHeightAt(row, column)
    
    def getAge(self):
        return self.age
    
    def getColumnCount(self):
        return self.columnCount
    
    def getRowCount(self):
        return self.rowCount
        
    def filterNeighbors(self, neighbors):
        result = []
        for neighbor in neighbors:
            if neighbor.getPlant() is not None:
                result.append(neighbor)
        return result
                
    def tick(self):
        print "tick: " + str(self.getAge())
        toRemove = []
        for gp in self.grid:
            plant = gp.getPlant()
            if plant is not None:
                if self.isPlantShaded(plant.getRow(), plant.getColumn()):
                    intensity = World.SHADED_INTENSITY
                else:
                    intensity = self.getSunIntensity()
                plant.grow(intensity)
                
                neighborPositions = self.grid.getNeighbors(gp.getRow(), gp.getColumn())
                neighbors = self.filterNeighbors(neighborPositions)
                        
                if len(neighbors) < 2 or len(neighbors) > 3:
                    toRemove.append(plant)
                elif plant.getShadedStreak() > 5:
                    toRemove.append(plant)
        
        # additions
        toAdd = []
        for gp in self.grid:
            if gp.getPlant() is None:
                neighborPositions = self.grid.getNeighbors(gp.getRow(), gp.getColumn())
                neighbors = self.filterNeighbors(neighborPositions)
                if len(neighbors) == 3:
                    toAdd.append(gp)
                    
        for plant in toRemove:
            print "Removing plant at: (" + str(plant.getRow()) + ", " + str(plant.getColumn())
            self.removePlantAt(plant.getRow(), plant.getColumn())
        
        for gp in toAdd:
            print "Adding seed at: (" + str(gp.getRow()) + ", " + str(gp.getColumn())
            self.addSeedAt(gp.getRow(), gp.getColumn())
            
        self.age += 1
        return self.age
    
    def isPlantShaded(self, row, column):
        height = self.grid.getPlantHeightAt(row, column)
        slope = self.getSlope()
        intermediates = self.grid.getGridPositionsOnVector(row, column, slope[0], slope[1])
        for gp in intermediates:
            if (gp.getPlantHeight() >= height):
                return True
            
        return False
    
    def removePlantAt(self, row, column):
        self.grid.removePlantAt(row, column)
        self.numPlants -= 1
    
    def getSunIntensity(self):
        if (self.getAge() == 5):
            print "At breakpoint"
        radians = math.radians(45.0 * (self.getAge() % 8))
        intensity = max(math.sin(radians), 0.0)
        if Utilities.almostEqual(intensity, 0.0):
            intensity = 0.0
        return intensity
    
    def getSlope(self):
        rises = [0, 1, 1, 1, 0, -1, -1, -1]
        runs = [1, 1, 0, -1, -1, -1, 0, 1]
        age = self.getAge()
        return (rises[age % 8], runs[age % 8])
    
    def getNumPlants(self):
        return self.numPlants;