'''
Created on Oct 12, 2012

@author: astahlman
'''

class Utilities(object):
    '''
    Utility functions
    '''
    FLOAT_TOLERANCE = .001
    constructorErr = "__init__: invalid constructor"
    addSeedAtAlreadyOccupiedErr = "addSeedAt: location already occupied"
    addSeedAtInvalidLocationErr = "addSeedAt: invalid grid location"
    getPlantAtErr = "getPlantAt: invalid grid location"
    invalidLocationErr = "invalid grid location"
    
    @classmethod
    def isInt(cls, n):
        try:
            return n is not None and n % 1 == 0
        except TypeError:
            return False
        
    @classmethod
    def almostEqual(cls, float1, float2, tolerance=FLOAT_TOLERANCE):
        delta = float1 - float2
        return abs(delta) <= tolerance
