'''
Created on Oct 11, 2012

@author: astahlman
'''

class Plant(object):
    '''
    classdocs
    '''

    def __init__(self, gridPosition, height = 0.0):
        '''
        Constructor
        '''
        if (height < 0):
            raise ValueError("Height must be >= 0.0")
        else:
            height = float(height)
        self.height = height
        self.gridPosition = gridPosition
        self.shadedStreak = 0
    
    def grow(self, intensity):
        assert(intensity >= 0)
        self.height = self.height + float(intensity)
        if (float(intensity) == 0.0):
            self.shadedStreak += 1
        else:
            self.shadedStreak = 0
            
    def getHeight(self):
        return self.height
    
    def getShadedStreak(self):
        return self.shadedStreak
    
    def getGridPosition(self):
        return self.gridPosition
    
    def getRow(self):
        return self.getGridPosition().getRow()
    
    def getColumn(self):
        return self.getGridPosition().getColumn()