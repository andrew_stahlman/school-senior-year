'''
Created on Oct 11, 2012

@author: astahlman
'''
from CA03.prod.plant import Plant
from CA03.prod.utilities import Utilities

class GridPosition(object):
    '''
    classdocs
    '''
    
    def __init__(self, row, column):
        '''
        Constructor
        '''
        if not Utilities.isInt(row) or not Utilities.isInt(column) or row < 0 or column < 0:
            raise ValueError("Invalid coordinates")
        self.row = row
        self.column = column
        self.plant = None
    
    def getRow(self):
        return self.row
    
    def getColumn(self):
        return self.column
    
    def getPlant(self):
        return self.plant
    
    def getPlantHeight(self):
        plant = self.getPlant()
        return plant.getHeight() if plant is not None else None
    
    def removePlant(self):
        self.plant = None
        return self.plant
    
    def addPlant(self):
        self.plant = Plant(self)
        return self.plant
    
    