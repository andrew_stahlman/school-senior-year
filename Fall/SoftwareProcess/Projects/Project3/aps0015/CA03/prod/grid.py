'''
Created on Oct 11, 2012

@author: astahlman
'''
from CA03.prod.gridposition import GridPosition
from CA03.prod.utilities import Utilities

class Grid(object):
    '''
    classdocs
    '''

    def __init__(self, numRows, numColumns):
        '''
        Constructor
        '''
        if not Utilities.isInt(numRows) or not Utilities.isInt(numColumns) or numRows < 0 or numColumns < 0:
            raise ValueError("Invalid number of rows and columns")
        self.numRows = numRows
        self.numColumns = numColumns
        self.gridPositions = [[GridPosition(row, col) for row in range(numRows)] for col in range(numColumns)]
        self.iterIndex = 0
    
    def __iter__(self):
        return self
    
    def next(self):
        col = self.iterIndex / self.getRowCount()
        row = self.iterIndex % self.getRowCount()
        if not self.isLocationOnGrid(row, col):
            self.iterIndex = 0
            raise StopIteration
        else:
            self.iterIndex += 1
            self.lastIterRowCol = (row, col)
            return self.getGridPositionAt(row, col)
    
    def getRowCount(self):
        return self.numRows
    
    def getColumnCount(self):
        return self.numColumns
    
    def getGridPositionAt(self, row, column):
        if self.isLocationOnGrid(row, column):
            return self.gridPositions[column][row]
        else:
            raise ValueError(Utilities.invalidLocationErr)
    
    def getPlantAt(self, row, column):
        if self.isLocationOnGrid(row, column):
            pos = self.getGridPositionAt(row, column)
            return pos.getPlant()
        else:
            raise ValueError(Utilities.invalidLocationErr)
    
    def getPlantHeightAt(self, row, column):
        if self.isLocationOnGrid(row, column):
            plant = self.getPlantAt(row, column)
            return plant.getHeight() if plant is not None else None
        else:
            raise ValueError(Utilities.invalidLocationErr)
    
    def plantSeedAt(self, row, column):
        if self.isLocationOnGrid(row, column):
            pos = self.getGridPositionAt(row, column)
            pos.addPlant()
            return self.getPlantHeightAt(row, column)
        else:
            raise ValueError(Utilities.invalidLocationErr)
        
    def removePlantAt(self, row, column):
        if self.isLocationOnGrid(row, column):
            gp = self.getGridPositionAt(row, column)
            return gp.removePlant()
        else:
            raise ValueError(Utilities.invalidLocationErr)
            
    def isLocationOnGrid(self, row, column):
        if not Utilities.isInt(row) or not Utilities.isInt(column):
            return False
        else:
            return row < self.getRowCount() and row >= 0 and column < self.getColumnCount() and column >= 0
        
    def getNeighbors(self, row, column):
        ''' Returns a list of tuples (row, col)'''
        p = []
        if not self.isLocationOnGrid(row, column):
            raise ValueError(Utilities.invalidLocationErr)
        for c in range(max(column - 1, 0), min(column + 2, self.getColumnCount())):
            for r in range(max(row - 1, 0), min(row + 2, self.getRowCount())):
                if not (r == row and c == column):
                    p.append(self.getGridPositionAt(r, c))
        return p
                    
    def getGridPositionsOnVector(self, row, column, rise, run):
        if not self.isLocationOnGrid(row, column):
            raise ValueError(Utilities.invalidLocationErr)
        if rise < -1 or rise > 1 or run < -1 or run > 1:
            raise ValueError("rise and run must be -1, 0 or 1")
        results = []
        onGrid = True
        while onGrid:
            row += rise
            column += run
            onGrid = self.isLocationOnGrid(row, column)
            if onGrid:
                results.append(self.getGridPositionAt(row, column))
        return results
        