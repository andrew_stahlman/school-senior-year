'''
Created on Oct 19, 2012

@author: astahlman
'''
FLOAT_TOLERANCE = .001

def isInt(n):
    try:
        return n is not None and n % 1 == 0
    except TypeError:
        return False

def isBlank(s):
    return not s or len(s.strip()) == 0

def countLeadingSpace(line):
    return len(line) - len(line.lstrip())
    
def almostEqual(float1, float2, tolerance=FLOAT_TOLERANCE):
    delta = float1 - float2
    return abs(delta) <= tolerance