'''
Created on Oct 19, 2012

@author: astahlman
'''
import CA04.prod.utilities as utilities

invalidNameErr = "init : Invalid name"
invalidMethodCountErr = "init : Invalid method count"
invalidLocCountErr = " init : Invalid LOC count"
invalidDesignErr = "init : Invalid design"
    
class Component(object):
    '''
    classdocs
    '''  
    MIN_LOC = 2
    
    def __init__(self, name=None, methodCount=None, locCount=None, design="OO"):
        '''
        Create an instance of a component
        '''
        try:
            if utilities.isBlank(name):
                raise ValueError(invalidNameErr)
        except AttributeError:
            raise ValueError(invalidNameErr)
            
        if design == "OO":
            minMethodCount = 0
        elif design == "FD":
            minMethodCount = 1
        else:
            raise ValueError(invalidDesignErr)
        
        if methodCount is None or not utilities.isInt(methodCount) or methodCount < minMethodCount:
            raise ValueError(invalidMethodCountErr)
        if locCount is None or not utilities.isInt(locCount) or locCount < Component.MIN_LOC:
            raise ValueError(invalidLocCountErr)
        
        self.name = name
        self.methodCount = methodCount
        self.locCount = locCount
        self.design = design
        
    def getName(self):
        return self.name
    
    def getMethodCount(self):
        return self.methodCount
    
    def getLocCount(self):
        return self.locCount
    
    def getDesign(self):
        return self.design