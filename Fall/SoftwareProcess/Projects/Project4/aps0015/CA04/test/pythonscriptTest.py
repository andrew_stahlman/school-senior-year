'''
Created on Oct 19, 2012

@author: astahlman
'''
import unittest
import CA04.prod.pythonscript as pythonscript
from CA04.prod.pythonscript import PythonScript
from CA04.test.apstestcase import APSTestCase
import os.path

BASE_PATH = os.path.join(os.path.dirname(__file__), '../..')
FILE_PATH = os.path.join(BASE_PATH, "suite/pythonFile.py")

class PythonScriptTest(APSTestCase):

    def test100_900_MissingFileName(self):
        self.assertRaisesWithMessage(ValueError, pythonscript.invalidFileNameErr, PythonScript, fileName=None)
    
    def test100_910_EmptyFileName(self):
        self.assertRaisesWithMessage(ValueError, pythonscript.invalidFileNameErr, PythonScript, fileName="")
    
    def test100_920_IntFileName(self):
        self.assertRaisesWithMessage(ValueError, pythonscript.invalidFileNameErr, PythonScript, fileName=42)
    
    def test100_930_NonPythonFileName(self):
        self.assertRaisesWithMessage(ValueError, pythonscript.invalidFileNameErr, PythonScript, fileName=os.path.join(BASE_PATH, "suite/somefile.java"))
    
    def test100_940_NonExistentFileName(self):
        self.assertRaisesWithMessage(ValueError, pythonscript.invalidFileNameErr, PythonScript, fileName="dne.py")
    
    def test100_950_TooShortFileName(self):
        self.assertRaisesWithMessage(ValueError, pythonscript.invalidFileNameErr, PythonScript, fileName=os.path.join(BASE_PATH, "suite/py"))
    
    def test100_100_PythonFile(self):
        myFile = PythonScript(fileName=FILE_PATH)
        components = myFile.extractDesign()
        self.assertEqual("ClassA", components[0].getName())
        self.assertEqual("ClassB", components[1].getName())
        self.assertEqual("Func", components[2].getName())
        
        self.assertEqual(2, components[0].getMethodCount())
        self.assertEqual(1, components[1].getMethodCount())
        self.assertEqual(1, components[2].getMethodCount())
        
        self.assertEqual(5, components[0].getLocCount())
        self.assertEqual(3, components[1].getLocCount())
        self.assertEqual(2, components[2].getLocCount())
        
        self.assertEqual("OO", components[0].getDesign())
        self.assertEqual("OO", components[1].getDesign())
        self.assertEqual("FD", components[2].getDesign())
        
    def test100_110_ValidationStrings(self):
        myFile = PythonScript(os.path.join(BASE_PATH, "suite/validationStrings.py"))
        components = myFile.extractDesign()
        self.assertEqual("EvilStrings", components[0].getName())
        self.assertEqual(1, components[0].getMethodCount())
        self.assertEqual(27, components[0].getLocCount())
        self.assertEqual("OO", components[0].getDesign())
        self.assertEqual(1, len(components))
        
    def test100_120_ValidationNoComponents(self):
        myFile = PythonScript(os.path.join(BASE_PATH, "suite/validationNoComponents.py"))
        components = myFile.extractDesign()
        self.assertEqual(0, len(components))
        
    def test100_130_ValidationHybrid(self):
        myFile = PythonScript(os.path.join(BASE_PATH, "suite/validationHybrid.py"))
        components = myFile.extractDesign()
        self.assertEqual("HybridClassA", components[0].getName())
        self.assertEqual(3, components[0].getMethodCount())
        self.assertEqual(7, components[0].getLocCount())
        self.assertEqual("OO", components[0].getDesign())
        
        self.assertEqual("HybridClassB", components[1].getName())
        self.assertEqual(0, components[1].getMethodCount())
        self.assertEqual(2, components[1].getLocCount())
        self.assertEqual("OO", components[1].getDesign())
        
        self.assertEqual("hybridFunctionA", components[2].getName())
        self.assertEqual(1, components[2].getMethodCount())
        self.assertEqual(2, components[2].getLocCount())
        self.assertEqual("FD", components[2].getDesign())
        
        self.assertEqual("hybridFunctionB", components[3].getName())
        self.assertEqual(1, components[3].getMethodCount())
        self.assertEqual(2, components[3].getLocCount())
        self.assertEqual("FD", components[3].getDesign())
        
        self.assertEqual("hybridFunctionC", components[4].getName())
        self.assertEqual(1, components[4].getMethodCount())
        self.assertEqual(2, components[4].getLocCount())
        self.assertEqual("FD", components[4].getDesign())
 
 
    def test100_140_ValidationFD(self):
        myFile = PythonScript(os.path.join(BASE_PATH, "suite/validationFD.py"))
        components = myFile.extractDesign()

        self.assertEqual("functionA", components[0].getName())
        self.assertEqual(1, components[0].getMethodCount())
        self.assertEqual(2, components[0].getLocCount())
        self.assertEqual("FD", components[0].getDesign())
        
        self.assertEqual("functionB", components[1].getName())
        self.assertEqual(1, components[1].getMethodCount())
        self.assertEqual(2, components[1].getLocCount())
        self.assertEqual("FD", components[1].getDesign())
        
        self.assertEqual("functionC", components[2].getName())
        self.assertEqual(1, components[2].getMethodCount())
        self.assertEqual(2, components[2].getLocCount())
        self.assertEqual("FD", components[2].getDesign())
        
        self.assertEqual("functionD", components[3].getName())
        self.assertEqual(1, components[3].getMethodCount())
        self.assertEqual(2, components[3].getLocCount())
        self.assertEqual("FD", components[3].getDesign()) 
        
    def test100_150_ValidationDocstrings(self):
        myFile = PythonScript(os.path.join(BASE_PATH, "suite/validationDocstrings.py"))
        components = myFile.extractDesign()

        self.assertEqual("docStringFunction1", components[0].getName())
        self.assertEqual(1, components[0].getMethodCount())
        self.assertEqual(2, components[0].getLocCount())
        self.assertEqual("FD", components[0].getDesign())
        
        self.assertEqual("docStringFunction2", components[1].getName())
        self.assertEqual(1, components[1].getMethodCount())
        self.assertEqual(2, components[1].getLocCount())
        self.assertEqual("FD", components[1].getDesign())
        
        self.assertEqual("docStringFunction3", components[2].getName())
        self.assertEqual(1, components[2].getMethodCount())
        self.assertEqual(2, components[2].getLocCount())
        self.assertEqual("FD", components[2].getDesign())
        
        self.assertEqual("DocStringClassA", components[3].getName())
        self.assertEqual(4, components[3].getMethodCount())
        self.assertEqual(9, components[3].getLocCount())
        self.assertEqual("OO", components[3].getDesign())
    
    def test100_160_ValidationComments(self):
        myFile = PythonScript(os.path.join(BASE_PATH, "suite/validationComments.py"))
        components = myFile.extractDesign()
        self.assertEqual(0, len(components))  
      
    def test100_170_ValidationOO(self):
        myFile = PythonScript(os.path.join(BASE_PATH, "suite/validationOO.py"))
        components = myFile.extractDesign()
        
        self.assertEqual("ClassA", components[0].getName())
        self.assertEqual(2, components[0].getMethodCount())
        self.assertEqual(5, components[0].getLocCount())
        self.assertEqual("OO", components[0].getDesign())  

        self.assertEqual("ClassB", components[1].getName())
        self.assertEqual(1, components[1].getMethodCount())
        self.assertEqual(3, components[1].getLocCount())
        self.assertEqual("OO", components[1].getDesign())  
        
        self.assertEqual("ClassC", components[2].getName())
        self.assertEqual(0, components[2].getMethodCount())
        self.assertEqual(2, components[2].getLocCount())
        self.assertEqual("OO", components[2].getDesign())  

    def test100_180_ValidationRandom(self):
        myFile = PythonScript(os.path.join(BASE_PATH, "suite/validationRandom.py"))
        components = myFile.extractDesign()
        
        self.assertEqual("det", components[0].getName())
        self.assertEqual(1, components[0].getMethodCount())
        self.assertEqual(24, components[0].getLocCount())
        self.assertEqual("FD", components[0].getDesign())
        
        self.assertEqual("num", components[1].getName())
        self.assertEqual(1, components[1].getMethodCount())
        self.assertEqual(7, components[1].getLocCount())
        self.assertEqual("FD", components[1].getDesign())
        
        self.assertEqual("what", components[2].getName())
        self.assertEqual(1, components[2].getMethodCount())
        self.assertEqual(26, components[2].getLocCount())
        self.assertEqual("FD", components[2].getDesign())
        
        self.assertEqual("encrypt", components[3].getName())
        self.assertEqual(1, components[3].getMethodCount())
        self.assertEqual(17, components[3].getLocCount())
        self.assertEqual("FD", components[3].getDesign())
        
        self.assertEqual("decrypt", components[4].getName())
        self.assertEqual(1, components[4].getMethodCount())
        self.assertEqual(19, components[4].getLocCount())
        self.assertEqual("FD", components[4].getDesign())
        
    def test200_100_isSingleLineComment(self):
        comment = []
        comment.append("    #test")
        comment.append("#test")
        comment.append("''' test '''")
        comment.append("    ''' test ''' ")
        comment.append('    """ test  """    ')
        
        loc = []
        loc.append("test = 4")
        loc.append("test = ''' Some string '''")
        loc.append("test = ''''''")
        loc.append("test = '''")
        
        for c in comment:
            self.assertTrue(PythonScript.isSingleLineComment(c))
            
        for l in loc:
            self.assertFalse(PythonScript.isSingleLineComment(l))
            
    def test300_100_isMultiLineCommentOpener(self):
        comment = []
        comment.append("'''\n")
        comment.append('    """ Comment\n')
        
        loc = []
        loc.append("test = ''''''")
        loc.append('test = """"""')
        loc.append("test = ''")
        loc.append('test = " "')
        
        for c in comment:
            self.assertTrue(PythonScript.isMultiLineCommentOpener(c))
            
        for l in loc:
            self.assertFalse(PythonScript.isMultiLineCommentOpener(l))
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()