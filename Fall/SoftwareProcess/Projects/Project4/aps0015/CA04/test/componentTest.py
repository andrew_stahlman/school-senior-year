'''
Created on Oct 19, 2012

@author: astahlman
'''
import unittest
from CA04.test.apstestcase import APSTestCase
import CA04.prod.component as component 
from CA04.prod.component import Component as Component

class ComponentTest(APSTestCase):

    def test100_100_ValidInit(self):
        c = Component(name="myComponent", methodCount=10, locCount=10, design="FD")
        self.assertEqual(c.getName(), "myComponent")
        self.assertEquals(c.getMethodCount(), 10)
        self.assertEqual(c.getLocCount(), 10)
        self.assertEqual(c.getDesign(), "FD")
    
    def test100_110_ValidInitDefaultDesign(self):
        c = Component(name="myComponent", methodCount=10, locCount=10)
        self.assertEqual(c.getName(), "myComponent")
        self.assertEquals(c.getMethodCount(), 10)
        self.assertEqual(c.getLocCount(), 10)
        self.assertEqual(c.getDesign(), "OO")
        
    def test100_900_InitInvalidName(self):
        self.assertRaisesWithMessage(ValueError, component.invalidNameErr, Component, name=None, methodCount=10, locCount=10) 
        self.assertRaisesWithMessage(ValueError, component.invalidNameErr, Component, name="", methodCount=10, locCount=10) 
        self.assertRaisesWithMessage(ValueError, component.invalidNameErr, Component, name=" ", methodCount=10, locCount=10) 
        self.assertRaisesWithMessage(ValueError, component.invalidNameErr, Component, name= 17, methodCount=10, locCount=10) 

    def test100_910_InitInvalidMethodCount(self):
        self.assertRaisesWithMessage(ValueError, component.invalidMethodCountErr, Component, name="myComponent", locCount=10)
        self.assertRaisesWithMessage(ValueError, component.invalidMethodCountErr, Component, name="myComponent", methodCount=None, locCount=10)
        self.assertRaisesWithMessage(ValueError, component.invalidMethodCountErr, Component, name="myComponent", methodCount=-1, locCount=10)
        self.assertRaisesWithMessage(ValueError, component.invalidMethodCountErr, Component, name="myComponent", methodCount=1.3, locCount=10)
   
    def test100_920_InitInvalidLocCount(self):
        self.assertRaisesWithMessage(ValueError, component.invalidLocCountErr, Component, name="myComponent", methodCount=10)
        self.assertRaisesWithMessage(ValueError, component.invalidLocCountErr, Component, name="myComponent", methodCount=3, locCount=None, design="OO")
        self.assertRaisesWithMessage(ValueError, component.invalidLocCountErr, Component, name="myComponent", methodCount=3, locCount=1, design="OO")
        self.assertRaisesWithMessage(ValueError, component.invalidLocCountErr, Component, name="myComponent", methodCount=3, locCount=1.3, design="OO")

    def test100_930_InitInvalidDesign(self):
        self.assertRaisesWithMessage(ValueError, component.invalidDesignErr, Component, name="myComponent", methodCount=10, locCount=10, design="xx")

    def test100_940_InitInvalidDesignAndMethodCount(self):
        self.assertRaisesWithMessage(ValueError, component.invalidMethodCountErr, Component, name="myComponent", methodCount=0, locCount=42, design="FD")

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()