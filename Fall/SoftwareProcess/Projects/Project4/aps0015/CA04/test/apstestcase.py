'''
Created on Oct 12, 2012

@author: astahlman
'''
import unittest

class APSTestCase(unittest.TestCase):
    '''
    Custom TestCase class that extends assertRaises to check the error message
    Credit for this technique: 
    http://stackoverflow.com/questions/8672754/how-to-show-the-error-messages-caught-by-assertraises-in-unittest-in-python2-7
    '''

    def assertRaisesWithMessage(self, errCls, errMsg, func, *args, **kwargs):
        try:
            func(*args, **kwargs)
            self.assertFail()
        except Exception as exc:
            self.assertIsInstance(exc, errCls)
            self.assertEqual(errMsg, exc.args[0])