'''
Created on Oct 27, 2012

@author: astahlman
'''
import unittest
import CA04.prod.utilities as utilities

class Test(unittest.TestCase):
    ''' Note: isInt and almostEqual reused from Project3, already tested there'''
    def test100_100_IsBlank(self):
        self.assertTrue(utilities.isBlank(" "))
        self.assertTrue(utilities.isBlank("  "))
        self.assertTrue(utilities.isBlank("    "))
        self.assertTrue(utilities.isBlank(""))
        
        self.assertFalse(utilities.isBlank("  _"))
        self.assertFalse(utilities.isBlank("1    "))
        
    def test200_100_CountLeadingTabs(self):
        self.assertEqual(utilities.countLeadingSpace(""), 0)
        self.assertEqual(utilities.countLeadingSpace("    Hi"), 4)
        self.assertEqual(utilities.countLeadingSpace("Hi"), 0)
        self.assertEqual(utilities.countLeadingSpace("        Hi"), 8)
        self.assertEqual(utilities.countLeadingSpace("Hi    "), 0)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()