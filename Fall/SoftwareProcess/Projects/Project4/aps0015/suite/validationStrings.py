'''
    Validation Suite:  Strings
    Baselined:  24 Mar 2011
    Modified:  17 Oct 2012
    @author:  D. Umphress
        Component("EvilStrings", 1, 27, "OO")
'''

class EvilStrings:
    """ this
    is 
    a
    multiline
    docstring """  

    emptyString1 = ""
    emptyString2 = ''
    emptyString3 = """"""
    emptyString4 = ''''''
    
    oneLineString1 = "abc"
    oneLineString2 = 'abc'
    oneLineString3 = """abc"""
    oneLineString4 = '''abc'''
    
    multiLineString1 = """
    abc """
    multiLineString2 = \
        """abc"""
    multiLineString3 = \
        """
    abc
        """
    multiLineString4 = """
    #"""
    
    multiLineString5 = """
    
    """
    
    multiLineString6 = """   "
    """
    
    def __init__(self, initString):
        self.internalString = initString + '''  
        '''