'''
    Validation Suite:  Functions
    Baselined:  29 Sep 2009
    Modified:  1 Oct 2012
    @author:  D. Umphress
        Component("functionA",1,2,"FD")
        Component("functionB",1,2,"FD")
        Component("functionC",1,2,"FD")
        Component("functionD",1,2,"FD")
'''

import sys

def functionA():
    pass

def functionB(parm1):
    pass

def functionC(parm1, parm2):
    pass

def functionD(*parm):
    pass
