'''
    Validation Suite:  Classes
    Baselined:  29 Sep 2009
    Modified:  1 Oct 2012
    @author:  D. Umphress
          Component("ClassA",2,5,"OO")
          Component("ClassB",1,3,"OO")
          Component("ClassC",0,2,"OO")
'''

import sys

class ClassA():
    def __init__(self):
        pass

    def methodA(self, parm1=""):
        pass


class ClassB(ClassA):
    def methodB(self):
        pass


class ClassC(ClassB):
        pass
