'''
Created on Oct 19, 2012

@author: astahlman
'''

from CA04.prod.component import Component
import CA04.prod.utilities as utilities
import re

invalidFileNameErr = "init : Invalid file name"

class PythonScript(object):

    def __init__(self, fileName=None):
        '''
        Constructor
        '''
        try:
            if fileName and fileName.endswith(".py") and len(fileName) >= 4:
                try:
                    self.lines = open(fileName, 'r').readlines()
                except (IOError, AttributeError):
                    raise ValueError(invalidFileNameErr)
            else:
                raise ValueError(invalidFileNameErr)
        except AttributeError:
            raise ValueError(invalidFileNameErr)
    
    @classmethod
    def isSingleLineComment(cls, line):
        test = line.lstrip().rstrip()
        if test.startswith('#'):
            return True
        elif test.startswith("'''") and test.endswith("'''") and len(test) > 3:
            return True
        elif test.startswith('"""') and test.endswith('"""') and len(test) > 3:
            return True
        return False
    
    @classmethod
    def isMultiLineCommentOpener(cls, line):
        test = line.lstrip()
        return test.startswith("'''") or test.startswith('"""')
    
    @classmethod
    def isMultiLineCommentCloser(cls, line):
        test = line.rstrip()
        return test.endswith("'''") or test.endswith('"""')
    
    @classmethod
    def extractMethodName(cls, line):
        m = re.search("def ([A-Za-z0-9_]*)\(.*\):", line)
        return m.group(1) if m is not None else None
    
    @classmethod
    def extractClassName(cls, line):
        m = re.search("class ([A-Za-z0-9_]*)(\([A-Za-z0-9_]*\))*:", line)
        return m.group(1) if m is not None else None

    @classmethod
    def getLOCAndMethodCount(cls, strippedLines):
        methodCount = 0
        singleDocStrings = 0
        i = 0
        while i < len(strippedLines):
            if PythonScript.extractMethodName(strippedLines[i]):
                methodCount += 1
                # skip one line docstrings directly after a method
                nextLine = strippedLines[i + 1].lstrip().rstrip()
                if re.match(r"'[^']*'$", nextLine) or re.match(r'"[^"]*"$', nextLine):
                    i += 1
                    singleDocStrings += 1
            i += 1
        return (len(strippedLines) - singleDocStrings, methodCount)
    
    @classmethod
    def getLinesBeneath(cls, lines):
        level = utilities.countLeadingSpace(lines[0])
        result = [lines[0]]
        for line in lines[1:]:
            if utilities.countLeadingSpace(line) > level:
                result.append(line)
            else:
                break
        return result
    
    @classmethod
    def findMultiLineStrings(cls, lines, pattern='"""'):
        def endsWithSlash(line):
            return line.rstrip().lstrip().endswith("\\")
        i = 0
        matches = []
        pattern = re.compile(pattern)
        for i in range(len(lines)):
            currentLine = lines[i].lstrip().rstrip()
            m = pattern.search(currentLine)
            while m:
                matches.append((i, m))
                m = pattern.search(currentLine, m.end())
        
        ranges = []
        i = 0
        while i < len(matches):
            line1, match1 = matches[i]
            i += 1
            
            # make sure we have more matches
            if i == len(matches):
                break
            
            if match1.start() > 0 or (line1 > 0 and endsWithSlash(lines[line1 - 1])):
                line2, match2 = matches[i]
                ranges.append((line1, line2))
            i += 1
        return ranges
        
    def extractDesign(self):
        def isMultiString(i, stringRanges):
            if stringRanges is not None and len(stringRanges) > 0:
                for r in stringRanges:
                    if i >= r[0] and i <= r[1]:
                        return True
            return False
        
        components = []

        lines = self.lines
        stringRanges = PythonScript.findMultiLineStrings(lines, '"""')
        stringRanges += PythonScript.findMultiLineStrings(lines, "'''")
        stripped = []
        i = -1
        while i < len(lines) - 1:
            i += 1
            if isMultiString(i, stringRanges):
                stripped.append(lines[i])
                continue
            elif PythonScript.isSingleLineComment(lines[i]):
                continue
            elif PythonScript.isMultiLineCommentOpener(lines[i]):
                i += 1
                while not PythonScript.isMultiLineCommentCloser(lines[i]):
                    i += 1
                    if i >= len(lines):
                        raise ValueError("Couldn't find end of multiline comment")
            elif utilities.isBlank(lines[i]):
                continue
            else:
                stripped.append(lines[i])
            
        currentLine = 0
        while currentLine < len(stripped):
            className = PythonScript.extractClassName(stripped[currentLine])
            methodName = PythonScript.extractMethodName(stripped[currentLine])
            if utilities.countLeadingSpace(stripped[currentLine]) == 0 and className is not None:
                classLines = PythonScript.getLinesBeneath(stripped[currentLine:])
                currentLine += len(classLines) - 1
                results = PythonScript.getLOCAndMethodCount(classLines)
                components.append(Component(className, results[1], results[0], "OO"))
            elif utilities.countLeadingSpace(stripped[currentLine]) == 0 and methodName is not None:
                methodLines = PythonScript.getLinesBeneath(stripped[currentLine:])
                currentLine += len(methodLines) - 1
                results = PythonScript.getLOCAndMethodCount(methodLines)
                components.append(Component(methodName, 1, results[0], "FD"))
            currentLine += 1
            
        return components
                
                
                
                
            
