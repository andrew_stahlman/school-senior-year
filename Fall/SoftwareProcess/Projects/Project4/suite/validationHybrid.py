'''
    Validation Suite:  Hybrid 
    Baselined:  29 Sep 2009
    Modified:  4 Mar 2011
    @author:  D. Umphress
        Component("HybridClassA",3,7,"OO")
        Component("HybridClassB",0,2,"OO")
        Component("hybridFunctionA",1,2,"FD")
        Component("hybridFunctionB",1,2,"FD")
        Component("hybridFunctionC",1,2,"FD")
'''

import sys
import unittest

class HybridClassA():
    def __init__(self):
        pass

    def methodA(self, parm1=""):
        pass

    def methodB(self, parm1, parm2):
        pass


class HybridClassB():
    pass


def hybridFunctionA(parm1=""):
    pass

def hybridFunctionB():
    pass

def hybridFunctionC(*parms):
    pass

# The following code is outside of a component
if __name__ == "__main__":
    pass
pass
pass