'''
Created on Oct 19, 2012

@author: astahlman
'''

import CA04.prod.utilities as utilities
import re
from CA04.prod.component import Component

invalidFileNameErr = "init : Invalid file name"

class PythonScript(object):
    '''
    classdocs
    '''

    def __init__(self, fileName=None):
        '''
        Constructor
        '''
        try:
            if fileName and fileName.endswith(".py"):
                try:
                    self.lines = open(fileName, 'r').readlines()
                except (IOError, AttributeError):
                    raise ValueError(invalidFileNameErr)
            else:
                raise ValueError(invalidFileNameErr)
        except AttributeError:
            raise ValueError(invalidFileNameErr)
    
    @classmethod
    def isSingleLineComment(cls, line):
        test = line.lstrip().rstrip()
        if test.startswith('#'):
            return True
        elif test.startswith("'''") and test.endswith("'''") and len(test) > 3:
            return True
        elif test.startswith('"""') and test.endswith('"""') and len(test) > 3:
            return True
        return False
    
    @classmethod
    def isMultiLineCommentOpener(cls, line):
        test = line.lstrip()
        return test.startswith("'''") or test.startswith('"""')
    
    @classmethod
    def isMultiLineCommentCloser(cls, line):
        test = line.rstrip()
        return test.endswith("'''") or test.endswith('"""')
    
    
    @classmethod
    def isMultiLineBoundary(cls, line):
        test = line.lstrip().rstrip()
        singleMatches = re.findall("'''", test)
        doubleMatches = re.findall('"""', test)
        return (len(singleMatches) % 2 == 1 or len(doubleMatches) % 2 == 1)
    
    @classmethod
    def extractMethodName(cls, line):
        m = re.search("def ([A-Za-z0-9_]*)\(.*\):", line)
        return m.group(1) if m is not None else None
    
    @classmethod
    def extractClassName(cls, line):
        m = re.search("class ([A-Za-z0-9_]*)(\([A-Za-z0-9_]*\))*:", line)
        return m.group(1) if m is not None else None
    
    @classmethod
    def getLOCAndMethodCount(cls, lines):
        methodCount = 0
        loc = 0
        i = -1
        stringRanges = PythonScript.findMultiLineStrings(lines, '"""')
        stringRanges += PythonScript.findMultiLineStrings(lines, "'''")
        while i < len(lines) - 1:
            i += 1
            
            isString = False
            if stringRanges is not None and len(stringRanges) > 0:
                for r in stringRanges:
                    if i >= r[0] and i <= r[1]:
                        isString = True
                        break
                
            if isString:
                loc += 1
                if PythonScript.extractMethodName(lines[i]):
                    methodCount += 1
                print "Counted: " + lines[i]
                continue
            elif utilities.isBlank(lines[i]):
                continue
            elif PythonScript.isSingleLineComment(lines[i]):
                continue
            elif PythonScript.isMultiLineCommentOpener(lines[i]):
                i += 1
                while not PythonScript.isMultiLineCommentCloser(lines[i]):
                    i += 1
                    if i >= len(lines):
                        raise ValueError("Couldn't find end of multiline comment")
            else:
                if PythonScript.extractMethodName(lines[i]):
                    methodCount += 1
                    # skip one line docstrings directly after a method
                    nextLine = lines[i + 1].lstrip().rstrip()
                    if re.match(r"'[^']*'$", nextLine):
                        i += 1
                    elif re.match(r'"[^"]*"$', nextLine):
                        i += 1         
                loc += 1
                print "Counted: " + lines[i]

        return (loc, methodCount)
    
    
    @classmethod
    def getLinesBeneath(cls, lines):
        level = utilities.countLeadingTabs(lines[0])
        result = [lines[0]]
        for line in lines[1:]:
            if utilities.countLeadingTabs(line) > level:
                result.append(line)
            else:
                break
        return result
    
    @classmethod
    def findMultiLineStrings(cls, lines, pattern='"""'):
        def endsWithSlash(line):
            return line.rstrip().lstrip().endswith("\\")
        i = 0
        matches = []
        pattern = re.compile(pattern)
        for i in range(len(lines)):
            currentLine = lines[i].lstrip().rstrip()
            m = pattern.search(currentLine)
            while m:
                matches.append((i, m))
                print "Found match " + str(i) + "on: " + currentLine
                m = pattern.search(currentLine, m.end())
        
        ranges = []
        i = 0
        while i < len(matches):
            print "i: " + str(i)
            line1, match1 = matches[i]
            i += 1
            if match1.start() > 0 or (line1 > 0 and endsWithSlash(lines[line1 - 1])):
                line2, match2 = matches[i]
                ranges.append((line1, line2))
                print "i = " + str(i) + ", Multistring: " + lines[line1] + " - " + lines[line2]
            i += 1
        return ranges
        
    def extractDesign(self):
        components = []
        currentLine = 0
        lines = self.lines
        while currentLine < len(lines):
            print "Examining this line: " + lines[currentLine]
            className = PythonScript.extractClassName(lines[currentLine])
            methodName = PythonScript.extractMethodName(lines[currentLine])
            if utilities.countLeadingTabs(lines[currentLine]) == 0 and className is not None:
                print "Extracting class: " + className
                classLines = PythonScript.getLinesBeneath(lines[currentLine:])
                currentLine += len(classLines) - 1
                results = PythonScript.getLOCAndMethodCount(classLines)
                components.append(Component(className, results[1], results[0], "OO"))
            elif utilities.countLeadingTabs(lines[currentLine]) == 0 and methodName is not None:
                print "Extracting method: " + methodName
                methodLines = PythonScript.getLinesBeneath(lines[currentLine:])
                currentLine += len(methodLines) - 1
                results = PythonScript.getLOCAndMethodCount(methodLines)
                components.append(Component(methodName, 1, results[0], "FD"))
            currentLine += 1
        return components
                
                
                
                
            
