'''
Created on Sep 20, 2012

@author: Andrew Stahlman
'''
import math
import random

valError1 = "E01: invalid n"
valError2 = "E02: invalid t"
valError3 = "E03: invalid number of tails"
    
class TCurve(object):
    '''
    This class represents a TCurve.
    '''

    @classmethod
    def is_int(cls, n):
        try:
            return n is not None and n % 1 == 0
        except TypeError:
            return False
        
    @classmethod
    def rand_float(cls, numMin, numMax):
        r = random.random()
        numRange = numMax - numMin
        r *= numRange
        return numMin + r
        
    @classmethod
    def calc_const(cls, x):
        top = math.gamma((x + 1) / 2.0)
        bottom = math.gamma(x / 2.0) * math.sqrt(x * math.pi)
        return top / bottom
        
    def __init__(self, n):
        '''
        Constructor takes an integer > 2 and < 50 that represents degrees of freedom
        '''
        if not TCurve.is_int(n):
            raise ValueError(valError1)
        elif n <= 2 or n >= 50:
            raise ValueError(valError1)
        else:
            self.n_deg = n

    def p(self, t, tails=1):
        if t < 0:
            raise ValueError(valError2)
        if not TCurve.is_int(tails) or not (tails == 1 or tails == 2):
            raise ValueError(valError3)
        lower_bound = 0
        upper_bound = t
        tolerance = .0001
        N = 750
        area_old = area_new = 0.0
        run = True
        while (run):
            area_old = area_new
            f = 0
            for i in range(1, N + 1):
                try:
                    u = TCurve.rand_float(lower_bound, upper_bound)
                except TypeError:
                    raise ValueError(valError2)
                base = (1 + (math.pow(u, 2)/self.n()))
                exp = ((self.n() + 1.0) / 2.0) * -1.0
                f += math.pow(base, exp)
            area_new = (t / (N + 1.0)) * f
            run = abs(((area_new - area_old) / area_new)) > tolerance
            N += 5
            
        area_new = area_new * TCurve.calc_const(self.n())
        if tails == 1:
            return area_new + .5
        else:
            return area_new * 2
    
    def n(self):
        return self.n_deg