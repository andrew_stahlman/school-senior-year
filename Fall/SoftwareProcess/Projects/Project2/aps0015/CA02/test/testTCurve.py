'''
Created on Sep 20, 2012

@author: Andrew Stahlman
'''
import unittest
from CA02.prod.TCurve import TCurve
from CA02.prod.TCurve import valError1, valError2, valError3

class TestTCurve(unittest.TestCase):
    
    def test_raises_E01(self, param=None):
        try:
            t = TCurve(param)
            return False
        except ValueError as exc:
            return valError1 == exc.args[0]
            
    def test100_910_BlankConstructor(self):
        self.assertTrue(self.test_raises_E01())
    
    def test100_920_EmptyString(self):
        self.assertTrue(self.test_raises_E01(""))
    
    def test100_930_NonInteger(self):
        self.assertTrue(self.test_raises_E01(1.5))
        
    def test100_940_AtMinBound(self):
        self.assertTrue(self.test_raises_E01(2))
    
    def test100_950_AtMaxBound(self):
        self.assertTrue(self.test_raises_E01(50))
    
    def test100_960_Negative(self):
        self.assertTrue(self.test_raises_E01(-5))
    
    def test100_970_AboveRange(self):
        self.assertTrue(self.test_raises_E01(51))
    
    def test100_010_ValidLow(self):
        t = TCurve(3)
        self.assertIsInstance(t, TCurve)
        self.assertEqual(t.n(), 3)
        
    def test100_020_ValidLow(self):
        t = TCurve(49)
        self.assertIsInstance(t, TCurve)
        self.assertEqual(t.n(), 49)
        
    def test_raises_E02(self, t=None, tails=None):
        try:
            curve = TCurve(3)
            curve.p(t, tails)
            return False
        except ValueError as exc:
            return valError2 == exc.args[0]
    
    def test_raises_E03(self, t=None, tails=None):
        try:
            curve = TCurve(3)
            curve.p(t, tails)
            return False
        except ValueError as exc:
            return valError3 == exc.args[0]
            
    def test200_910_NegativeFloat(self):
        self.assertTrue(self.test_raises_E02(-.1))
        
    def test200_920_TailBelowRange(self):
        self.assertTrue(self.test_raises_E03(.5, 0))
        
    def test200_930_TailAboveRange(self):
        self.assertTrue(self.test_raises_E03(.5, 3))
        
    def test200_940_NoArgs(self):
        self.assertTrue(self.test_raises_E02())
    
    def test200_010_Valid_DefaultTail(self):
        t = TCurve(3)
        p = t.p(.2767)
        self.assertAlmostEqual(p, .6, delta=(.01*.6))
        
    def test200_020_Valid_Tail1(self):
        t = TCurve(3)
        p = t.p(.2767, 1)
        self.assertAlmostEqual(p, .6, delta=(.01*.6))
    
    def test200_030_Valid_Tail2(self):
        t = TCurve(3)
        p = t.p(.2767, 2)
        self.assertAlmostEqual(p, .2, delta=(.01*.2))
        
    def test200_040_Valid2(self):
        t = TCurve(3)
        p = t.p(1.6377, 2)
        self.assertAlmostEqual(p, .8, delta=(.01*.8))
        
if __name__ == "__main__":
    unittest.main()