import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.Scanner;

class ServerTCP {
	public static byte[] doCapitalize(byte[] req) {
		int tml = req[1] + req[0] * 256;
		int s_length = tml - 5;
		byte[] resp = new byte[4 + s_length];
		String resp_s = new String(Arrays.copyOfRange(req, 5, tml));
		resp_s = resp_s.toUpperCase();
		System.out.println(resp_s);
		int resp_tml = 4 + s_length;
		resp[0] = (byte) (resp_tml / 256);
		resp[1] = (byte) (resp_tml - (resp[0] * 256));
		resp[2] = req[2];
		resp[3] = req[3];
		byte[] s_bytes = resp_s.getBytes();
		System.arraycopy(s_bytes, 0, resp, 4, s_bytes.length);
		return resp;
	}

	public static byte[] doCountLength(byte[] req) {
		byte[] resp = new byte[6];
		int tml = req[1] + req[0] * 256;
		int l = tml - 5;
		resp[0] = 0;
		resp[1] = 6;
		resp[2] = req[2];
		resp[3] = req[3];
		resp[4] = (byte) (l / 256);
		resp[5] = (byte) (l - (resp[4] * 256));
		return resp;
	}

	public static void main(String argv[]) throws Exception {

		int port;
		Scanner readPort = new Scanner(System.in);
		System.out.print("please input port to open server on: ");
		port = readPort.nextInt();
		try {
			ServerSocket welcomeSocket = new ServerSocket(port);
			while (true) {
				System.out.println("Listening on " + port);
				Socket connectionSocket = welcomeSocket.accept();
				System.out.println("Accepted connection");
				InputStream iStream = connectionSocket.getInputStream();
				byte[] buff = new byte[200];
				int numbytes = iStream.read(buff);
				System.out.println("Here are the bytes:");
				for (int i = 0; i < numbytes; i++) {
					System.out.print(String.format("%02X", buff[i]));
					if (i != (numbytes - 1)) {
						System.out.print(", ");
					} else {
						System.out.println();
					}
				}
				byte[] resp = null;
				DataOutputStream outToClient = new DataOutputStream(
						connectionSocket.getOutputStream());
				if ((buff[4] & 0xFF) == 170) {
					resp = doCapitalize(buff);
					outToClient.write(resp);
				} else if (buff[4] == 85) {
					resp = doCountLength(buff);
					outToClient.write(resp);
				} else {
					String error = "recieved an invalid operation, must be either 170 or 85.";
					System.out.print("\n" + error + "\n");
					outToClient.write(error.getBytes());
				}

			}
		} catch (IOException e) {
			System.out.println("Could not listen on port " + port);
			System.exit(-1);

		}
	}
}
