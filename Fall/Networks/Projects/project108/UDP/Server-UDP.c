#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "stringops.h"

void getSocket(char* port, int *socket_fd);

#define BUFF_SIZE 100

int main(int argc, char* argv[]) {
	if (argc != 3) {
		puts("Usage: server portnumber");
		exit(1);
	} else {
		char* port = argv[2];
		int socket_fd;
	   	getSocket(port, &socket_fd);
		char buffer[BUFF_SIZE]; 
		struct sockaddr_storage remote_addr;
		socklen_t addr_len = sizeof remote_addr;
		int bytes_rcvd, bytes_sent;
		while (1) {
			if ((bytes_rcvd = recvfrom(socket_fd, buffer, BUFF_SIZE - 1 , 0,
				(struct sockaddr *)&remote_addr, &addr_len)) == -1) {
				puts("Error in receiving datagram. Exiting...");
				perror("recvfrom");
				exit(2);
			} else {
				printf("Received this data: %s\n", &buffer[0]);
				print_msg(&buffer[0], bytes_rcvd);
				struct request_t req;
				build_request(&buffer[0], &req);
				struct response_t resp;
			   	handle_request(&req, &resp);
				if (resp.has_err) {
					puts("Received an invalid request");
					printf("Error: %s\n", resp.err);
					continue;
				}
				if ((bytes_sent = sendto(socket_fd, resp.result, resp.len, 0,
					(struct sockaddr *)&remote_addr, addr_len)) != resp.len) {
					puts("Error in sending datagram. Exiting...");
					perror("sendto");
					exit(3);
				} else {
					puts("Sending this response:");
					print_msg(resp.result, resp.len);
				}
			}
		}
	}

	return 0;
}

void getSocket(char* port, int *socket_fd) {
	struct addrinfo hints, *server_info; 
	memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM; // use an unconnected datagram socket
    hints.ai_flags = AI_PASSIVE; // use my IP

	if (getaddrinfo(NULL, port, &hints, &server_info) != 0) {
		puts("Error getting the server's address info. Exiting...");
		exit(3);
	} 
	
	struct addrinfo *addr_p;
	int tryCount;
	for (addr_p = server_info; addr_p != NULL; addr_p = addr_p->ai_next) {
		tryCount++;
		*socket_fd = socket(addr_p->ai_family, addr_p->ai_socktype, addr_p->ai_protocol);
		if (*socket_fd == -1) {
			continue;
		}

		// we have a socket file descriptor, now bind it
		if (bind(*socket_fd, addr_p->ai_addr, addr_p->ai_addrlen) == -1) {
			close(*socket_fd);
			continue;
		}

		// we have a bound socket
		break;
	}

	if (addr_p == NULL) {
			puts("Couldn't bind a socket to any address. Exiting...");
			exit(4);
	}

	printf("Listening on port %s...\n", port);
}
