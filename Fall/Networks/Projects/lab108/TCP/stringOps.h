#include <stdint.h>

#define TO_UPPER 32
#define MIN_MSG_LEN 5
#define TRUE 1
#define FALSE 0
#define GET_LEN 85
#define CAPS 170

typedef short BOOL;
typedef uint8_t uint8;
typedef uint16_t uint16;

BOOL handle_response(char* msg, char* operation);
BOOL build_message(char* msg, char* operation, char *buffer);
void print_msg(unsigned char *msg, uint16 length);
