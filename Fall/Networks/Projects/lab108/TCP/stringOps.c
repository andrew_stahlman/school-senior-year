#include "stringOps.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAXBUFLEN 128
#define HEADER_LEN 5 // bytes in the message header
#define MAX_RESP_LEN 250
#define is_bigendian() ( (*(char*)&test_val) == 0 )

BOOL handle_response(char* msg, char* operation) {
	int length;

	if (strcmp(operation, "85") == 0) {
		int r_len = 0;
		r_len = ((unsigned char) msg[4]) * 256;
		r_len += (unsigned char) msg[5];
		printf("The Length of the Message is: %d\n", r_len);
	} else if (strcmp(operation, "170") == 0) {
		printf("Received Response From the Server is: %s\n", &msg[4]);
	} else {
		return FALSE;
	}
	return TRUE;
}

BOOL build_message(char* msg, char* operation, char* buffer) {
	int i, op;
	short length;
	unsigned char buf[MAXBUFLEN];

	op = atoi(operation);
	length = strlen(msg) + HEADER_LEN;
	
	if (length > 128) {
		printf("Message must be less than 128 characters\n");
		exit(-1);
	} else {
		buf[0] = (unsigned char) (length >> 8);
		buf[1] = (unsigned char) (length & 0xff);
		buf[2] = (unsigned char) (1 >> 8);
		buf[3] = (unsigned char) (1);
		buf[4] = (unsigned char) op;
		for (i = 0; i < length; i++) {
			buf[i + 5] = msg[i];
		}
		memcpy(buffer, buf, length + 5);
		printf("The Bytes Sent are: \n");
		print_msg(buffer, length + 5);
	}
	return TRUE;
}

void print_msg(unsigned char *msg, uint16 length) {
	uint16 i;
	printf("[");
	for (i = 0; i < length; i++) {
		if (i > 0)
			printf(", ");
		printf("%02X", msg[i]);
	}
	printf("]\n");
}