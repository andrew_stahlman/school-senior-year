#! /usr/bin/env python
import socket
import struct
from time import time

PRE_STR_LEN  = 5
COUNT = 85
UPPER = 170
BUFFER_SZ = 200

op_queue = []

def pack_message(op, string, r_id):
	tml = PRE_STR_LEN + len(string)
	buff = bytearray(PRE_STR_LEN)
	s_two = struct.Struct('!h') # 2 bytes, big-endian
	s_two.pack_into(buff, 0, tml)
	s_two.pack_into(buff, 2, r_id)
	s_one = struct.Struct('B') # one byte, 0 to 255
	s_one.pack_into(buff, 4, op)
	string_bytes = bytearray(string)
	buff.extend(string_bytes)
	return buff

def unpack_message(buff):
	response = {}
	s_two = struct.Struct('!h')
	response['tml'] = s_two.unpack_from(buff)[0]
	response['r_id'] = s_two.unpack_from(buff, 2)[0]
	retrieved_id, retrieved_op, start = req_for_id(r_id)
	if retrieved_op == UPPER:
		response['val'] = buff[4:response['tml']]
	elif retrieved_op == COUNT:
		response['val'] = s_two.unpack_from(buff, 4)[0]
	else:
		raise ValueError("Received an unexpected response.")
	response['time'] = time() - start
	return response


def req_for_id(req_id):
	for entry in op_queue:
		req, op, start = entry
		if req == req_id:
			e = entry
			return e

USAGE = "Usage: client hostname port operation string\n"
alive = True
r_id = 0
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while (alive):
	cmd = raw_input("Enter a command: ")
	if cmd == 'quit' or cmd == 'exit':
		alive = False
	else:
		args = cmd.split(' ')
		if len(args) < 5 and args[0] != 'client':
			print(USAGE)
		else:
			try:
				server_name = args[1]
				port = int(args[2])
				op = int(args[3])
				string = ' '.join(args[4:])
			except:
				print(USAGE)
				continue
			
			msg = pack_message(op, string, r_id)
			r_id += 1
			op_queue.append( (r_id, op, time()) )
			s.sendto(msg, (server_name, port))

			bytes_rcvd = s.recvfrom(200)[0]
			b = buffer(bytes_rcvd)
			resp = unpack_message(buffer(b))
			print "Request %d response: %s" % (resp['r_id'], resp['val'])
			print "Request %d time: %F" % (resp['r_id'], resp['time'])
