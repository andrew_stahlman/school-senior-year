#! /usr/bin/env python
import socket
import struct
import re
import sys
from time import time
from nim import NimGame

PRE_STR_LEN  = 5
COUNT = 85
UPPER = 170
BUFFER_SZ = 200
GID = 8
MAGIC_NUM = 4660

op_queue = []

def sendMove(conn, row, numRemoved):
	moveLen = 5 # 2 + 1 + 1 + 1
	buff = bytearray(moveLen) 
	s_one = struct.Struct('B') # one byte, 0 to 255
	s_two = struct.Struct('!h') # 2 bytes, big-endian
	s_two.pack_into(buff, 0, MAGIC_NUM)  # magic number
	s_one.pack_into(buff, 2, GID)
	s_one.pack_into(buff, 3, row)
	s_one.pack_into(buff, 4, numRemoved)
	if (conn.send(buff) != moveLen):
		print "Error sending the move! Exiting..."
		conn.close()
		exit(1)

def rejectMove(conn):
	moveLen = 5 # 2 + 1 + 1 + 1
	buff = bytearray(moveLen) 
	s_one = struct.Struct('B') # one byte, 0 to 255
	s_two = struct.Struct('!h') # 2 bytes, big-endian
	s_two.pack_into(buff, 0, MAGIC_NUM)  # magic number
	s_one.pack_into(buff, 2, GID)
	s_one.pack_into(buff, 3, 0xFF)
	s_one.pack_into(buff, 4, 0xFF)
	if (conn.send(buff) != moveLen):
		print "Error sending the rejection! Exiting..."
		conn.close()
		exit(1)

def parseMove(buff):
	move = {}
	s_two = struct.Struct('!h')
	s_one = struct.Struct('B')
	magic = s_two.unpack_from(buff, 0)[0]
	g_id = s_one.unpack_from(buff, 2)[0]
	row = s_one.unpack_from(buff, 3)[0]
	numRemoved = s_one.unpack_from(buff, 4)[0]

	move['rejected'] = (row == 0xFF and numRemoved == 0xFF)
	move['row'] = row
	move['numRemoved'] = numRemoved
	return move
	
def doNextMove(conn, game):
	while 1: # get the move
		try:
			myMove = raw_input("Enter next move (row #, # to remove): ")
			row, numRemoved = map(int, re.split(",\s*", myMove))
			sendMove(conn, row, numRemoved)
			return row, numRemoved
		except:
			print("Invalid input. Example format: 3,5")

def play(conn, game, lastRow=None, lastNumRemoved=None):
	while not game.isGameOver(): # receive moves from opponent
		print("Waiting for opponent...")
		moveData = conn.recv(1024)
		if not moveData:
			print "Game Over. You Win.\n"
			break
		oppMove = parseMove(moveData)
		if oppMove['rejected']:
			print "Your move was rejected! Try again."
			lastRow, lastNumRemoved = doNextMove(conn, game)
		else:
			if lastRow is not None and lastNumRemoved is not None:
				if not game.updateForMove(lastRow, lastNumRemoved):
					print "Invalidated our own move. Something is very wrong. Exiting..."
					exit(1)
				else:
					print ("You removed " + str(lastNumRemoved) + " from row " + str(lastRow))
					print(str(game))
					lastRow = None
					lastNumRemoved = None

			if game.updateForMove(oppMove['row'], oppMove['numRemoved']):
				print("Opponent took " + str(oppMove['numRemoved']) + " from row " + str(oppMove['row']))
				print(str(game))
				if game.isGameOver():
					print "Game Over. You lose.\n"
					conn.close()
					continue
				lastRow, lastNumRemoved = doNextMove(conn, game)
			else:
				print("Received invalid move! Rejecting...")
				rejectMove(conn)

def startAsServer(port):
	alive = True
	while (alive):
		try:
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.bind(('', port))
		except socket.error, msg:
			sys.stderr.write("[ERROR] %s\n" % msg[1])
			sys.exit(1)
		sock.listen(1)
		print "Listening for a connection on port " + str(port)
		conn, addr = sock.accept()
		break

	game = NimGame()
	print str(game)
	play(conn, game)

def startAsClient(ip, port):
	conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print "Client attempting to connect to " + ip + " on port " + str(port)
	conn.connect((ip, port))
	game = NimGame()
	print str(game)
	clientRow, clientNumRemoved = doNextMove(conn, game)
	play(conn, game, clientRow, clientNumRemoved)


def getMsg(errCode):
	messages = ["Magic number", "Incorrect length", "Port out of range"]
	base = "Error: "
	result = ""
	for i in range(len(messages)):
		if isBitSet(errCode, i):
			result += base + messages[i] + ". \n"
	return result if len(result) > 0 else None

def isBitSet(byte, bit):
	if sys.byteorder == "big":
		return 1 & (byte << bit)
	else:
		return 1 & (byte >> bit)

def pack_message(myPort):
	buff = bytearray(PRE_STR_LEN)
	s_one = struct.Struct('B') # one byte, 0 to 255
	s_two = struct.Struct('!h') # 2 bytes, big-endian
	s_two.pack_into(buff, 0, 4660) 
	#s_two.pack_into(buff, 0, 2222) #To test invalid magic number
	s_one.pack_into(buff, 2, 8)
	s_two.pack_into(buff, 3, myPort)
	#s_one.pack_into(buff, 5, 12)	#To test for incorrect length
	
	return buff

def unpack_message(buff):
	response = {}
	s_two = struct.Struct('!h')
	s_one = struct.Struct('B')
	response['magic'] = s_two.unpack_from(buff, 0)[0]
	response['g_id'] = s_one.unpack_from(buff, 2)[0]
	if len(buff) == 9:		#For IPv4 case
		response['ip1'] = s_one.unpack_from(buff, 3)[0]
		response['ip2'] = s_one.unpack_from(buff, 4)[0]
		response['ip3'] = s_one.unpack_from(buff, 5)[0]
		response['ip4'] = s_one.unpack_from(buff, 6)[0]
		response['port'] = s_two.unpack_from(buff, 7)[0]
	else:					#For Error and Response
		response['Ph'] = s_one.unpack_from(buff, 3)[0] 
		if response['Ph'] == 0:
			response['Pl'] = s_one.unpack_from(buff, 4)[0]
			response['error'] = getMsg(response['Pl'])
		else:	
			response['port'] = s_two.unpack_from(buff, 3)[0]
	return response

USAGE = "Usage: ServerName ServerPort MyPort\n"
alive = True
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while (alive):
	cmd = raw_input("Enter a command (or press Enter for usage): ")
	if cmd == 'quit' or cmd == 'exit':
		alive = False
	else:
		args = cmd.split(' ')
		if len(args) < 3:
			print(USAGE)
		else:
			try:
				server_name = args[0]
				port = int(args[1])
				myPort = int(args[2])
			except:
				print(USAGE)
				continue
			
			msg = pack_message(myPort)
			s.sendto(msg, (server_name, port))

			bytes_rcvd = s.recvfrom(200)[0]
			b = buffer(bytes_rcvd)
			resp = unpack_message(buffer(b))
			if 'error' in resp:
				print resp['error']
			elif len(b) == 5:
				print "Magic Number: %d " % (resp['magic'])
				print "Port Number: %d " % (resp['port'])
				startAsServer(resp['port'])
			else:
				print "Magic Number: %d " % (resp['magic'])
				print "\nIP Address: %d.%d.%d.%d " % (resp['ip1'], resp['ip2'], resp['ip3'], resp['ip4'])
				print "\nPort Number: %d " % (resp['port'])
				ip = "%d.%d.%d.%d" % (resp['ip1'], resp['ip2'], resp['ip3'], resp['ip4'])
				port = resp['port']
				startAsClient(ip, port)
