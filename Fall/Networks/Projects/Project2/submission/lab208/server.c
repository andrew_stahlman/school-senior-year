#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <assert.h>
#include <math.h>

void getSocket(char* port, int *socket_fd);
void handle_request(char buffer[], int buff_len, struct sockaddr_storage *client_addr, short *resp_len, char** response);
void build_response(struct sockaddr_storage* client_addr, uint16_t port_in, short *resp_len, char *response);
void print_msg(unsigned char *msg, uint16_t length);
void test_handle_request();
void uint16_to_bytes(uint16_t n, char *b);

const int test_val = 1;
#define is_bigendian() ( (*(char*)&test_val) == 0 )

#define BUFF_SIZE 100
#define GID 8 
#define BASE_PORT 10010
#define MAGIC_MSB 18
#define MAGIC_LSB 52
#define NONE 0 

#define WAIT 0
#define CONNECT 1

struct sockaddr_storage* waiting_addr;
uint16_t waiting_port = NONE;

int main(int argc, char* argv[]) {
	waiting_addr = (struct sockaddr_storage*)malloc(sizeof(struct sockaddr_storage));
	//test_handle_request();

	if (argc != 2) {
		puts("Usage: portnumber");
		exit(1);
	} else {
		char* port = argv[1];
		int socket_fd;
	   	getSocket(port, &socket_fd);
		char buffer[BUFF_SIZE]; 
		struct sockaddr_storage remote_addr;
		socklen_t addr_len = sizeof remote_addr;
		int bytes_rcvd, bytes_sent;
		while (1) {
			if ((bytes_rcvd = recvfrom(socket_fd, buffer, BUFF_SIZE - 1 , 0,
				(struct sockaddr *)&remote_addr, &addr_len)) == -1) {
				puts("Error in receiving datagram. Exiting...");
				perror("recvfrom");
				exit(2);
			} else {
				printf("Received this data: %s\n", buffer);
				print_msg(buffer, bytes_rcvd);

				short resp_len;
				char* response;
			   	handle_request(buffer, bytes_rcvd, &remote_addr, &resp_len, &response);
				if ((bytes_sent = sendto(socket_fd, response, resp_len, 0,
					(struct sockaddr *)&remote_addr, addr_len)) != resp_len) {
					puts("Error in sending datagram. Exiting...");
					perror("sendto");
					exit(3);
				} else {
					puts("Sending this response:");
					print_msg(response, resp_len);
				}
			}
		}
	}

	return 0;
}

void getSocket(char* port, int *socket_fd) {
	struct addrinfo hints, *server_info; 
	memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM; // use an unconnected datagram socket
    hints.ai_flags = AI_PASSIVE; // use my IP

	if (getaddrinfo(NULL, port, &hints, &server_info) != 0) {
		puts("Error getting the server's address info. Exiting...");
		exit(3);
	} 
	struct addrinfo *addr_p;
	int tryCount;
	for (addr_p = server_info; addr_p != NULL; addr_p = addr_p->ai_next) {
		tryCount++;
		*socket_fd = socket(addr_p->ai_family, addr_p->ai_socktype, addr_p->ai_protocol);
		if (*socket_fd == -1) {
			continue;
		}

		// we have a socket file descriptor, now bind it
		if (bind(*socket_fd, addr_p->ai_addr, addr_p->ai_addrlen) == -1) {
			close(*socket_fd);
			continue;
		}

		// we have a bound socket
		break;
	}

	if (addr_p == NULL) {
			puts("Couldn't bind a socket to any address. Exiting...");
			exit(4);
	}

	printf("Listening on port %s...\n", port);
}

void handle_request(char buffer[], int buff_len, struct sockaddr_storage *client_addr, short *resp_len, char** response) {
	unsigned char err_code = 0;
	*response = (char*)malloc(21); // 21 == max(IPv6, IPv4)

	if (buffer[0] != MAGIC_MSB || buffer[1] != MAGIC_LSB) {
		err_code |= 1; // set bit 0

	}
	if (buff_len != 5) {
		err_code |= (1 << 1); // set bit 1
	}
	uint16_t port_in;
	uint8_t gid = (uint8_t)buffer[2];
	port_in = (uint8_t)(buffer[3]) << 8;
	port_in += (uint8_t)buffer[4];

	if (port_in < BASE_PORT + (5*gid) || port_in > BASE_PORT + (5*gid) + 4) {
		err_code |= (1 << 2); // set bit 2
	}

	if (err_code != 0) {
		(*response)[0] = MAGIC_MSB;
		(*response)[1] = MAGIC_LSB;
		(*response)[2] = GID;
		(*response)[3] = 0;
		(*response)[4] = err_code;
		*resp_len = 5;
	} else {
		build_response(client_addr, port_in, resp_len, *response);
	}
	print_msg(*response, *resp_len);
}

void build_response(struct sockaddr_storage* client_addr, uint16_t port_in, short *resp_len, char *response) {
	response[0] = MAGIC_MSB;
	response[1] = MAGIC_LSB;
	response[2] = GID;
	if (waiting_port == NONE) {
		waiting_port = port_in;
		memcpy(waiting_addr, client_addr, sizeof(struct sockaddr_storage));
		response[3] = port_in / (1 << 8);
		response[4] = port_in % (1 << 8);
		*resp_len = 5;
	} else {
		void *ip;
		size_t ip_len;
		//if (client_addr->ss_family == AF_INET6) {
		if (waiting_addr->ss_family == AF_INET6) {
			struct sockaddr_in6* waiting_ip6 = (struct sockaddr_in6*)waiting_addr;
			ip_len = 16;
			ip = &(waiting_ip6->sin6_addr);
			//memcpy(&response[3], (void*)((client_ip6->sin6_addr).s6_addr), ip_len); // IPv6 addr is 16 bytes
		} else {
			struct sockaddr_in* waiting_ip4 = (struct sockaddr_in*)waiting_addr;
			ip_len = 4;
			ip = &(waiting_ip4->sin_addr);
			//memcpy(&response[3], (void*)((client_ip4->sin_addr).s_addr), ip_len); // IPv4 addr is 4 bytes
		}
		memcpy(&response[3], ip, ip_len);

		uint16_t net_port = htons(waiting_port);
		char *port_ptr = (char*)&net_port;
		memset(&response[3 + ip_len], port_ptr[0], 1);
		memset(&response[3 + ip_len + 1], port_ptr[1], 1);

		*resp_len = 5 + ip_len;
		memset(waiting_addr, 0, sizeof(struct sockaddr_storage));
		waiting_port = NONE;
	}
}

void print_msg(unsigned char *msg, uint16_t length) {
	uint16_t i;
	for (i = 0; i < length; i++)
	{
		if (i > 0) printf(", ");
		printf("%02X", msg[i]);
	}
	printf("\n");
}

void test_handle_request() {
	puts("in test");
	uint16_t port = BASE_PORT + (GID * 5);
	char s_port[50];
	struct addrinfo hints, *server_info; 
	memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM; // use an unconnected datagram socket
    hints.ai_flags = AI_PASSIVE; // use my IP

	puts("about to get addrinfo");
	sprintf(s_port, "%d", port);
	printf("s_port: %s\n", s_port);
	if (getaddrinfo(NULL, s_port, &hints, &server_info) != 0) {
		puts("Error getting the server's address info. Exiting...");
		exit(3);
	}

	puts("got addrinfo");
	port++; // port + 1 is the port we are willing to play on

	struct sockaddr *remote_addr = (struct sockaddr*)(server_info->ai_addr);

	char ip1[INET6_ADDRSTRLEN];
	if (server_info->ai_family == AF_INET) {
		inet_ntop(AF_INET, &(((struct sockaddr_in*)remote_addr)->sin_addr), ip1, INET_ADDRSTRLEN);
	} else {
		inet_ntop(AF_INET6, &(((struct sockaddr_in6*)remote_addr)->sin6_addr), ip1, INET6_ADDRSTRLEN);
	}

	char request[50];
	request[0] = MAGIC_MSB;
	request[1] = MAGIC_LSB;
	request[2] = GID;
	request[3] = port / (1 << 8);
	request[4] = port % (1 << 8);

	short resp_len;
	puts("about to handle_request");
	char* response;
	handle_request(request, 5, (struct sockaddr_storage*)remote_addr, &resp_len, &response);
	puts("handled request");
	assert(response[0] == MAGIC_MSB);
	assert(response[1] == MAGIC_LSB);
	assert(response[2] == GID);
	assert(response[3] == port / (1 << 8));
	assert(response[4] == port % (1 << 8));
	print_msg(response, 5);

	port++;
	request[3] = port / (1 << 8);
	request[4] = port % (1 << 8);
	handle_request(request, 5, (struct sockaddr_storage*)remote_addr, &resp_len, &response);

	puts("handled request 2");
	char ip2[INET6_ADDRSTRLEN];
	if (server_info->ai_family == AF_INET) {
		puts("testing ip4");
		inet_ntop(AF_INET, &(response[3]), ip2, INET_ADDRSTRLEN);
		puts("tested ip4");
	} else {
		puts("testing ip6");
		inet_ntop(AF_INET6, &(response[3]), ip2, INET6_ADDRSTRLEN);
	}
	print_msg(response, resp_len);
	printf("%s\n", ip1);
	printf("%s\n", ip2);
	assert(strcmp(ip1, ip2) == 0);
}

void uint16_to_bytes(uint16_t n, char *b) {
	if (!is_bigendian()) {
		b[0] = n / pow(2,8);
		b[1] = n - (b[0] * pow(2,8));
	} else {
		b[1] = n / pow(2,8);
		b[0] = n - (b[1] * pow(2,8));
	}
}
