#! /usr/bin/env python
import socket
import struct
import sys
from time import time

PRE_STR_LEN  = 5
COUNT = 85
UPPER = 170
BUFFER_SZ = 200

op_queue = []

def getMsg(errCode):
	messages = ["Magic number", "Incorrect length", "Port out of range"]
	base = "Error: "
	result = ""
	for i in range(len(messages)):
		if isBitSet(errCode, i):
			result += base + messages[i] + ". \n"
	return result if len(result) > 0 else None

def isBitSet(byte, bit):
	if sys.byteorder == "big":
		return 1 & (byte << bit)
	else:
		return 1 & (byte >> bit)

def pack_message(myPort):
	buff = bytearray(PRE_STR_LEN)
	s_one = struct.Struct('B') # one byte, 0 to 255
	s_two = struct.Struct('!h') # 2 bytes, big-endian
	s_two.pack_into(buff, 0, 4660) 
	#s_two.pack_into(buff, 0, 2222) #To test invalid magic number
	s_one.pack_into(buff, 2, 8)
	s_two.pack_into(buff, 3, myPort)
	#s_one.pack_into(buff, 5, 12)	#To test for incorrect length
	
	return buff

def unpack_message(buff):
	response = {}
	s_two = struct.Struct('!h')
	s_one = struct.Struct('B')
	response['magic'] = s_two.unpack_from(buff, 0)[0]
	response['g_id'] = s_one.unpack_from(buff, 2)[0]
	if len(buff) == 9:		#For IPv4 case
		response['ip1'] = s_one.unpack_from(buff, 3)[0]
		response['ip2'] = s_one.unpack_from(buff, 4)[0]
		response['ip3'] = s_one.unpack_from(buff, 5)[0]
		response['ip4'] = s_one.unpack_from(buff, 6)[0]
		response['port'] = s_two.unpack_from(buff, 7)[0]
	else:					#For Error and Response
		response['Ph'] = s_one.unpack_from(buff, 3)[0] 
		if response['Ph'] == 0:
			response['Pl'] = s_one.unpack_from(buff, 4)[0]
			response['error'] = getMsg(response['Pl'])
		else:	
			response['port'] = s_two.unpack_from(buff, 3)[0]
	return response

USAGE = "Usage: ServerName ServerPort MyPort\n"
alive = True
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while (alive):
	cmd = raw_input("Enter a command: ")
	if cmd == 'quit' or cmd == 'exit':
		alive = False
	else:
		args = cmd.split(' ')
		if len(args) < 3:
			print(USAGE)
		else:
			try:
				server_name = args[0]
				port = int(args[1])
				myPort = int(args[2])
			except:
				print(USAGE)
				continue
			
			msg = pack_message(myPort)
			s.sendto(msg, (server_name, port))

			bytes_rcvd = s.recvfrom(200)[0]
			b = buffer(bytes_rcvd)
			resp = unpack_message(buffer(b))
			if 'error' in resp:
				print resp['error']
			elif len(b) == 5:
				print "Magic Number: %d " % (resp['magic'])
				print "Port Number: %d " % (resp['port'])
			else:
				print "Magic Number: %d " % (resp['magic'])
				print "\nIP Address: %d.%d.%d.%d " % (resp['ip1'], resp['ip2'], resp['ip3'], resp['ip4'])
				print "\nPort Number: %d " % (resp['port'])



