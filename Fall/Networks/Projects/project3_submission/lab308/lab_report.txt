Group 08:
Andrew Stahlman - aps0015@auburn.edu
Keith Graves - klg0006@auburn.edu
Matt Campbell - msc0003@auburn.edu

Our code has been tested on the engineering network tux machines and works as described in the Lab3 instructions.

To compile and run the code, use the following instructions:

1. In the terminal, use the command 'make' to build the server executable (Server.exe)
2. Run server.exe passing the server's port as a command line parameter. Example: ./Server.exe 10018
3. Run the python client with no command line arguments. Example: python Client.py
4. The client will prompt, "Enter a command:"
5. Enter commands in the following format: ServerName ServerPort ClientPort. Example: tux177.eng.auburn.edu 10018 10050
6. The first Client will receive the response from the server with the magic number
a. If a request is invalid, the server will respond with a message containing an error byte with the error encoded as specified in the lab instructions. The client examines this byte and displays the error to the user. 
7. This Client will now set up a TCP Server and listens on the port advertised to the server
8. Repeat steps 3 - 5 on a different machine.
9. The second Client will receive as a response from the server, the magic number and the IP address and port number of the first client
10. The second Client will now connect to the TCP Server set up by the first Client
11. The NIM board display will print on the second Client, and prompt for a move
12. The second Client will send a move with the format: “row #”, ”number to remove”   	example: 1,1
13. The first Client will receive the move and verify its validity.  
a. If the move is invalid, it will send to the opponent that his move was invalid.  
b. If the move was valid, the first Client will print the updated board and prompt for a new move.
14. Repeat steps 12 and 13 until the game has finished.
15. The game ends when there are no more tokens left on the board.  The player who takes the final token is the winner.
16. The winning player will be notified that he has won, and the losing player will be notified that he has lost
17. Both Clients close the TCP connection and revert to the state in step 4.

Our client is not compatible with IPv6 addresses. However, Dr. Biaz confirmed that our program need only be functional on the tux machines, therefore all requirements have been met.
