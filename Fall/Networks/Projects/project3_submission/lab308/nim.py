class NimGame(object):

	def __init__(self):
		self.rows = [1, 3, 5, 7] 

	def updateForMove(self, row, numRemove):
		if numRemove <= 0:
			return False
		if not (row >= 1 and row <= len(self.rows)):
			return False
		elif (self.rows[row - 1] < numRemove):
			return False
		else:
			self.rows[row - 1] -= numRemove
		return True
	
	def isGameOver(self):
		for row in self.rows:
			if row > 0:
				return False
		return True

	def __str__(self):
		s = "Row#	Tokens\n"
		for i in range(len(self.rows)):
			s += str(i+1) + "	" + str(self.rows[i]) + "\n"
		s += "\n"
		return s
