/*
** client.c -- a stream socket client demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "stringOps.h"

#include <arpa/inet.h>

#define MAXDATASIZE 128 // max number of bytes we can get at once 
#define HEADER_LEN 5

#define COUNT = 85
#define UPPER = 170

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char *argv[])
{
    int sockfd, numbytes;  
    char buffer[MAXDATASIZE];
    struct addrinfo hints, *servinfo, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];
	
    if (argc != 5) {
        fprintf(stderr,"usage: servername PortNumber Operation String\n");
		exit(1);
    }
	
	char* port = argv[2];
	char* op = argv[3];
	char* message = argv[4];
	int length = strlen(message) + HEADER_LEN;
	printf("length is %d\n", length);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((rv = getaddrinfo(argv[1], argv[2], &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }

        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            continue;
        }

        break;
    }

    if (p == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        return 2;
    }

    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
            s, sizeof s);
    printf("client: connecting to %s\n", s);

    freeaddrinfo(servinfo); // all done with this structure
	//sleep(5);
	printf("Sending this data %s\n", message);
	build_message(message, op, buffer);
	if (send(sockfd, buffer, length, 0) == -1) {
		perror("send");
		exit(1);
	}
    if ((numbytes = recv(sockfd, buffer, MAXDATASIZE-1, 0)) <= 0) {
        perror("recv");
		printf("failed to receive");
        exit(1);
    } else {
		buffer[numbytes] = '\0';
		printf("client: received '%s'\n",buffer);
		print_msg(&buffer[0], numbytes);
		struct request_t req;
		//build_message(&buffer[0], &req);
	}
    close(sockfd);

    return 0;
}
