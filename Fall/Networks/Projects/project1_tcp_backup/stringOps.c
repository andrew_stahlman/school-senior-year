#include "stringOps.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAXBUFLEN 128
#define HEADER_LEN 5 // bytes in the message header
#define MAX_RESP_LEN 250
const int test_val = 1;
#define is_bigendian() ( (*(char*)&test_val) == 0 )

void uint16_to_bytes(uint16 n, char *b);
uint16 bytes_to_uint16(unsigned char* b);

uint16 count_length(char input[]) {
	uint16 i = 0;
	uint16 count = 0;
	while (input[i] != '\0') {
		count++;
		i++;
	}
	return count;
}

char* to_upper(char input[], uint16 length) {
	char *result;
	result = (char*) malloc(sizeof(char) * length);
	uint16 i;
	for (i = 0; i < length; i++) {
		if (input[i] >= 'a' && input[i] <= 'z') {
			result[i] = input[i] - TO_UPPER;
		} else {
			result[i] = input[i];
		}
	}
	result[length] = '\0';
	return result;
}

BOOL handle_response(struct request_t *req, struct response_t *resp) {
	resp->has_err = TRUE;
	//memset(resp->result, 0, MAX_RESP_LEN);
	char *resp_val;
	uint16 resp_val_len;
	uint16 string_len = count_length(req->input);
	printf("string_len = %d\n", string_len);
	if (req->op == CAPS) {
		puts("About to convert to upper");
		resp->string = to_upper(req->input, string_len);
		print_msg(resp->string, string_len);
		resp->len = string_len + HEADER_LEN; //4 bytes for TML and req_id
		printf("Counted length. resp->len = %d\n", resp->len);
		resp_val = resp->string;
		resp_val_len = string_len;
	} else {
		puts("Requested length");
		resp->len = 6;
		resp_val = (char*)malloc(sizeof(uint16));
		uint16_to_bytes(string_len, resp_val);
		//memcpy(&resp_val[0], &string_len, sizeof(uint16));
		//resp_val[0] = (char) (string_len >> 8);
		//resp_val[1] = (char) string_len;
		resp_val_len = 2;
	}

	puts("about to malloc");
	resp->result = (char*)malloc(sizeof(char) * resp->len);
	uint16_to_bytes(resp->len, resp->result);
	//resp->result[0] = (char) (resp->len >> 8);
	//resp->result[1] = (char) resp->len;
	//memcpy(&(resp->result), &(resp->len), sizeof(uint16));
	uint16_to_bytes(req->req_id, &(resp->result[2]));
	//resp->result[2] = (char) (req->req_id >> 8);
	//resp->result[3] = (char) req->req_id;
	//memcpy(&(resp->result[2]), &(req->req_id), sizeof(uint16));
	puts("memcpy");
	memcpy(&(resp->result[4]), resp_val, resp_val_len);
	resp->has_err = FALSE;
	puts("Returning response.");
	return TRUE;
}

BOOL build_message(char* msg, char* operation, char* buffer) {
	int i;
	short length;
	char buf[MAXBUFLEN];
	
	length = strlen(msg) + 5;
	buf[0] = (char) (length >> 8 );
	buf[1] = (char) (length & 0xff);
	buf[2] = (char) (1 >> 8);
	buf[3] = (char) (1);
	buf[4] = (char) (170);
	for (i = 0; i < length;i++){
		buf[i+5] = msg[i];
	}
	memcpy(buffer, buf, length + 5);
	print_msg(buffer, length+5);
	return TRUE;
}

void print_msg(unsigned char *msg, uint16 length) {
	uint16 i;
	for (i = 0; i < length; i++)
	{
		if (i > 0) printf(", ");
		printf("%02X", msg[i]);
	}
	printf("\n");
}

uint16 bytes_to_uint16(unsigned char* b) {
	uint16 r;
	if (!is_bigendian()) {
		puts("detected little-endian");
		r = (uint16) (b[0] * pow(2,8) + b[1]);
	} else {
		puts("detected big-endian");
		r = (uint16) (b[1] * pow(2,8) + b[0]);
	}
	printf("bytes_to_int returned %d", r);
	return r;
}

void uint16_to_bytes(uint16 n, char *b) {
	if (!is_bigendian()) {
		puts("detected little-endian");
		b[0] = n / pow(2,8);
		b[1] = n - (b[0] * pow(2,8));
	} else {
		puts("detected big-endian");
		b[1] = n / pow(2,8);
		b[0] = n - (b[1] * pow(2,8));
	}
}
