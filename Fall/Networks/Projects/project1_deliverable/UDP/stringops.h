#include <stdint.h>

#define TO_UPPER 32
#define MIN_MSG_LEN 5
#define TRUE 1
#define FALSE 0
#define GET_LEN 85
#define CAPS 170

typedef short BOOL;
typedef uint8_t uint8;
typedef uint16_t uint16;

struct request_t {
	uint16 msg_len;
	uint16 req_id;
	uint8 op;
	char *input;
	char *err;
	short has_err;
};

struct response_t {
	uint16 len;
	char *string;
	char *result;
	char *err;
	short has_err;
};

uint16 count_length(char input[]);
char* to_upper(char input[], uint16 length);
BOOL handle_request(struct request_t *req, struct response_t *resp);
BOOL build_request(char* msg, struct request_t *req);
void print_msg(unsigned char *msg, uint16 length);
