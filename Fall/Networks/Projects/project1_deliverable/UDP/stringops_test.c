#include "stringops.h"
#include "assert.h"
#include <stdio.h>
#include <string.h>

int main() {

	struct request_t req;
	struct response_t resp;

	// Test 1, get_length for "Hello"
	char msg[10] = { 0, 10, 0, 7, GET_LEN, 'H', 'e', 'l', 'l', 'o' };
	build_request(msg, &req);
	puts("Built request.");
	assert(req.has_err == FALSE);
	assert(req.req_id == 7);
	assert(req.op == GET_LEN);
	char test[] = "Hello";
	printf("The req->input is: %s\n", req.input);
	assert(strcmp(req.input, test) == 0);
	handle_request(&req, &resp);
	assert(resp.len == 5);
	assert(resp.has_err == FALSE);

	// Test 2, capitalize "Hello"
	msg[4] = CAPS;
	build_request(msg, &req);
	puts("Built request.");
	assert(req.has_err == FALSE);
	assert(req.req_id == 7);
	assert(req.op == CAPS);
	char test2_req[] = "Hello";
	printf("The req->input is: %s\n", req.input);
	assert(strcmp(req.input, test2_req) == 0);
	handle_request(&req, &resp);
	assert(resp.len == 5);
	char test2_resp[] = "HELLO";
	printf("The resp->result is: %s\n", resp.result);
	assert(strcmp(resp.result, test2_resp) == 0);
	assert(resp.has_err == FALSE);

	// Test 3, invalid op code
	msg[4] = 1;
	build_request(msg, &req);
	puts("Built request.");
	assert(req.has_err == TRUE);
	printf("Here is the error text: %s\n", req.err);
	return 0;
}
