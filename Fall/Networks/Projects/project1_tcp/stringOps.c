#include "stringOps.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAXBUFLEN 128
#define HEADER_LEN 5 // bytes in the message header
#define MAX_RESP_LEN 250
const int test_val = 1;
#define is_bigendian() ( (*(char*)&test_val) == 0 )

void uint16_to_bytes(uint16 n, char *b);
uint16 bytes_to_uint16(unsigned char* b);

uint16 count_length(char input[]) {
	uint16 i = 0;
	uint16 count = 0;
	while (input[i] != '\0') {
		count++;
		i++;
	}
	return count;
}

char* to_upper(char input[], uint16 length) {
	char *result;
	result = (char*) malloc(sizeof(char) * length);
	uint16 i;
	for (i = 0; i < length; i++) {
		if (input[i] >= 'a' && input[i] <= 'z') {
			result[i] = input[i] - TO_UPPER;
		} else {
			result[i] = input[i];
		}
	}
	result[length] = '\0';
	return result;
}

BOOL handle_response(char* msg, char* operation) {
	int length;
	
	if (strcmp(operation,"85") == 0) {
		int r_len = 0;
		r_len = ((unsigned char)msg[0]) * 256;
		r_len += (unsigned char)msg[1];
		r_len -= 4;
		printf("Length is %d\n", r_len);
	} else if (strcmp(operation, "170") == 0) {
		printf("Received response: %s\n", &msg[4]);
	} else {
		return FALSE;
	}
	return TRUE;
}

BOOL build_message(char* msg, char* operation, char* buffer) {
	int i;
	short length;
	unsigned char buf[MAXBUFLEN];
	
	length = strlen(msg) + HEADER_LEN;
	buf[0] = (unsigned char) (length >> 8 );
	buf[1] = (unsigned char) (length & 0xff);
	buf[2] = (unsigned char) (1 >> 8);
	buf[3] = (unsigned char) (1);
	buf[4] = (unsigned char) operation;
	for (i = 0; i < length;i++){
		buf[i+5] = msg[i];
	}
	memcpy(buffer, buf, length + 5);
	print_msg(buffer, length+5);
	return TRUE;
}

void print_msg(unsigned char *msg, uint16 length) {
	uint16 i;
	for (i = 0; i < length; i++)
	{
		if (i > 0) printf(", ");
		printf("%02X", msg[i]);
	}
	printf("\n");
}

uint16 bytes_to_uint16(unsigned char* b) {
	uint16 r;
	if (!is_bigendian()) {
		puts("detected little-endian");
		r = (uint16) (b[0] * pow(2,8) + b[1]);
	} else {
		puts("detected big-endian");
		r = (uint16) (b[1] * pow(2,8) + b[0]);
	}
	printf("bytes_to_int returned %d", r);
	return r;
}

void uint16_to_bytes(uint16 n, char *b) {
	if (!is_bigendian()) {
		puts("detected little-endian");
		b[0] = n / pow(2,8);
		b[1] = n - (b[0] * pow(2,8));
	} else {
		puts("detected big-endian");
		b[1] = n / pow(2,8);
		b[0] = n - (b[1] * pow(2,8));
	}
}
