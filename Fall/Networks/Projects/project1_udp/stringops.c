#include "stringops.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define HEADER_LEN 4 // bytes in the response header
#define MAX_RESP_LEN 250
const int test_val = 1;
#define is_bigendian() ( (*(char*)&test_val) == 0 )

void uint16_to_bytes(uint16 n, char *b);
uint16 bytes_to_uint16(unsigned char* b);

uint16 count_length(char input[]) {
	uint16 i = 0;
	uint16 count = 0;
	while (input[i] != '\0') {
		count++;
		i++;
	}
	return count;
}

char* to_upper(char input[], uint16 length) {
	char *result;
	result = (char*) malloc(sizeof(char) * length);
	uint16 i;
	for (i = 0; i < length; i++) {
		if (input[i] >= 'a' && input[i] <= 'z') {
			result[i] = input[i] - TO_UPPER;
		} else {
			result[i] = input[i];
		}
	}
	result[length] = '\0';
	return result;
}

BOOL handle_request(struct request_t *req, struct response_t *resp) {
	resp->has_err = TRUE;
	char *resp_val;
	uint16 resp_val_len;
	uint16 string_len = count_length(req->input);
	if (req->op == CAPS) {
		resp->string = to_upper(req->input, string_len);
		//print_msg(resp->string, string_len);
		resp->len = string_len + HEADER_LEN; //4 bytes for TML and req_id
		resp_val = resp->string;
		resp_val_len = string_len;
	} else {
		resp->len = 6;
		resp_val = (char*)malloc(sizeof(uint16));
		uint16_to_bytes(string_len, resp_val);
		resp_val_len = 2;
	}

	resp->result = (char*)malloc(sizeof(char) * resp->len);
	uint16_to_bytes(resp->len, resp->result);
	uint16_to_bytes(req->req_id, &(resp->result[2]));
	memcpy(&(resp->result[4]), resp_val, resp_val_len);
	resp->has_err = FALSE;
	return TRUE;
}

BOOL build_request(char* msg, struct request_t *req) {
	req->has_err = TRUE;
	req->err = (char*)malloc(sizeof(char) * 200);
	// first two bytes are the TML
	req->msg_len = bytes_to_uint16(msg);
	//print_msg(msg, req->msg_len);
	// TML + RequestID + Operation : (2 + 2 + 1) = 5

	if (req->msg_len < MIN_MSG_LEN) {
		sprintf(req->err, "Message can't be less than %d bytes long.", MIN_MSG_LEN);
		return FALSE;
	}

	req->req_id = bytes_to_uint16(&(msg[2]));
	req->op = msg[4];
	if (req->op != GET_LEN && req->op != CAPS) {
		puts("Error: Invalid opcode");
		sprintf(req->err, "Invalid op code: 85 = length, 170 = capitalize.");
		return FALSE;
	}

	req->input = &msg[5];
	// length of the string == TML - ( 2 + 2 + 1)
	// terminate the input string with a NULL byte
	uint16 input_len = req->msg_len - 5;
	char *end = req->input + input_len; 
	*end = '\0';
	req->has_err = FALSE;

	//print_msg(msg, req->msg_len);
	return TRUE;
}

void print_msg(unsigned char *msg, uint16 length) {
	uint16 i;
	for (i = 0; i < length; i++)
	{
		if (i > 0) printf(", ");
		printf("%02X", msg[i]);
	}
	printf("\n");
}

uint16 bytes_to_uint16(unsigned char* b) {
	uint16 r;
	if (!is_bigendian()) {
		r = (uint16) (b[0] * pow(2,8) + b[1]);
	} else {
		r = (uint16) (b[1] * pow(2,8) + b[0]);
	}
	return r;
}

void uint16_to_bytes(uint16 n, char *b) {
	if (!is_bigendian()) {
		b[0] = n / pow(2,8);
		b[1] = n - (b[0] * pow(2,8));
	} else {
		b[1] = n / pow(2,8);
		b[0] = n - (b[1] * pow(2,8));
	}
}
